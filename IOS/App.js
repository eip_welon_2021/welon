import React from 'react';
import MainApp from './Components/App'
import { store } from './Store/Reducer/rootReducer'
import { StyleSheet } from 'react-native';
import { Provider } from 'react-redux'


class App extends React.Component {

  constructor(props) {
    super(props);
  
  }

  render() {
    
    return (
      <Provider store={store} >
        <MainApp />
      </Provider>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default (App)