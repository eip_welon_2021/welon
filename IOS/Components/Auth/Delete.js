import React from 'react'
import {
    Text,
    View,
    Button,
    StyleSheet,
    Image,
    AsyncStorage
} from 'react-native'

import { connect } from 'react-redux'
import { logout, deleteUser } from '../../Store/Actions/authActions';

import Headers from '../Drawer/Headers'


class Logout extends React.Component {

  state = ({
    token: ''
  })

  static navigationOptions = {
    headerStyle: {
       height: 80,
     },
     title: "Suppression",     

     headerRight: (
       <Headers/>
     )
   }

  delete = async () => {
    this.props.Delete({token: this.props.navigation.state.params.token, email: this.props.navigation.state.params.email})
    await AsyncStorage.multiRemove(['token'])
  }

  
    render() {

      if (!this.props.error && this.props.deleted && !this.props.loading)
      {
        this.props.navigation.navigate('SignIn')
      }
      
        return (
                <View style={styles.container}>
                  <Image
                    resizeMode="contain"
                      style={styles.headingImage}
                      source={require('../assets/logo_green.png')}
                    />
                  <Text style={styles.greeting}>
                        Supression de votre compte
                  </Text>
                  <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 50}}>
                  
                  
                    <Text style={{ marginTop: 10}}> Toutes vos données seront éffacées de notre plateforme </Text>
                    <Text style={{ marginTop: 20}}> Cette action est irréverssible </Text>

                  </View>
                    

                  <View style={styles.containers}>
                    <View style={styles.buttonContainer}>
                      <Button title="Supprimer" color="red" onPress={this.delete}/>
                    </View>
                    <View style={styles.buttonContainer}>
                      <Button title="Annuler" color="green"/>
                    </View>
                  </View>
                </View>
        )
    }
}

const styles = StyleSheet.create({
  containers: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 60
  },
  buttonContainer: {
    justifyContent: 'flex-start',
    flex: 1,
  },
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    button: {
      margin: 10, 
    },
    headingImage: {
      width: 200,
      height: 259,
    },
    greeting: {
      color: 'black',
      fontWeight: 'bold',
      textAlign: 'center',
      fontSize: 30,
      marginTop: 17
    },
})

const mapStateToProps = (state) => {
  return {
    error: state.auth.deleteError,
    token: state.auth.user.token,
    deleted: state.auth.deleteSuccess,
    loading: state.auth.deleteLoading,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    Logout: (text) => {dispatch(logout(text))},
    Delete: (text) => {dispatch(deleteUser(text))}
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout)

