import React from 'react'
import {
    Text,
    View,
    Button,
    StyleSheet,
    AsyncStorage
} from 'react-native'

import { connect } from 'react-redux'
import { logout } from '../../Store/Actions/authActions';

import Headers from '../Drawer/Headers'


class Logout extends React.Component {

  state = ({
    token: ''
  })

  static navigationOptions = {
    headerStyle: {
       height: 80,
     },
     title: "Déconnexion",     

     headerRight: (
       <Headers/>
     )
   }

  

  onPress = async () => {
    this.props.Logout(this.state)
    await AsyncStorage.multiRemove(['token'])
    this.props.navigation.navigate('SignIn')
  }

  componentDidMount () {

    AsyncStorage.getItem('token').then((result) => {
      if (result) {
        this.setState({
          token: result,
        })
      }
    })
    this.setState({
      token: this.props.token,
    })
  }

  
    render() {
      
        return (
                <View style={styles.container}>
                    <Text style={styles.greeting}>
                        Déconnexion
                    </Text>
                    <View style={styles.button}>
                    <Button 
                      title="Se déconnecter"
                      color="#FFFFFF"
                      onPress={this.onPress}
                    />
                  </View>
                </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-around',
    },
    button: {
      margin: 10, 
      backgroundColor: "#00C851"
    },
    greeting: {
      color: 'black',
      fontWeight: 'bold',
      textAlign: 'center',
      fontSize: 45,
      marginTop: 100
    },
})

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    token: state.auth.user.token
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    Logout: (text) => {dispatch(logout(text))}
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout)

