import React from 'react'
import {
    Text,
    View,
    Image,
    Button,
    Keyboard,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    TouchableWithoutFeedback,
    AsyncStorage
} from 'react-native'
import { connect } from 'react-redux'
import { signIn, me } from '../../Store/Actions/authActions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      {children}
    </TouchableWithoutFeedback>
  )

class SignIn extends React.Component {

  static navigationOptions = {
    headerTransparent: true,
    headerLeft: null
  }

    state = {
        email: '',
        password: '',
        hidePassword: true
      }

    managePasswordVisibility = () => {
      this.setState({ hidePassword: !this.state.hidePassword})
    }
    
    onSignInpPress = () => {
        this.props.SignIn(this.state)
      }

      componentDidMount() {
        AsyncStorage.getItem('token').then((result) => {
          if (result) {
            this.props.navigation.navigate('Home', { token: result })
          }
        })
      }

    render() {

      if (this.props.loggedIn == true) {
        AsyncStorage.setItem('token', this.props.token)
        this.props.navigation.navigate('Home')
      }

        return (
          <DismissKeyboard>

          <KeyboardAwareScrollView 
          resetScrollToCoords={{ x: 0, y: 0 }}
          contentContainerStyle={styles.container}
          scrollEnabled={false}>
                <View style={styles.container}>
                  <Image
                  resizeMode="contain"
                    style={styles.headingImage}
                    source={require('../assets/logo_green.png')}
                  />
                  <Image
                  resizeMode="contain"
                    style={styles.headingImage2}
                    source={require('../assets/Chef.png')}
                  />
                    <View style={styles.inputStyle}>
                        <TextInput
                            type='email'
                            placeholder="Email"
                            autoCorrect={false}
                            autoCapitalize='none'
                            style={styles.textInput}
                            onChangeText={(text) => {this.setState(() => {
                              return {
                                email: text
                              }
                            })} }
                        />
                      <View>
                      <TextInput
                            type='password'
                            autoCorrect={false}
                            autoCapitalize='none'
                            style={styles.textInput}
                            placeholder="Mot de passe"
                            placeholderTextColor="#a0a0a0"
                            secureTextEntry = { this.state.hidePassword}
                            onChangeText={(text) => {this.setState(() => {
                              return {
                                password: text
                              }
                            })} }
                        />
                      <TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = { this.managePasswordVisibility }>
                        <Image source = { ( this.state.hidePassword ) ? require('../assets/hide_password.png') : require('../assets/show-password.jpg') } style = { styles.btnImage } />
                      </TouchableOpacity>
                      </View>
                      
                        { this.props.error ?
                          (
                            <Text style={styles.error}> {this.props.error} </Text>
                          )
                          :
                          (
                            <Text></Text>
                          )
                        }
                    </View>
                    <View style={styles.bottom}>
                      <View style={styles.button}>
                        <Button 
                          title="Se connecter"
                          color="#FFFFFF"
                          onPress={this.onSignInpPress}
                        />
                      </View>
                      <Text> Vous n'avez pas de compte ? 
                        <Text style={[{color: '#00C851'}]} 
                              onPress = {() => this.props.navigation.push('SignUp')}> S'inscrire </Text> 
                      </Text>
                    </View>
                </View>
          </KeyboardAwareScrollView>
          </DismissKeyboard>

        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-around',
    },
    inputStyle : {
      marginTop: 150,
      marginBottom: 300
    },
    button: {
      marginTop: 50,
      margin: 10, 
      backgroundColor: "#00C851"
    },
    greeting: {
      color: 'black',
      fontWeight: 'bold',
      textAlign: 'center',
      fontSize: 45,
      marginTop: 100
    },
    bottom: {
      marginBottom: 200,
    },
    headingImage: {
      width: 200,
      height: 259,
      marginTop: 100
    },
    headingImage2: {
      width: 300,
      height: 459,
      marginTop: 90,
      marginBottom: 100
    },
    error: {
      color: 'red',
      textAlign: 'center',
      width: 250,
    },
    textInput: {
        height: 45,
        width: 250,
        marginBottom: 15,
        borderBottomWidth: 1.5,
        fontSize: 16,
        borderBottomColor: '#00C851',
    },
    visibilityBtn:
  {
    position: 'absolute',
    right: 3,
    height: 40,
    width: 35,
    padding: 5
  },
 
  btnImage:
  {
    resizeMode: 'contain',
    height: '100%',
    width: '100%'
  }
})

const mapStateToProps = (state) => {
  return {
    loggedIn: state.auth.logIn,
    error: state.auth.signInError,
    token: state.auth.user.token,
    deleted: state.auth.deleteSuccess
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    SignIn: (text) => {dispatch(signIn(text))}
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn)