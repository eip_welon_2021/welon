import React from 'react'
import {
    Text,
    View,
    Image,
    Button,
    Keyboard,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


import { connect } from 'react-redux'

const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      {children}
    </TouchableWithoutFeedback>
  )

class SignUp extends React.Component {

  static navigationOptions = {
    headerTransparent: true,
    headerLeft: null
  }

    state = {
        email: '',
        password: '',
        passwordConfirmed: '',
        error: false,
        hidePassword: true,
        hidePasswordC: true,
      }

    handleOnPress = () => {
      if (this.state.password && this.state.passwordConfirmed && this.state.email) 
          this.props.navigation.push('SignUpBiss', this.state) 
      else {
        this.setState({error: true})
      }
    }

    managePasswordVisibility = () => {
      this.setState({ hidePassword: !this.state.hidePassword})
    }

    managePasswordVisibility1 = () => {
      this.setState({ hidePasswordC: !this.state.hidePasswordC})
    }
     
    render() {

        return (
            <DismissKeyboard>
              <KeyboardAwareScrollView 
              resetScrollToCoords={{ x: 0, y: 0 }}
              contentContainerStyle={styles.container}
              scrollEnabled={false}>
              <View style={styles.container}>
                  <Image
                  resizeMode="contain"
                    style={styles.headingImage}
                    source={require('../assets/logo_green.png')}
                  />
                  <Image
                  resizeMode="contain"
                    style={styles.headingImage2}
                    source={require('../assets/Chef.png')}
                  />
                    <View style={styles.inputStyle}>
                        <TextInput
                            type='email'
                            placeholder="Email"
                            autoCorrect={false}
                            autoCapitalize='none'
                            style={styles.textInput}
                            onChangeText={(text) => {this.setState(() => {
                              return {
                                email: text
                              }
                            })} }
                        />
                        <View>
                        <TextInput
                            type='password'
                            autoCorrect={false}
                            autoCapitalize='none'
                            style={styles.textInput}
                            placeholder="Mot de passe"
                            placeholderTextColor="#a0a0a0"
                            secureTextEntry = { this.state.hidePassword}
                            onChangeText={(text) => {this.setState(() => {
                              return {
                                password: text
                              }
                            })} }
                        />
                          <TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = { this.managePasswordVisibility }>
                            <Image source = { ( this.state.hidePassword ) ? require('../assets/hide_password.png') : require('../assets/show-password.jpg') } style = { styles.btnImage } />
                          </TouchableOpacity>
                        </View>
                        
                        <View> 
                        <TextInput
                            type='password'
                            autoCorrect={false}
                            autoCapitalize='none'
                            style={styles.textInput}
                            placeholder="Confirmer Mot de passe"
                            placeholderTextColor="#a0a0a0"
                            secureTextEntry = { this.state.hidePasswordC }
                            onChangeText={(text) => {this.setState(() => {
                              return {
                                passwordConfirmed: text
                              }
                            })} }
                        />
                        <TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = { this.managePasswordVisibility1 }>
                            <Image source = { ( this.state.hidePasswordC ) ? require('../assets/hide_password.png') : require('../assets/show-password.jpg') } style = { styles.btnImage } />
                          </TouchableOpacity>
                        </View>
                        
                        { this.state.error ?
                          (
                            <Text style={styles.error}> Vous devez remplir tous les champs</Text>
                          )
                          :
                          (
                            <Text></Text>
                          )
                        }
                    </View>
                    <View style={styles.bottom}>
                      <View style={styles.button}>
                        <Button 
                          title="Suivant"
                          color="#FFFFFF"
                          onPress = {this.handleOnPress}
                        />
                      </View>
                      <Text> Vous avez déjà un compte ? 
                        <Text style={[{color: '#00C851'}]} onPress = {() => this.props.navigation.navigate('SignIn')}> Se connecter </Text>
                      </Text>
                    </View>
                </View>
              </KeyboardAwareScrollView>
            </DismissKeyboard>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-around',
    },
    button: {
      margin: 10, 
      backgroundColor: "#00C851"
    },
    inputStyle : {
      marginTop: 130,
      marginBottom: 300
    },
    bottom: {
      marginBottom: 200,
    },
    headingImage: {
      width: 200,
      height: 259,
      marginTop: 100
    },
    headingImage2: {
      width: 300,
      height: 459,
      marginTop: 90,
      marginBottom: 100
    },
    error: {
      color: 'red',
      textAlign: 'center'
    },
    textInput: {
        height: 45,
        width: 250,
        marginBottom: 15,
        borderBottomWidth: 1.5,
        fontSize: 16,
        borderBottomColor: '#00C851',
    },
    visibilityBtn:
    {
      position: 'absolute',
      right: 3,
      height: 40,
      width: 35,
      padding: 5
    },
   
    btnImage:
    {
      resizeMode: 'contain',
      height: '100%',
      width: '100%'
    }
})

const mapStateToProps = (state) => {
  return {
    loggedIn: state.auth.loggedIn
  }
}

const mapDispatchToProps = (dispatch) => {
  return { }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp)
