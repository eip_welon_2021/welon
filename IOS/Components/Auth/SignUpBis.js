import React from 'react'
import {
    Text,
    View,
    Image,
    Button,
    Keyboard,
    TextInput,
    StyleSheet,
    AsyncStorage,
    TouchableWithoutFeedback
} from 'react-native'
import { connect } from 'react-redux'
import { signUp } from '../../Store/Actions/authActions'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Information from '../Information'


const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      {children}
    </TouchableWithoutFeedback>
  )

class SignUpBis extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      firstname: '',
      lastname: '',
      phone: '',
      email: '',
      password: '',
    }
  }

  static navigationOptions = {
    headerTransparent: true,
  }
  
  componentDidMount () {
    const { navigation } = this.props;
    this.setState({
      email: navigation.getParam('email'),
      password: navigation.getParam('password'),
    })
  }

  onSignUpPress = () => {
    this.props.SignUp(this.state)
    //this.props.navigation.navigate('Information')
  }
  
  render() {
    if (!this.props.error && this.props.loggedIn && !this.props.loading){
      this.props.navigation.navigate('Information')

    }


    return (
            <DismissKeyboard>
              <KeyboardAwareScrollView
              resetScrollToCoords={{ x: 0, y: 0 }}
              contentContainerStyle={styles.container}
              scrollEnabled={false}>
              <View style={styles.container}>
                <Image
                  resizeMode="contain"
                    style={styles.headingImage}
                    source={require('../assets/logo_green.png')}
                  />
                  <Image
                  resizeMode="contain"
                    style={styles.headingImage2}
                    source={require('../assets/Chef.png')}
                />
                <View style={styles.inputStyle}>
                    <TextInput
                        type='name'
                        placeholder="Nom"
                        autoCorrect={false}
                        autoCapitalize='none'
                        style={styles.textInput}
                        onChangeText={(text) => {this.setState(() => {
                          return {
                            firstname: text
                          }
                        })} }
                    />
                    <TextInput
                        autoCorrect={false}
                        placeholder="Prénom"
                        autoCapitalize='none'
                        style={styles.textInput}
                        placeholderTextColor="#a0a0a0"
                        onChangeText={(text) => {this.setState(() => {
                          return {
                            lastname: text
                          }
                        })} }
                    />
                    <TextInput
                        autoCorrect={false}
                        autoCapitalize='none'
                        style={styles.textInput}
                        keyboardType = "number-pad"
                        placeholderTextColor="#a0a0a0"
                        placeholder="Numéro de téléphone"
                        onChangeText={(text) => {this.setState(() => {
                          return {
                            phone: text
                          }
                        })} }
                    />
                    { this.props.error ?
                          (
                            <Text style={styles.error}> {this.props.error} </Text>
                          )
                          :
                          (
                            <Text></Text>
                          )
                        }
                </View>
                <View style={styles.bottom}>
                  <View style={styles.button}>
                    <Button 
                      title="S'inscrire"
                      color="#FFFFFF"
                      onPress={this.onSignUpPress}
                    />
                  </View>
                  <Text> Vous avez déjà un compte ? <Text style={[{color: '#00C851'}]} onPress = {() => this.props.navigation.push('SignIn')}> Se connecter </Text> </Text>
                </View>
            </View>
              </KeyboardAwareScrollView>
        </DismissKeyboard>
    )
  }   
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-around',
    },
    button: {
      margin: 10, 
      backgroundColor: "#00C851"
    },
    bottom: {
      marginBottom: 150,
    },
    headingImage: {
      width: 200,
      height: 259,
      marginTop: 100
    },
    headingImage2: {
      width: 300,
      height: 459,
      marginBottom: 95
    },
    error: {
      color: 'red',
      textAlign: 'center',
      width: 250,
    },  
    inputStyle : {
      marginBottom: 200
    },
    textInput: {
        height: 45,
        width: 250,
        marginBottom: 15,
        borderBottomWidth: 1.5,
        fontSize: 16,
        borderBottomColor: '#00C851',
    }
})

const mapStateToProps = (state) => {
  return {
    loggedIn: state.auth.signUpSuccess,
    error: state.auth.signUpError,
    loading: state.auth.signUpLoading
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    SignUp: (text) => {dispatch(signUp(text))}
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpBis)
