import React from 'react';
import { connect } from 'react-redux'

import BoissonList from './BoissonList'
import  { getAllBoisson } from '../../Store/Actions/boissonActions'

import { Container, Content } from 'native-base';


class Boisson extends React.Component {


  componentDidMount() {
    this.props.dispatch(getAllBoisson({token: this.props.navigation.dangerouslyGetParent().getParam('token'),
                                   restaurant: this.props.navigation.dangerouslyGetParent().getParam('restaurant')._id
  
  }))
  }


  render() {
    return (
      <Container>
      <Content>
        <BoissonList boisson={this.props.boisson}/>
      </Content>
    </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    token: state.auth.user.token,
    boisson: state.boisson.boisson,
    loading: state.boisson.loading
  }
}

export default connect(mapStateToProps)(Boisson)
