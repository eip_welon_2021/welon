import React from 'react';
import { connect } from 'react-redux'

import { ActivityIndicator } from 'react-native'

import BoissonDetail from './BoissonDetail'

import { Container, Content, List } from 'native-base';


class BoissonList extends React.Component {


  render() {

      return (
      <Container style={{marginTop: 10}}>
      <Content>
        <List>
          {
            this.props.boisson && this.props.boisson.map(obj => {
                return(
                    <BoissonDetail boisson={obj} key={obj._id}/>
                )
            })
          }
        </List>
      </Content>
    </Container>
    );
  }
}


export default (BoissonList)
