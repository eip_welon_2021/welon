import React from 'react';
import { connect } from 'react-redux'

import DessertList from './DessertList'
import  { getAllDessert } from '../../Store/Actions/dessertActions'

import { Container, Content } from 'native-base';


class Dessert extends React.Component {


  componentDidMount() {
    this.props.dispatch(getAllDessert({token: this.props.navigation.dangerouslyGetParent().getParam('token'),
                                   restaurant: this.props.navigation.dangerouslyGetParent().getParam('restaurant')._id
  
  }))
  }


  render() {
    return (
      <Container>
      <Content>
        <DessertList dessert={this.props.dessert}/>
      </Content>
    </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    token: state.auth.user.token,
    dessert: state.dessert.dessert,
    loading: state.dessert.loading
  }
}

export default connect(mapStateToProps)(Dessert)
