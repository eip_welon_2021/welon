import React from 'react';
import { connect } from 'react-redux'

import { ActivityIndicator } from 'react-native'

import DessertDetail from './DessertDetail'

import { Container, Content, List } from 'native-base';


class DessertList extends React.Component {


  render() {


    const {loading } = this.props

    if (loading)
      return <ActivityIndicator size="small" color="#00ff00" />

      return (
      <Container style={{marginTop: 10}}>
      <Content>
        <List>
          {
            this.props.dessert && this.props.dessert.map(obj => {
                return(
                    <DessertDetail dessert={obj} key={obj._id}/>
                )
            })
          }
        </List>
      </Content>
    </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    token: state.auth.user.token,
    loading: state.dessert.loading
  }
}

export default connect(mapStateToProps)(DessertList)
