import React from 'react';

import { withNavigation } from 'react-navigation';

import { DrawerActions } from 'react-navigation-drawer';

import { TouchableOpacity, StyleSheet, Image } from 'react-native';


class DrawerTrigger extends React.Component {

  render() {
    return (
      <TouchableOpacity 
        style={styles.trigger}
        onPress={() => {
          this.props.navigation.dispatch(DrawerActions.openDrawer())
        }}>
          <Image
            style={{width: 70, height: 60}}
            source={require('../../assets/Chef.png')}/>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  trigger: {
    paddingTop: 6,
    marginRight: 10,
    borderRadius: 30,
    width: 80,
    height: 60,
    color: "transparent"
  }
});

export default withNavigation(DrawerTrigger);
