import React from 'react';
import { connect } from 'react-redux'

import EntréesList from './EntréesList'
import  { getAllEntrées } from '../../Store/Actions/entréesActions'

import { Container, Content } from 'native-base';


class Entrées extends React.Component {


 componentDidMount() {
  this.props.dispatch(getAllEntrées({token: this.props.navigation.dangerouslyGetParent().getParam('token'),
                                   restaurant: this.props.navigation.dangerouslyGetParent().getParam('restaurant')._id
  
  }))
  }


  render() {
    
    return (
      <Container>
      <Content>
        <EntréesList entrées={this.props.entrées}/>
      </Content>
    </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    token: state.auth.user.token,
    entrées: state.entrées.entrées,
    loading: state.entrées.loading
  }
}

export default connect(mapStateToProps)(Entrées)
