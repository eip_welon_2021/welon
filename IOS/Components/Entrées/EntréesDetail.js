import React from 'react';
import { connect } from 'react-redux'
import { Text, Card, CardItem} from 'native-base';
import ProgressiveImage from '../ProgressiveImage'
import { TouchableRipple } from 'react-native-paper';
import { Image, StyleSheet, View } from 'react-native'
import Modal, { ModalContent, ModalTitle } from 'react-native-modals';

class EntréesDetail extends React.Component {

  state = {
    visible: false
  };

  toggleModal = () => {
    this.setState({ visible: !this.state.visible });
  };

  render() {

    str = this.props.entrées.name 
    
    return (
      <TouchableRipple
        onPress={this.toggleModal}>
        <Card style={styles.card}>
          <CardItem >
            <ProgressiveImage
              source={require('../../assets/appetizer-min.jpg')}
              style={styles.img}
              resizeMode="cover"/>
            <View>
              <Text style={styles.name}>   {this.props.entrées.name}</Text>
              <Text note numberOfLines={1} style={styles.price}>   {this.props.entrées.price} €</Text>
            </View>
          </CardItem>
          <Modal
            height={230}
            width={250}
            modalTitle={<ModalTitle title={str}/>}
            visible={this.state.visible}
            onTouchOutside={() => {this.setState({ visible: false });}}>
              <ModalContent style={{margin: 10}}>

              <Image style={{ alignSelf: 'center', width: 240,height: 110}} source={require('../../assets/appetizer-min.jpg')}/>
                <Text style={{ alignSelf: 'center', marginTop: 10}}> {this.props.entrées.description} </Text>
                <Text style={{ alignSelf: 'center', color:"#c40474" }}> {this.props.entrées.price} € </Text>
              </ModalContent>
            </Modal>
        </Card>  
      </TouchableRipple>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    token: state.auth.user.token,
    loading: state.entrées.loading
  }
}

export default connect(mapStateToProps)(EntréesDetail)

const styles = StyleSheet.create({
  
  card: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 20,
    marginBottom: 20,
    width: 350
  },
  modal: {
    alignSelf: 'center',
    flex: 1,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
  },
  img: {
    marginTop: -10,
    marginLeft: -15,
    marginBottom:-10,
    width: 120,
    height: 110
  },
  price: {
    fontSize: 15,
    marginBottom: 12,
    color:"#c40474"
  },
  name: {
    fontSize: 15,
    marginTop: 2,
    marginBottom: 10
  },
  content: {
    height: 42,
    marginLeft: -10,
  },
  Text : {
    marginTop: 10,
    fontSize: 16,
    color: '#929492'
  }
})