import React from 'react';
import { connect } from 'react-redux'

import { ActivityIndicator } from 'react-native'

import EntréesDetail from './EntréesDetail'

import { Container, Content, List } from 'native-base';


class EntréesList extends React.Component {


  render() {


    const {loading } = this.props

    if (loading)
      return <ActivityIndicator size="small" color="#00ff00" />

      return (
      <Container style={{marginTop: 10}}>
      <Content>
        <List>
          {
            this.props.entrées && this.props.entrées.map(obj => {
                return(
                    <EntréesDetail entrées={obj} key={obj._id}/>
                )
            })
          }
        </List>
      </Content>
    </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    token: state.auth.user.token,
    loading: state.entrées.loading
  }
}

export default connect(mapStateToProps)(EntréesList)
