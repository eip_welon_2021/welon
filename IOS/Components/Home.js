import React from 'react'
import { connect } from 'react-redux'
import Headers from './Drawer/Headers'
import { Text, ActivityIndicator, AsyncStorage, StyleSheet, View, Dimensions, Image, TouchableHighlight } from 'react-native'
import RestautantList from './Restaurant/RestaurantList'
import { getAllRestaurant, getBestRestaurant } from '../Store/Actions/restaurantActions'
import { me } from '../Store/Actions/authActions'
import SignIn from './Auth/SignIn'
import Information from './Information'
import { Container } from 'native-base'
import Icon from "react-native-vector-icons/FontAwesome";



class Home extends React.Component {

  state = ({
    token: ''
  })

  static navigationOptions = {
   headerStyle: {
      height: 80,
    },
    headerLeft: (
      <Text style={{fontSize:28, 
        marginLeft: 20, 
        paddingTop: 20, 
        color: '#929492'}}>RESTAURANTS</Text>
    ),
    headerRight: (
      <Headers/>

    )
  }

  best =  () => {
    this.props.dispatch(getBestRestaurant(this.state.token))
  }

  all = () => {
    this.props.dispatch(getAllRestaurant(this.state.token));
  }

  componentDidMount () {

    AsyncStorage.getItem('token').then((result) => {
        if (result) {          
            this.setState({
              token: result
            })
            this.props.dispatch(me({token : result}));

        } else {
          this.setState({
            token: this.props.token
          })
          this.props.dispatch(me({token : this.props.token}));
        }
        this.props.dispatch(getAllRestaurant(this.state.token));
      })

  }

    render() {

      const {loading } = this.props

      const sortTab = [].concat(this.props.restaurant
        .sort((a,b) => a.rank < b.rank ? -1 : 1))

      
      if (loading)
        return <ActivityIndicator size="small" color="#00ff00" />


      return(
          
        <Container >
          
          
          <RestautantList  token={this.state.token} restaurant={this.props} tab={sortTab}/>

        </Container>

        )
    }
}

const styles = StyleSheet.create({
  button: {
    marginTop: 50,
    margin: 10, 
    backgroundColor: "#00C851"
  },
  buttonContainer: {
    flex: 1,
  },
  buttonView: {
    flexDirection: 'row',
    display: 'flex'
  },
  text: {
    textAlign: 'center',
  },
  all:{
    width: 45,
    height: 60,
    marginTop:10,
    paddingTop:20,
    paddingBottom:20,
    borderRadius:60,
    borderWidth: 1,
    borderColor: '#fff',

  },
  submit:{
    width: 50,
    height:60,
    marginTop:10,
    paddingTop:20,
    marginLeft: 10,
    paddingBottom:20,
    borderRadius:60,
    borderWidth: 1,
    borderColor: '#fff',
    alignContent: 'flex-start'

  },
  submitText:{
      color:'#fff',
      textAlign:'center',
  },
  btnImage:
  {
    resizeMode: 'contain',
    height: '20%',
    width: '20%'
  }
})

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    login: state.auth.logIn,
    error: state.auth.authError,
    token: state.auth.user.token,
    restaurant: state.restaurant.restaurant,
    loading: state.restaurant.loading,
    user: state.auth.me,
    meLoading: state.auth.me.meLoading
  }
}

export default connect(mapStateToProps)(Home)

/*

<View style={{ flexDirection: 'row'}}>
          <TouchableHighlight underlayColor='#c7c4bd' style={styles.submit} onPress={this.best}>
          <Text style={styles.text}> Top 5 <Icon name="star" size={20} color="green" style={{marginTop:10}} /></Text>
          </TouchableHighlight>
          <TouchableHighlight underlayColor='#c7c4bd' style={styles.all} onPress={this.all}>
          <Text  style={styles.text}> Restaurants</Text>
          </TouchableHighlight>
          </View>
          
          
         <View style={{ flexDirection: 'row'}}>
          <TouchableHighlight underlayColor='#c7c4bd' style={styles.submit} onPress={this.best}>
          <Text style={styles.text}> <Icon name="star" size={20} color="green" style={{marginTop:10}} /></Text>
          </TouchableHighlight>
          <TouchableHighlight underlayColor='#c7c4bd' style={styles.all} onPress={this.all}>
          <Text  style={styles.text}>  <Icon name="refresh" size={20} color="green" style={{marginTop:10}} /></Text>
          </TouchableHighlight>
          </View> 
          
          
          
          */