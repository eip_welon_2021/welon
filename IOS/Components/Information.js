import React from 'react'
import {
    Text,
    View,
    Image,
    StyleSheet,
    AsyncStorage
} from 'react-native'
import { connect } from 'react-redux'

import { me } from '../Store/Actions/authActions'


class Informations extends React.Component {

  static navigationOptions = {
    headerTransparent: true,
    headerLeft: null
  }


    render() {

        return (
                <View style={styles.container}>
                  <Image
                  resizeMode="contain"
                    style={styles.headingImage}
                    source={require('../assets/logo_green.png')}
                  />
                  <Image
                  resizeMode="contain"
                    style={styles.headingImage2}
                    source={require('../assets/Chef.png')}
                  />
                    <View style={styles.inputStyle}>
                        
                    <Text style={styles.greeting}> Veuillez confirmez votre adresse email pour pouvoir vous connecter</Text>
                        
                    </View>
                    <View style={styles.bottom}>
                      <View style={styles.button}>
                        
                      </View>
                      <Text> Adresse Validée ?
                        <Text style={[{color: '#00C851'}]} 
                              onPress = {() => this.props.navigation.push('SignIn')}> Se connecter</Text> 
                      </Text>
                    </View>
                </View>

        )
    }
}

const mapDispatchToProps = (dispatch) => {
  return {
    GetMe: (text) => {dispatch(me(text))}
   }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.user.token,
    user: state.auth.me,
    meLoading: state.auth.me.meLoading
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-around',
    },
    inputStyle : {
      marginTop: 150,
      marginBottom: 300
    },
    button: {
      marginTop: 50,
      margin: 10, 
      backgroundColor: "#00C851"
    },
    greeting: {
      color: 'black',
      fontWeight: 'bold',
      textAlign: 'center',
      fontSize: 30,
      marginTop: 10,
      textAlign: 'center',
      width: 250,
    },
    bottom: {
      marginBottom: 200,
    },
    headingImage: {
      width: 200,
      height: 259,
      marginTop: 100
    },
    headingImage2: {
      width: 300,
      height: 459,
      marginTop: 90,
      marginBottom: 100
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Informations)