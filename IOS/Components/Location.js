import React, { Component } from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import Headers from './Drawer/Headers'


class Localisation extends React.Component {

  static navigationOptions = {
    headerStyle: {
       height: 80,
     },
     headerLeft: (
       <Text style={{fontSize:28, 
         marginLeft: 20, 
         paddingTop: 20, 
         color: '#929492'}}>Map</Text>
     ),
     headerRight: (
       <Headers/>
 
     )
   }

   state = {
    region: {
      latitude: 48.8534,
      longitude: -122.4324,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    },
   }

   onRegionChange = () => {
    this.setState({ region });

   }

   mapMarkers = () => {
     if (this.props.restaurant) {
      return this.props.restaurant.map((report) => <Marker
      key={report._id}
      coordinate={{ latitude: report.latitude, longitude: report.longitude }}
      title={report.name}
      description={report.description}
    >
    </Marker >)
     }
    
  }

  render() {

    return (
      <View style={styles.container}>
        <MapView
        style={styles.mapStyle}
          provider={PROVIDER_GOOGLE}
          showsUserLocation
          initialRegion={{
          latitude: this.props.latitude,
          longitude: this.props.longitude,
          latitudeDelta: 0.0043,
          longitudeDelta: 0.0034}}
          >
                    {this.mapMarkers()}
          </MapView>
      </View>
         
      
    )}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});

export default Localisation