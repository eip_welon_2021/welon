import React from 'react'
import { connect } from 'react-redux'
import Headers from '../Drawer/Headers'
import { Text, ActivityIndicator, AsyncStorage, StyleSheet, View, Button, Image, TouchableHighlight } from 'react-native'
import RestautantList from '../Restaurant/RestaurantList'
import { getAllRestaurant, getBestRestaurant } from '../../Store/Actions/restaurantActions'
import { me } from '../../Store/Actions/authActions'
import SignIn from '../Auth/SignIn'
import Information from '../Information'
import { Container } from 'native-base'
import Icon from "react-native-vector-icons/FontAwesome";
import { Marker, MapView, PROVIDER_GOOGLE }  from 'react-native-maps';
import Location from "../Location"



class Map extends React.Component {

  state = ({
    token: '',
    region: {
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      }
  })

  static navigationOptions = {
   headerStyle: {
      height: 80,
    },
    headerLeft: (
      <Text style={{fontSize:28, 
        marginLeft: 20, 
        paddingTop: 20, 
        color: '#929492'}}>Map</Text>
    ),
    headerRight: (
      <Headers/>

    )
  }

  componentDidMount () {

    AsyncStorage.getItem('token').then((result) => {
        if (result) {          
            this.setState({
              token: result
            })
            this.props.dispatch(me({token : result}));

        } else {
          this.setState({
            token: this.props.token
          })
          this.props.dispatch(me({token : this.props.token}));
        }
        this.props.dispatch(getAllRestaurant(this.state.token));
      })

  }

    render() {

      const {loading } = this.props

      
      if (loading)
        return <ActivityIndicator size="small" color="#00ff00" />


      return(
 
        <View>
          <Location longitude={37.78825} latitude={-122.4324} restaurant={this.props.restaurant}/>
        </View>
        

        )
    }
}

const styles = StyleSheet.create({
  button: {
    marginTop: 50,
    margin: 10, 
    backgroundColor: "#00C851"
  },
  buttonContainer: {
    flex: 1,
  },
  buttonView: {
    flexDirection: 'row',

  },
  text: {
    textAlign: 'center',
  },
})

const mapStateToProps = (state) => {
  return {
    token: state.auth.user.token,
    restaurant: state.restaurant.restaurant,
    loading: state.restaurant.loading,
  }
}

export default connect(mapStateToProps)(Map)
