import React from 'react'
import axios from 'axios'
import { connect } from 'react-redux'
import { StyleSheet} from 'react-native'
import ProgressiveImage from '../ProgressiveImage'
import { Text, Body, Thumbnail, Left, ListItem,} from 'native-base';



class BoissonDetail extends React.Component {

  state = {
    t: {}
  }

  componentDidMount() {
    const c = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token': this.props.token
      }
    }

      axios.get('https://welon.fr/api/boissons/infos/' + this.props.boisson, c)
      .then(result => {
        t = result.data
        this.setState({t: t[0]})
      })
      console.log(this.state)
  }

  render() {
    return (
      <ListItem thumbnail>
        <Left>
        <ProgressiveImage
                source={require('../../assets/drinks-min.jpg')}
                resizeMode="cover"
                style={styles.img}/>
        </Left>
        <Body>
          <Text> {this.state.t.name} </Text>
          <Text note numberOfLines={1}> {this.state.t.description}</Text>
        </Body>
      </ListItem>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    loading: state.boisson.loading
  }
}

export default connect(mapStateToProps)(BoissonDetail)


const styles = StyleSheet.create({
  
  img: {
    width: 57,
    height: 53
  }
})

