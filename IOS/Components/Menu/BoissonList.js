import React from 'react';
import { connect } from 'react-redux'

import { ActivityIndicator } from 'react-native'

import BoissonDetail from './BoissonDetail'

import { Container, ListItem, List, Text} from 'native-base';


class BoissonList extends React.Component {


  render() {


    const { loading } = this.props

    if (loading)
      return <ActivityIndicator size="small" color="#00ff00" />

      return (
        <List>

      <ListItem itemDivider>
              <Text>Boissons</Text>
          </ListItem>
          {
            this.props.boisson && this.props.boisson.map(obj => {
                return(
                    <BoissonDetail token={this.props.token} boisson={obj} key={obj}/>
                )
            })
          }
        </List>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    loading: state.boisson.loading
  }
}

export default connect(mapStateToProps)(BoissonList)
