import React from 'react';
import DessertDetail from './DessertDetail'

import {ListItem, Text, List } from 'native-base';



class DessertList extends React.Component {



  render() {

      return (
      <List>
        <ListItem itemDivider><Text>Dessert</Text></ListItem>

          {
            this.props.dessert && this.props.dessert.map(obj => {
                return(
                    <DessertDetail token={this.props.token} dessert={obj} key={obj}/>
                )
            })
          }
    </List>
    );
  }
}

export default (DessertList)
