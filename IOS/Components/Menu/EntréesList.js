import React from 'react';
import { connect } from 'react-redux'

import { ActivityIndicator } from 'react-native'

import EntréesDetail from './EntréesDetail'

import { Container, ListItem , Text, List} from 'native-base';


class EntréesList extends React.Component {


  render() {


    const {loading } = this.props

    if (loading)
      return <ActivityIndicator size="small" color="#00ff00" />

      return (
      <List >
        <ListItem itemDivider first>
              <Text>Entrées</Text>
          </ListItem>
          {
            this.props.entrées && this.props.entrées.map(obj => {

                return(
                  <EntréesDetail token={token} entrées={obj} key={obj}/>
              )
            })
          }
    </List>
    );
  }
}



export default (EntréesList)
