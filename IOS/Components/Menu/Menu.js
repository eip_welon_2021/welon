import React from 'react';
import { connect } from 'react-redux'

import MenuList from './MenuList'
import  { getAllMenu } from '../../Store/Actions/menuActions'

import { Container, Content } from 'native-base';




class Menu extends React.Component {

  static navigationOptions = {
    headerTransparent: true,
    headerStyle: {
       height: 80, // Specify the height of your custom header
     },
   }

  componentDidMount() {
    this.props.dispatch(getAllMenu({token: this.props.navigation.dangerouslyGetParent().getParam('token'),
                                   restaurant: this.props.navigation.dangerouslyGetParent().getParam('restaurant')
  
  }))
  }


  render() {
    return (
      <Container>
      <Content>
        <MenuList 
          nav={this.props.navigation}
          token={this.props.navigation.dangerouslyGetParent().getParam('token')}
          restaurant={this.props.navigation.dangerouslyGetParent().getParam('restaurant')}
          menu={this.props.menu}/>
      </Content>
    </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    token: state.auth.user.token,
    menu: state.menu.menu,
    loading: state.menu.loading
  }
}

export default connect(mapStateToProps)(Menu)
