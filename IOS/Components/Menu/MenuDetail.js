import React from 'react';
import { connect } from 'react-redux'
import { Text, Card, CardItem} from 'native-base';
import ProgressiveImage from '../ProgressiveImage'
import { TouchableRipple } from 'react-native-paper';
import { Image, StyleSheet, View } from 'react-native'


class MenuDetail extends React.Component {
  

  state = {
    visible: false
  };



  toggleModal = () => {
    this.setState({ visible: !this.state.visible });
  };


  render() {
    return (
      <TouchableRipple
      onPress={() => this.props.nav.navigate("MenuSummary", { restaurant: this.props.restaurant, 
        token: this.props.token, id: this.props.menu})}>
      <Card style={styles.card}>
        <CardItem>
        <ProgressiveImage
            source={require('../../assets/repas-min.jpg')}
            style={styles.img}
            resizeMode="cover"/>
          <View >
            <Text style={styles.name}>   {this.props.menu.name}</Text>
            <Text note numberOfLines={1} style={{marginBottom: 12, color:"#747474"}}>   {this.props.menu.description}</Text>
            <Text note numberOfLines={1} style={styles.price}>   {this.props.menu.price} €</Text>
          </View>
        </CardItem>
    </Card>    
    </TouchableRipple>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    oneMenu: state.menu.oneMenu
  }
}

const styles = StyleSheet.create({
  
  card: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 20,
    marginBottom: 20,
    width: 350
  },
  img: {
    marginTop: -10,
    marginLeft: -15,
    marginBottom:-10,
    width: 120,
    height: 110
  },
  price: {
    fontSize: 15,
    marginBottom: 12,
    color:"#c40474"
  },
  name: {
    fontSize: 15,
    marginTop: 5,
    marginBottom: 10
  },
  content: {
    height: 42,
    marginLeft: -10,
  }
})

export default connect(mapStateToProps)(MenuDetail)