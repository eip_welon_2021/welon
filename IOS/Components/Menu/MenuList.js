import React from 'react';
import { connect } from 'react-redux'

import { ActivityIndicator } from 'react-native'

import MenuDetail from './MenuDetail'

import { Container, Content, List } from 'native-base';


class MenuList extends React.Component {


  render() {


    const {loading } = this.props

    if (loading)
      return <ActivityIndicator size="small" color="#00ff00" />

      return (
      <Container style={{marginTop: 10}}>
      <Content>
        <List>
          {
            this.props.menu && this.props.menu.map(obj => {
                return(
                    <MenuDetail restaurant={this.props.restaurant} nav={this.props.nav} token={this.props.token} menu={obj} key={obj._id}/>
                )
            })
          }
        </List>
      </Content>
    </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    loading: state.menu.loading
  }
}

export default connect(mapStateToProps)(MenuList)
