import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { Card } from 'react-native-paper';

import Modal, { ModalContent, ModalTitle } from 'react-native-modals';

  
class MenuModal extends React.Component {

  state = {
    visible: false
  };

  toggleModal = () => {
    this.setState({ visible: !this.state.visible });
  };

  render() {
    return(
      <Card >
          
          <Modal
          height={100}
          modalTitle={<ModalTitle title={this.props.menu.name} />}
            visible={this.state.visible}
            onTouchOutside={this.toggleModal}>
            <ModalContent>
            <MenuModal token={this.props.token} menu={this.props.menu}/>
            </ModalContent>
          </Modal>
            <Card.Cover source={require('../../assets/repas-min.jpg')}/>
            <Card.Content>
            <Text style={styles.Text}>{this.props.menu.name}</Text>
            </Card.Content>
          </Card>
    )
  }
}

const styles = StyleSheet.create({
  
  container: {
    margin: 10,
  },

  content: {
    height: 42,
    marginLeft: -10,
  },
  Text : {
    marginTop: 10,
    fontSize: 16,
    color: '#929492'
  }
})

export default (MenuModal)
