import React from 'react';
import { connect } from 'react-redux'
import { StyleSheet } from 'react-native'
import { Content, List } from 'native-base';


import PlatList from './PlatList'
import EntréesList from './EntréesList'
import DessertList from './DessertList';
import BoissonList from './BoissonList'
import Headers from '../Drawer/Headers'

import {getMenu} from '../../Store/Actions/menuActions'


class MenuSummary extends React.Component {

  static navigationOptions = {
   }

   static navigationOptions = {
    headerTintColor: 'green'  ,
    headerStyle: {
       height: 80,
     },
     headerRight: (
       <Headers/>
 
     )
   }

  componentDidMount() {
    this.props.dispatch(getMenu({token: this.props.navigation.getParam('token'),
                                   menu: this.props.navigation.getParam('id'),
                                  restaurant: this.props.navigation.getParam('restaurant')}))
  }
  
   isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
}

  render() {

    

    token = this.props.navigation.getParam('token')
    menu = this.props.oneMenu
    let dessert, entrées, plat, boisson = null

    if (typeof(menu.id_dessert) != "undefined") {
      if (!this.isEmpty(menu.id_dessert)) {
        dessert = <DessertList token={token} dessert={menu.id_dessert}/>
      }
    }

    if (typeof(menu.id_boisson) != "undefined") {
      if (!this.isEmpty(menu.id_boisson)) {
        boisson = <BoissonList token={token} boisson={menu.id_boisson}/>
      }
    }

    if (typeof(menu.id_entree) != "undefined") {
      if (!this.isEmpty(menu.id_entree)) {
        entrées = <EntréesList token={token} entrées={menu.id_entree}/>
      }
    }

    if (typeof(menu.id_plat) != "undefined") {
      if (!this.isEmpty(menu.id_plat)) {
        plat = <PlatList token={token} plat={menu.id_plat}/>
      }
    }

      return (
      <Content>
        <List>
          {plat}
          {entrées}
          {dessert}
          {boisson}
        </List>
      </Content>
    );
  }
}

const styles = StyleSheet.create({
  
  card: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 20,
    marginBottom: 20,
    width: 350
  },container: {
    flex: 1,
   // marginTop: Constants.statusBarHeight,
  },
  scrollView: {
    marginHorizontal: 20,
  },
  img: {
    marginTop: -10,
    marginLeft: -15,
    marginBottom:-10,
    width: 120,
    height: 110
  },
  price: {
    fontSize: 15,
    marginBottom: 12
  },
  name: {
    fontSize: 15,
    marginTop: 5,
    marginBottom: 10
  },
  content: {
    height: 42,
    marginLeft: -10,
  }
})

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    oneMenu: state.menu.oneMenu
  }
}

export default connect(mapStateToProps)(MenuSummary)

/* 
              <EntréesList token={token} entrées={menu.id_entree}/>
              <PlatList token={token} plat={menu.id_plat}/>
              <DessertList token={token} dessert={menu.id_dessert}/> */
