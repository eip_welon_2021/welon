import React from 'react';
import { connect } from 'react-redux'

import PlatDetail from './PlatDetail'


import { Container, Text, ListItem, List } from 'native-base';


class PlatList extends React.Component {


  render() {

    return (
      <List >
        <ListItem itemDivider first>
              <Text>Plats</Text>
          </ListItem>
          {
            this.props.plat && this.props.plat.map(obj => {
                return(
                    <PlatDetail token={this.props.token} plat={obj} key={obj}/>
                )
            })
          }
    </List>
    );
  }
}

export default (PlatList)
