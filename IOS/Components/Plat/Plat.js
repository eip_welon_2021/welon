import React from 'react';
import { connect } from 'react-redux'

import PlatList from './PlatList'
import { getAllPlat } from '../../Store/Actions/platActions'

import { Container, Content } from 'native-base';


class Plat extends React.Component {


  componentDidMount() {

   this.props.dispatch(getAllPlat({token: this.props.navigation.dangerouslyGetParent().getParam('token'),
                                   restaurant: this.props.navigation.dangerouslyGetParent().getParam('restaurant')._id
  
  }))
  }

  render() {
    return (
      <Container>
      <Content>
        <PlatList plat={this.props.plat}/>
      </Content>
    </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    token: state.auth.user.token,
    plat: state.plat.plat,
    loading: state.restaurant.loading
  }
}

export default connect(mapStateToProps)(Plat)
