import React from 'react';
import { connect } from 'react-redux'

import PlatDetail from './PlatDetail'
import { ActivityIndicator } from 'react-native'


import { Container, Content, List } from 'native-base';


class PlatList extends React.Component {


  render() {


    const {loading } = this.props

    if (loading)
      return <ActivityIndicator size="small" color="#00ff00" />
      
    return (
      <Container style={{marginTop: 10}}>
      <Content>
          {
            this.props.plat && this.props.plat.map(obj => {
                return(
                    <PlatDetail plat={obj} key={obj._id}/>
                )
            })
          }
      </Content>
    </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    logout: state.auth.logout,
    error: state.auth.authError,
    token: state.auth.user.token,
    plat: state.plat.plat,
    loading: state.restaurant.loading
  }
}

export default connect(mapStateToProps)(PlatList)
