import React from 'react'
import { connect } from 'react-redux'
import { historique } from '../../Store/Actions/FactureActions'
import HistoryList from './HistoryList'
import Headers from '../Drawer/Headers'

import { View, AsyncStorage, Text } from 'react-native'




class Profil extends React.Component {


  static navigationOptions = {
    headerStyle: {
       height: 80,
     },
     title: "Historique",     
     headerRight: (
       <Headers/>
 
     )
   }

  componentDidMount () {
    this.props.dispatch(historique({token: this.props.navigation.state.params.token}));
  }

    render() {

        return (
            <View style={{marginTop: 10}}>
                  <HistoryList history={this.props.history} nav={this.props} token={this.props.navigation.state.params.token}/>
            </View> 
                            
        )
    }
}

const mapStateToProps = (state) => {
  return {
    history: state.command.history,
  }
}

export default connect(mapStateToProps)(Profil)
