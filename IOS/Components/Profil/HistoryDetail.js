import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native'
import { ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import Modal, {  ModalTitle } from 'react-native-modals';
import { getCommand } from '../../Store/Actions/FactureActions'
import Summary from './HistorySummary/Summary'


class HistoryDetail extends React.Component {

  state = {
    visible: false
  }

  static navigationOptions = {
    headerStyle: {
       height: 80,
       backgroundColor: 'green',
       title: 'Screen Title',
       headerTintColor: 'royalblue',
     },
     headerTitleStyle: {color:'white'},
   }

  toggleModal = () => {
    this.setState({ visible: !this.state.visible });
  };
  

  render() {

    const tmp = new Date(this.props.history.created_at);

    const date =  tmp.getDate() + '/' + parseInt(tmp.getMonth() + 1) + '/' + tmp.getFullYear()
    const heure = tmp.getHours() + 'h' + tmp.getMinutes()

    return (
      <ListItem >
        <Body>
          <TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = { this.toggleModal }>
           <Text note numberOfLines={1} style={styles.text}> {date} à {heure}  </Text>
          </TouchableOpacity>
        </Body>
        <Modal
          height={300}
          width={300}
          modalTitle={<ModalTitle title={"Recapitulatif"}/>}
          visible={this.state.visible}
          onTouchOutside={() => {this.setState({ visible: false });}}>
          <Summary token={this.props.token} history={this.props.history}/>
        </Modal>
      </ListItem>
    );
  }
}

const styles = StyleSheet.create({
  img: {
    width: 57,
    height: 53
  },
  text: {
    color: "#5c5c79", 
    fontWeight: 'bold', 
    fontSize: 20,
    margin: 10
},
})


export default (HistoryDetail)
