import React from 'react';

import HistoryDetail from './HistoryDetail'

import {  List } from 'native-base';
import { connect } from 'react-redux';


class HistoryList extends React.Component {

  static navigationOptions = {
    headerStyle: {
       height: 80,
       backgroundColor: 'green',
       title: 'Screen Title',
       headerTintColor: 'royalblue',
     },
     headerTitleStyle: {color:'white'},
   }

  render() {

      return (
      <List>
          {
            this.props.history && this.props.history.map(obj => {
                return(
                  <HistoryDetail token={this.props.token} history={obj} key={obj._id} nav={this.props.nav}/>
              )
            })
          }
    </List>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    history: state.command.history,
  }
}

export default connect(mapStateToProps)(HistoryList)
