import React from 'react';
import axios from 'axios'
import { connect } from 'react-redux'
import { StyleSheet } from 'react-native'
import ProgressiveImage from '../../ProgressiveImage'
import { ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';

class BoissonRequest extends React.Component {

  state = {
    t: {}
  }
  
  componentDidMount() {
    const c = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token': this.props.token
      }
    }

      axios.get('https://welon.fr/api/boissons/infos/' + this.props.boisson[0], c)
      .then(result => {
        t = result.data
        this.setState({t: t[0]})
      })
  }

  render() {

    return (
        <ListItem>
           <Body>
          <Text> x{this.props.boisson[1]} {this.state.t.name} </Text>
          <Text note numberOfLines={1} style={{width: 300,}}> {this.state.t.description}</Text>
          <Text> Prix : {this.state.t.price} €</Text>

        </Body>
       </ListItem>
    );
  }
}

const styles = StyleSheet.create({
  img: {
    width: 57,
    height: 53
  }
})

const mapStateToProps = (state) => {
  return {
    error: state.plat.oneError,
    loading: state.plat.oneLoading
  }
}

export default connect(mapStateToProps)(BoissonRequest)
