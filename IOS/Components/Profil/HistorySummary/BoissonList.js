import React from 'react';

import BoissonDetail from './BoissonDetail'

import { List } from 'native-base';


class BoissonList extends React.Component {


  render() {

      return (
      <List>
          {
            this.props.boissons && this.props.boissons.map(obj => {
                return(
                  <BoissonDetail token={this.props.token} boisson={obj} key={obj[0]}/>
              )
            })
          }
    </List>
    );
  }
}


export default (BoissonList)
