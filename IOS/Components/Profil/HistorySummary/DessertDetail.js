import React from 'react';
import axios from 'axios'
import { StyleSheet } from 'react-native'
import ProgressiveImage from '../../ProgressiveImage'
import { ListItem, Text, Left, Body, Right, Button } from 'native-base';



class DessertRequest extends React.Component {

  state = {
    t: {}
  }

  componentDidMount() {
    const c = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token': this.props.token
      }
    }
      axios.get('https://welon.fr/api/desserts/infos/' + this.props.dessert[0], c)
      .then(result => {
        t = result.data
        this.setState({t: t[0]})
      })
  }

  render() {

    console.log(this.props)

    return (
       <ListItem>
           <Body>
          <Text> x{this.props.dessert[1]} {this.state.t.name} </Text>
          <Text note numberOfLines={1} style={{width: 300,}}> {this.state.t.description}</Text>
          <Text> Prix : {this.state.t.price} €</Text>

        </Body>
       </ListItem>
        
    );
  }
}

const styles = StyleSheet.create({
  img: {
    width: 57,
    height: 53
  }
})

export default (DessertRequest)
