import React from 'react';
import axios from 'axios'
import { StyleSheet } from 'react-native'
import { ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';

class MenuRequest extends React.Component {

  state = {
    t: {}
  }

  
  componentDidMount() {

    const c = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token': this.props.token
      }
    }
    axios.get('https://welon.fr/api/menu/' + this.props.menu[0], c)
    .then(result => {
        t = result.data
        this.setState({t: t[0]})
      }).catch(error => {
          console.log(error)
      })
  }

  render() {

    return (
      <ListItem >
       
        <Body>
          <Text> x{this.props.menu[1]} {this.state.t.name} </Text>
          <Text note numberOfLines={1} style={{width: 300,}}> {this.state.t.description}</Text>
          <Text> Prix : {this.state.t.price} €</Text>

        </Body>
      </ListItem>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    error: state.plat.oneError,
    onePlat: state.plat.onePlat,
    loading: state.plat.oneLoading
  }
}

const styles = StyleSheet.create({
  img: {
    width: 57,
    height: 53
  }
})


export default (MenuRequest)
