import React from 'react';

import MenuDetail from './MenuDetail'

import { List } from 'native-base';


class MenuList extends React.Component {


  render() {

      return (
      <List>
          {
            this.props.menu && this.props.menu.map(obj => {
                return(
                  <MenuDetail token={this.props.token} menu={obj} key={obj}/>
              )
            })
          }
    </List>
    );
  }
}


export default (MenuList)
