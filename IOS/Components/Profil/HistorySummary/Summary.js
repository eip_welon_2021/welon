import React from 'react';
import axios from 'axios'
import { connect } from 'react-redux'
import { StyleSheet, SafeAreaView, ScrollView } from 'react-native'
import { ListItem, Thumbnail, Text, Left, Body, Right, Button, Container } from 'native-base';
import Modal, { ModalContent, ModalTitle } from 'react-native-modals';

import MenuList from './MenuList'
import PlatList from './PlatList';
import DessertList from './DessertList';
import BoissonList from './DessertList';
import EntréesList from './EntréesList'




class Summary extends React.Component {

  state = {
    visible: false
  }

  static navigationOptions = {
    headerStyle: {
       height: 80,
       backgroundColor: 'green',
       title: 'Screen Title',
       headerTintColor: 'royalblue',
     },
     headerTitleStyle: {color:'white'},
   }

 

  render() {
   
    return (
      <SafeAreaView style={styles.scrollView}>
            <ScrollView style={styles.scrollView} >
          <ModalContent>
              {
                  this.props.history.desserts.length > 0 ? 
                  <DessertList token={this.props.token} desserts={this.props.history.desserts}/>
                  :
                  null
              }
              {
                  this.props.history.boissons.length > 0 ? 
                  <BoissonList token={this.props.token} boissons={this.props.history.boissons}/>
                  :
                  null
              }
              {
                  this.props.history.entrees.length > 0 ? 
                  <EntréesList token={this.props.token} entrées={this.props.history.entrees}/>
                  :
                  null
              }
              {
                  this.props.history.plats.length > 0 ? 
                  <PlatList token={this.props.token} plat={this.props.history.plats}/>
                  :
                  null
              }
              {
                  this.props.history.menus.length > 0 ? 
                  <MenuList token={this.props.token} menu={this.props.history.menus}/>
                  :
                  null
              }            
            </ModalContent>
        </ScrollView>
      </SafeAreaView>
        
        
      
    );
  }
}

const styles = StyleSheet.create({
  img: {
    width: 57,
    height: 53
  },
  text: {
    color: "#5c5c79", 
    fontWeight: 'bold', 
    fontSize: 20,
    margin: 10
  },
  container: {
    flex: 1,
    marginTop: 20
  },
  scrollView: {
    marginHorizontal: 20,
    flex: 1,
  },
})


export default (Summary)
