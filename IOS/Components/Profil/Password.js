import React from 'react'
import {
    Text,
    View,
    Image,
    Button,
    Keyboard,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native'
import { connect } from 'react-redux'
import { updatePwd } from '../../Store/Actions/authActions';
import Headers from '../Drawer/Headers'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'



const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      {children}
    </TouchableWithoutFeedback>
  )

class SignIn extends React.Component {

  static navigationOptions = {
    headerStyle: {
       height: 80,
     },
     title: "Mot de passe",
     headerRight: (
      <Headers/>

    )
   }

  state = {
    hidePassword1: true,
    hidePassword2: true,
    hidePassword3: true,

    new_password: '',
    old_password: '',
    new_passwordBis: '',
    pwdError: ''
  }
    

  onUpdate = () => {
    const obj = {
      new_password: this.state.new_password,
      old_password: this.state.old_password,
      email: this.props.navigation.state.params.email
    }
    if (this.state.new_password == this.state.new_passwordBis){
        this.props.updatePwd(obj)
    }
    else {
      this.setState({
        pwdError: "Les mots de doivent être identiques"
      })
    }
  }

  managePasswordVisibility1 = () => {
    this.setState({ hidePassword1: !this.state.hidePassword1})
  }

  managePasswordVisibility2 = () => {
    this.setState({ hidePassword2: !this.state.hidePassword2})
  }

  managePasswordVisibility3 = () => {
    this.setState({ hidePassword3: !this.state.hidePassword3})
  }


    render() {

        return (
            <DismissKeyboard>
                <KeyboardAwareScrollView 
                  resetScrollToCoords={{ x: 0, y: 0 }}
                  contentContainerStyle={styles.container}
                  scrollEnabled={false}>
                  <View style={styles.container}>
                    <Image
                    resizeMode="contain"
                      style={styles.headingImage}
                      source={require('../assets/logo_green.png')}
                    />
                    <View style={styles.inputStyle}>                    
                      <View>
                        <View>
                          <TextInput
                                type='password'
                                autoCorrect={false}
                                style={styles.textInput}
                                placeholder="Ancien Mot de passe"
                                placeholderTextColor="#a0a0a0"
                                secureTextEntry = { this.state.hidePassword1}
                                onChangeText={(text) => {this.setState(() => {
                                  return {
                                    old_password: text
                                  }
                                })} }
                            />
                            <TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = { this.managePasswordVisibility1 }>
                          <Image source = { ( this.state.hidePassword1 ) ? require('../assets/hide_password.png') : require('../assets/show-password.jpg') } style = { styles.btnImage } />
                        </TouchableOpacity>
                        </View>
                        <View>
                          <TextInput
                          type='password'
                          autoCorrect={false}
                          style={styles.textInput}
                          placeholder="Nouveau Mot de passe"
                          placeholderTextColor="#a0a0a0"
                          secureTextEntry = { this.state.hidePassword2}
                          onChangeText={(text) => {this.setState(() => {
                            return {
                              new_passwordBis: text
                            }
                          })} }
                        />
                        <TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = { this.managePasswordVisibility2 }>
                          <Image source = { ( this.state.hidePassword2 ) ? require('../assets/hide_password.png') : require('../assets/show-password.jpg') } style = { styles.btnImage } />
                        </TouchableOpacity>
                        </View>
                        <View>
                          <TextInput
                              type='password'
                              autoCorrect={false}
                              autoCapitalize='none'
                              style={styles.textInput}
                              placeholder="Confirmez Mot de passe"
                              placeholderTextColor="#a0a0a0"
                              secureTextEntry = { this.state.hidePassword3}
                              onChangeText={(text) => {this.setState(() => {
                                return {
                                  new_password: text
                                }
                              })} }
                          />
                        <TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = { this.managePasswordVisibility3 }>
                          <Image source = { ( this.state.hidePassword3 ) ? require('../assets/hide_password.png') : require('../assets/show-password.jpg') } style = { styles.btnImage } />
                        </TouchableOpacity>
                        </View>
                        
                      </View>

                      {this.state.pwdError ?
                        <Text style={styles.error}> {this.state.pwdError} </Text>
                        :
                        null
                      }
                      { this.props.error ?
                          (
                            <Text style={styles.error}> {this.props.error} </Text>
                            
                          )
                          :
                          (
                            <Text></Text>
                          )
                        }
                    </View>
                    <View style={styles.bottom}>
                      <View style={styles.button}>
                        <Button 
                          title="Modifier mot de passe"
                          color="#FFFFFF"
                          onPress={this.onUpdate}
                        />
                      </View>
                    </View>
                </View>

          </KeyboardAwareScrollView>
            </DismissKeyboard>
                

        )
    }
}


const mapStateToProps = (state) => {
  return {
    update: state.auth.update,
    error: state.auth.updateError,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updatePwd: (text) => {dispatch(updatePwd(text))}
   }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-around',
    },
    inputStyle : {
      marginTop: 150,
      marginBottom: 300
    },
    button: {
      marginTop: 50,
      margin: 10, 
      backgroundColor: "#00C851"
    },
    greeting: {
      color: 'black',
      fontWeight: 'bold',
      textAlign: 'center',
      fontSize: 45,
      marginTop: 100
    },
    bottom: {
      marginBottom: 200,
    },
    headingImage: {
      width: 200,
      height: 259,
      marginTop: 100
    },
    headingImage2: {
      width: 300,
      height: 459,
      marginTop: 90,
      marginBottom: 100
    },
    error: {
      color: 'red',
      textAlign: 'center',
      width: 250,
    },
    textInput: {
        height: 45,
        width: 250,
        marginBottom: 15,
        borderBottomWidth: 1.5,
        fontSize: 16,
        borderBottomColor: '#00C851',
    },
    visibilityBtn:
  {
    position: 'absolute',
    right: 3,
    height: 40,
    width: 35,
    padding: 5
  },
 
  btnImage:
  {
    resizeMode: 'contain',
    height: '100%',
    width: '100%'
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(SignIn)