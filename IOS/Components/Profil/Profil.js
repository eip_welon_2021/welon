import React from 'react'
import { connect } from 'react-redux'
import Headers from '../Drawer/Headers'
import { List, ListItem, Text, Left, Body, Right} from 'native-base';
import { deleteUser, logout, me } from '../../Store/Actions/authActions'
import Modal, { ModalContent, ModalTitle, ModalButton, ModalFooter } from 'react-native-modals';
import { View, AsyncStorage, Button, StyleSheet} from 'react-native'

class Profil extends React.Component {

  state = 
  ({
    token: '',
    progress: 20,
    progressWithOnComplete: 0,
    progressCustomized: 0,
    exp: 35,
    visibility: false
  })

  increase = (key, value) => {
    this.setState({
      [key]: this.state[key] + value,
    });
  }

  toggleModal = () => {
    this.setState({visibility: !this.state.visibility});
  };

  static navigationOptions = {
   headerStyle: {
      height: 80,
    },
    headerLeft: (
      <Text style={{fontSize:28, 
        marginLeft: 20, 
        paddingTop: 20, 
        color: '#929492'}}>Profil</Text>
    ),
    headerRight: (
      <Headers/>

    )
  }

  componentDidMount () {

    AsyncStorage.getItem('token').then((result) => {
        if (result) {          
          this.setState({
            token: result
          })
            this.props.getUser({token: result})
        } else {
          this.setState({
            token: this.props.token
          })
          this.props.getUser({token: this.props.token})

        }
      })

  }

  modifier = () => {
    this.props.navigation.navigate('Modifier', { token: this.state.token, email: this.props.user.email})
  }

  historique = () => {
    this.props.navigation.navigate('Historique', { token: this.state.token})
  }

  
  logout = async () => {
    this.toggleModal()
    await AsyncStorage.multiRemove(['token'])
    this.props.Logout(this.state.token)
    this.props.navigation.navigate('Logout')

  }

  delete = async() => {
    
    await AsyncStorage.multiRemove(['token'])
    this.props.navigation.navigate('Delete', { token: this.state.token, email: this.props.user.email})
  };

    render() {

        return (
            
            <View style={styles.container}>      
              <List>

                <ListItem>
                  <Body>
                    <Text>Nom</Text>
                    <Text note>{this.props.user.firstname} </Text>
                  </Body>
                </ListItem>

                <ListItem>
                  <Body>
                    <Text>Prénom</Text>
                    <Text note>{this.props.user.lastname} </Text>
                  </Body>
                </ListItem>
          
                <ListItem>
                  <Body>
                    <Text>Email</Text>
                    <Text note>{this.props.user.email} </Text>
                  </Body>
                </ListItem>

                <ListItem>
                  <Body>
                    <Text>Mot de passe </Text>
                    <Text note> ******* </Text>
                  </Body>
                  <Right>
                    <Button  onPress={this.modifier} title=">"/>
                  </Right>
                </ListItem>

                <ListItem>
                  <Left>
                  <Button color="#3dd095" onPress={this.historique}title="Historique"/>
                  </Left>
                </ListItem>

                <ListItem>
                  <Left>
                  <Button  title="Déconnexion" color="#db873d" onPress={this.toggleModal}/>
                  </Left>
                </ListItem>


                <ListItem>
                  <Left>
                  <Button color="#b93455" onPress={this.delete} title="Suppression compte"/>
                  </Left>
                </ListItem>
                
            
              </List>
              <Modal
                height={160}
                width={250}
                modalTitle={<ModalTitle title="Déconnexion"/>}
                visible={this.state.visibility}
                onTouchOutside={this.toggleModal}
                footer={
                  <ModalFooter>
                    <ModalButton
                      text="Oui"
                      onPress={this.logout}
                    />
                    <ModalButton
                      text="Non"
                      onPress={this.toggleModal}
                    />
                  </ModalFooter>
                }>
              <ModalContent style={{margin: 10}}>
                <Text style={{ alignSelf: 'center'}}> Confirmer la déconnexion </Text>
              </ModalContent>
              </Modal>
              
            </View> 
                            
        )
    }
}

const mapStateToProps = (state) => {
  return {
    user: state.auth.me,
    token: state.auth.user.token,
    deleted: state.auth.deleteSuccess,
    logIn: state.auth.logIn,
    logout: state.auth.logout,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteUser: (text) => {dispatch(deleteUser(text))},
    Logout: (text) => {dispatch(logout(text))},
    getUser: (obj) => {dispatch(me(obj))}
   }
}

const styles = StyleSheet.create({
    avatar: {
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
      flex: 1,
      backgroundColor: '#FFF',
      marginTop: 50,
      padding: 15,
    },
    buttonContainer: {
      marginTop: 40,
    },
    separator: {
      marginVertical: 30,
      borderWidth: 0.5,
      borderColor: '#DCDCDC',
    },
    label: {
    alignItems: 'center',
      color: '#999',
      fontSize: 14,
      fontWeight: '500',
      marginBottom: 10,
    },
    inputStyle : {
        marginBottom: 200
    },
      textInput: {
          height: 45,
          width: 250,
          marginBottom: 15,
          borderBottomWidth: 1.5,
          fontSize: 16,
          borderBottomColor: '#00C851',
    },
    tinyLogo: {
      width: 20,
      height: 20,
    }
  });

export default connect(mapStateToProps, mapDispatchToProps)(Profil)
