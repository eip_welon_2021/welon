import React from 'react'
import {
    Text,
    View,
    Image,
    StyleSheet
} from 'react-native'
import { AnimatedCircularProgress } from 'react-native-circular-progress';

import { Separator, Container, Header, Content, List, ListItem, Left, Body, Right} from 'native-base';


class Reward extends React.Component {


    static navigationOptions = {
        headerStyle: {
           height: 80,
           backgroundColor: 'green',
           title: 'Screen Title',
           headerTintColor: 'royalblue',
         },
         headerTitleStyle: {color:'white'},
       }

  state = ({
    exp: 80,
  })

  componentDidMount() {
      this.setState({
          exp: this.props.navigation.state.params.exp
      })
  }

    render() {

        const r = 100 - parseInt(this.state.exp)

        return (
                <View style={styles.container}>
                  <List>

                        <View >
                        <Text style={{marginLeft: 110, marginTop:20, fontWeight: 'bold', marginBottom: 10, fontSize: 30}}> Expérience </Text>

                            <AnimatedCircularProgress
                            style={{marginLeft: 120}}
                            size={150}
                            width={15}
                            fill={this.state.exp}
                            tintColor="#0cc857"
                            onAnimationComplete={() => console.log('onAnimationComplete')}
                            backgroundColor="#8a8195" />
                            <Text style={{marginLeft: 150, marginTop:20, fontWeight: 'bold'}}> { this.state.exp } sur 100 Pts</Text>
                        </View>

                    <ListItem>
                        <View >
                            
                           <Text style={{color: "#8cc494", width: 350, textAlign: 'center'}}> Gagnez {r} points supplémentaires pour gagnez une récompense et passer au niveau suivant </Text>
                        </View>
                    </ListItem>

                    <ListItem>
                        <View >
                            <Text style={styles.list_Title}> Comment gagner des points </Text>
                            <Text style={styles.text}> Passez des commandes dans des restaurants </Text>
                            <Text style={styles.text}> Scanner le QR associé à votre commande </Text>
                            <Text style={styles.text}> Effectuer une notation </Text>
                            <Text style={styles.text}> Pour chaque notation effectuées, cumulez un point </Text>
                        </View>
                    </ListItem>

                    <ListItem>
                        <View >
                            <Text style={styles.list_Title}> Vos prochains avantages </Text>
                            <Text style={styles.text}> 10% de réduction dans l'un de nos restaurants partenaires </Text>
                            <Text style={styles.text}> Un boisson offerte sur les menu du midi </Text>
                            <Text style={styles.text}> 10% de réduction sur un menu pour deux personnes </Text>
                            <Text style={styles.text}> Dessert offert sur le menu du soir </Text>
                        </View>
                    </ListItem>
                  </List>
                    
                    
                </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-around',
    },
    list_Title: {
        color: "#5c5c79", 
        fontWeight: 'bold', 
        fontSize: 20,
        margin: 10
    },
    text: {
        margin: 5,
        width: 350
    }
})


export default (Reward)