import React from 'react'
import RestaurantSummary from './RestaurantSummary'
import { StyleSheet,View,SafeAreaView, ScrollView, RefreshControl } from 'react-native'
import Constants from 'expo-constants';
import { Button } from 'react-native-paper';


function wait(timeout) {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}

export default function RestaurantList({restaurant, token, tab}) {

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    wait(2000).then(() => setRefreshing(false));
  }, [refreshing]);


  return(
    <SafeAreaView style={styles.container}>
    <ScrollView 
      
      styles={styles.scrollView}>
      <View >
          { tab && tab.map(obj => {
              return (                    
                    <RestaurantSummary 
                    token={token} 
                    nav={restaurant} 
                    restaurant={obj} 
                    key={obj._id}/>
              )
          })}
      </View>
    </ScrollView>
  </SafeAreaView> 
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 40,

  },
  scrollView: {

    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

