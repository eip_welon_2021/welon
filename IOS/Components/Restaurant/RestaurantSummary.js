import React, { Component } from 'react'
import ProgressiveImage from '../ProgressiveImage'
import { Text, StyleSheet, View, TouchableOpacity, Image } from 'react-native'
import Location from '../Location'
import { TouchableRipple } from 'react-native-paper';


class RestaurantSummary extends Component {

  render () {

    console.log(this.props.restaurant)

    let uri = null

    if (this.props.restaurant.img)
      uri = this.props.restaurant.img 
    
      console.log(!isNaN(parseInt(this.props.restaurant.rank)))
    return (
      <TouchableRipple
        onPress={() => this.props.nav.navigation.navigate("tab", { restaurant: this.props.restaurant, 
        token: this.props.token, uri: uri})}>
        <View style={styles.container}>
          <View style={styles.content}>
            {
              uri ?
                <ProgressiveImage
                  source={{ uri: uri }}
                  resizeMode="cover"
                  style={styles.img}/>
              :
                <ProgressiveImage
                  source={require('../../assets/restaurant.jpg')}
                  resizeMode="cover"
                  style={styles.img}/>
            }
          </View>
          <View style={{padding: 10, width:400, alignItems: "center" }}>
            {
              this.props.restaurant.rank && !isNaN(parseInt(this.props.restaurant.rank))
               ? 
              <Image source = { require('../../assets/fork3.png')} style = { styles.btnImage } />
              :
              null
            }
            {
              this.props.restaurant.rank && !isNaN(parseInt(this.props.restaurant.rank))
              ?
              <Text style={{fontWeight: 'bold', textAlign: 'center',}}> {this.props.restaurant.rank} {this.props.restaurant.name} </Text>
              :
              <Text style={{fontWeight: 'bold', textAlign: 'center',}}> {this.props.restaurant.name} </Text>

            }
            <Text style={styles.Text}> {this.props.restaurant.address}  </Text>
          </View>
        </View>         
      </TouchableRipple>
    )
  }
}

const styles = StyleSheet.create({
  
  container: {
    flex: 1, 
    alignItems: "center", 
    justifyContent: "center",
    margin: 5
  },
  img: {
    width: 360,
    height: 228,
  },
  content: {
    backgroundColor: "#eee", 
    borderRadius: 10, 
    overflow: "hidden",
    height: 228,
    margin: 5,
  },
  Text : {
    marginBottom: 5,
    textAlign: 'center',
    justifyContent: "center",
    fontSize: 16,
    color: '#929492'
  },
  card: {
    alignSelf: 'center',
    flex: 1,
    margin: 10,
    height: 260,
    width: 340,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
    backgroundColor: '#ffffff'
  },

  btnImage:
  {
    resizeMode: 'contain',
    height: '15%',
    width: '20%'
  }
})

export default RestaurantSummary

