import React from 'react';

import BoissonDetail from './BoissonDetail'

import { ListItem, Text, List } from 'native-base';


class BoissonList extends React.Component {


  render() {

      return (
      <List>
        <ListItem itemDivider first>
              <Text>Boissons</Text>
          </ListItem>
          {
            this.props.boisson && this.props.boisson.map(obj => {
                return(
                  <BoissonDetail token={this.props.token} boisson={obj} key={obj._id}/>
              )
            })
          }
    </List>
    );
  }
}


export default (BoissonList)
