import React from 'react';
import axios from 'axios'
import { connect } from 'react-redux'
import StarRating from 'react-native-star-rating';
import { StyleSheet } from 'react-native'
import ProgressiveImage from '../ProgressiveImage'
import { rateDessert } from '../../Store/Actions/dessertActions'
import { ListItem, Thumbnail, Text, Left, Body, Right, Button, View } from 'native-base';
import Modal, { ModalFooter, ModalButton, ModalContent, ModalTitle } from 'react-native-modals'



class DessertRequest extends React.Component {

  state = {
    t: {},
    visible: false,
    quality: 0,
    quantity: 0,
    price: 0,
  }

  toggleModal = () => {
    this.setState({ visible: !this.state.visible });
  };

  onQuality(rating) {
    this.setState({
      quality: rating
    });
  }

  onQuantity(rating) {
    this.setState({
      quantity: rating
    });
  }

  onPrice(rating) {
    this.setState({
      price: rating
    });
  }
  
  ratingCompleted(rating) {
    console.log("Rating is: " + rating)
  }

  rate = () => {
    obj = {
      quantity: this.state.quantity,
      quality: this.state.quality,
      price: this.state.price,
      id: this.props.dessert._id,
      token: this.props.token
    }
    this.props.rateDessert(obj)
    this.setState({
      visible: false
    })
    let moy = (parseInt(this.state.quality)  + parseFloat(this.state.quantity)  + parseInt(this.state.price)) / 3
    alert("Votre notation de " + Number.parseFloat(moy).toFixed(1) + "/5 a bien été envoyé ", "ok")
  }

  componentDidMount() {
    const c = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token': this.props.token
      }
    }
      axios.get('https://welon.fr/api/desserts/infos/' + this.props.dessert._id, c)
      .then(result => {
        t = result.data
        this.setState({t: t[0]})
      })
  }

  render() {

    return (
      <ListItem thumbnail>
        <Left>
        <ProgressiveImage
                source={require('../../assets/dessert-min.jpeg')}
                resizeMode="cover"
                style={styles.img}/>
        </Left>
        <Body>
          <Text> {this.state.t.name} </Text>
          <Text note numberOfLines={1}> {this.state.t.description}</Text>
        </Body>
        <Right>
          <Button transparent>
            <Text style={[{color: '#00C851'}]} onPress={this.toggleModal} >Notez</Text>
          </Button>
        </Right>
        <Modal
          width={250}
          modalTitle={<ModalTitle title={this.props.dessert.name}/>}
          visible={this.state.visible}
          onTouchOutside={() => {this.setState({ visible: false });}}
          footer={
            <ModalFooter>
              <ModalButton
                text="Envoyer"
                onPress={this.rate}
              />
            </ModalFooter>
          }>
          <ModalContent>
            <Text style={{marginTop: 10}}>Qualité</Text>
              <StarRating
              starSize={20}
              disabled={false}
              maxStars={5}
              emptyStar={require('../../assets/fork.png')}
              fullStar={require('../../assets/fork3.png')}
              fullStarColor={'green'}
              rating={this.state.quality}
              selectedStar={(rating) => this.onQuality(rating)}/>      
            <Text style={{marginTop: 10}}>Quantité</Text>
              <StarRating
              starSize={20}
              disabled={false}
              maxStars={5}
              emptyStar={require('../../assets/fork.png')}
              fullStar={require('../../assets/fork3.png')}
              rating={this.state.quantity}
              fullStarColor={'green'}
              selectedStar={(rating) => this.onQuantity(rating)}/>
            <Text style={{marginTop: 10}}>Prix</Text>
              <StarRating
              starSize={20}
              disabled={false}
              maxStars={5}
              emptyStar={require('../../assets/fork.png')}
              fullStar={require('../../assets/fork3.png')}
              fullStarColor={'green'}
              rating={this.state.price}
              selectedStar={(rating) => this.onPrice(rating)}/>
          </ModalContent>
        </Modal>
      </ListItem>
    );
  }
}

const styles = StyleSheet.create({
  img: {
    width: 57,
    height: 53
  }
})

const mapStateToProps = (state) => {
  return {
    error: state.plat.oneError,
    onePlat: state.plat.onePlat,
    loading: state.plat.oneLoading
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    rateDessert: (text) => {dispatch(rateDessert(text))}
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(DessertRequest)
