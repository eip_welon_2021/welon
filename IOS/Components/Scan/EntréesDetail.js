import React from 'react'
import axios from 'axios'
import { connect } from 'react-redux'
import { StyleSheet } from 'react-native'
import ProgressiveImage from '../ProgressiveImage'
import StarRating from 'react-native-star-rating';
import { rateEntrées } from '../../Store/Actions/entréesActions'
import { ListItem, Text, Left, Body, Right, Button, View } from 'native-base';
import Modal, { ModalFooter, ModalButton, ModalContent, ModalTitle } from 'react-native-modals'


class EntréesRequest extends React.Component {

  state = {
    t: {},
    visible: false,
    quality: 0,
    quantity: 0,
    price: 0,
  }

  toggleModal = () => {
    this.setState({ visible: !this.state.visible });
  };

  onQuality(rating) {
    this.setState({
      quality: rating
    });
  }

  onQuantity(rating) {
    this.setState({
      quantity: rating
    });
  }

  onPrice(rating) {
    this.setState({
      price: rating
    });
  }
  
  ratingCompleted(rating) {
    console.log("Rating is: " + rating)
  }

  rate = () => {
    obj = {
      quantity: this.state.quantity,
      quality: this.state.quality,
      price: this.state.price,
      id: this.props.entrées._id,
      token: this.props.token
    }
    this.props.rateEntrées(obj)
    this.setState({
      visible: false
    })
    let moy = (parseInt(this.state.quality)  + parseFloat(this.state.quantity)  + parseInt(this.state.price)) / 3
    alert("Votre notation de " + Number.parseFloat(moy).toFixed(1) + "/5 a bien été envoyé ", "ok")
  }

  componentDidMount() {
    const c = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token': this.props.token
      }
    }

    console.log(this.props.token)
      axios.get('https://welon.fr/api/entrees/infos/' + this.props.entrées._id, c)
      .then(result => {

        t = result.data
        this.setState({t: t[0]})
        
      })
  }

  render() {

    return (
      <ListItem thumbnail>
        <Left>
        <ProgressiveImage
                source={require('../../assets/appetizer-min.jpg')}
                resizeMode="cover"
                style={styles.img}/>
        </Left>
        <Body>
          <Text> {this.state.t.name} </Text>
          <Text note numberOfLines={1}> {this.state.t.description}</Text>
        </Body>
        <Right>
          <Button transparent>
            <Text style={[{color: '#00C851'}]} onPress={this.toggleModal} >Notez</Text>
          </Button>
        </Right>
        <Modal
          width={250}
          modalTitle={<ModalTitle title={this.props.entrées.name}/>}
          visible={this.state.visible}
          onTouchOutside={() => {this.setState({ visible: false });}}
          footer={
            <ModalFooter>
              <ModalButton
                text="Envoyer"
                onPress={this.rate}
              />
            </ModalFooter>
          }>
          <ModalContent>
            <Text style={{marginTop: 10}}>Qualité</Text>
              <StarRating
              starSize={20}
              disabled={false}
              maxStars={5}
              emptyStar={require('../../assets/fork.png')}
              fullStar={require('../../assets/fork3.png')}
              fullStarColor={'green'}
              rating={this.state.quality}
              selectedStar={(rating) => this.onQuality(rating)}/>      
            <Text style={{marginTop: 10}}>Quantité</Text>
              <StarRating
              starSize={20}
              disabled={false}
              maxStars={5}
              emptyStar={require('../../assets/fork.png')}
              fullStar={require('../../assets/fork3.png')}
              rating={this.state.quantity}
              fullStarColor={'green'}
              selectedStar={(rating) => this.onQuantity(rating)}/>
            <Text style={{marginTop: 10}}>Prix</Text>
              <StarRating
              starSize={20}
              disabled={false}
              maxStars={5}
              emptyStar={require('../../assets/fork.png')}
              fullStar={require('../../assets/fork3.png')}
              fullStarColor={'green'}
              rating={this.state.price}
              selectedStar={(rating) => this.onPrice(rating)}/>
          </ModalContent>
        </Modal>
      </ListItem>
    );
  }
}
const styles = StyleSheet.create({
    img: {
      width: 57,
      height: 53
    }
})


const mapStateToProps = (state) => {
  return {
    error: state.entrées.oneError,
    loading: state.entrées.oneloading,
    entrée: state.entrées.oneEntrées
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    rateEntrées: (text) => {dispatch(rateEntrées(text))}
   }
}
export default connect(mapStateToProps, mapDispatchToProps)(EntréesRequest)
