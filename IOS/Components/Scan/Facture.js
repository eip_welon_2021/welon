import React from 'react';
import { connect } from 'react-redux'
import { Container, Content, List} from 'native-base';
import { ListItem, Text} from 'native-base';
import { getCommand, linkCmd } from '../../Store/Actions/FactureActions'
import { Share, Button } from 'react-native';
import axios from 'axios'


import MenuList from './MenuList'
import PlatList from './Facture/PlatList'
import EntreList from './Facture/EntréesList'
import DessertList from './Facture/DessertList'
import BoissonList from './Facture/BoissonList'
import { View, StyleSheet } from 'react-native';
import GradientButton from 'react-native-gradient-buttons';
import Headers from '../Drawer/Headers';
import * as Print from 'expo-print';





class Facture extends React.Component {

  static navigationOptions = {
    headerStyle: {
       height: 80,
     },

     title: "Facture",
     
     headerRight: (
       <Headers/>
 
     )
   }

   state = {
     menu: ``,
   }

   // get details of plat for the bail
   plat = () => {
    token = this.props.navigation.getParam('token')
    if (this.props.command.plats && this.props.command.plats.map(o => {
      const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': token
        }
      }
      axios.get('https://welon.fr/api/plats/infos/' + o[0], c)
      .then(result => {
        t = result.data
          str = 
          `
          <tr>
          <td class="service">${t[0].name}</td>
          <td class="desc">${t[0].description}</td>
          <td class="unit">${t[0].price}€</td>
          <td class="qty">${o[1]}</td>
          </tr>
          `
        this.setState({
          menu: this.state.menu + str
        })
        })
    })) {

    }
   }

      // get details of entrees for the bail

   entrees = () => {
    token = this.props.navigation.getParam('token')
    if (this.props.command.entrees && this.props.command.entrees.map(o => {
      const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': token
        }
      }
      axios.get('https://welon.fr/api/entrees/infos/' + o[0], c)
      .then(result => {
        t = result.data
          str = 
          `
          <tr>
          <td class="service">${t[0].name}</td>
          <td class="desc">${t[0].description}</td>
          <td class="unit">${t[0].price}€</td>
          <td class="qty">${o[1]}</td>
          </tr>
          `
        this.setState({
          menu: this.state.menu + str
        })
        })
    })) {

    }
   }

      // get details of dessertts for the bail

   desserts = () => {
    token = this.props.navigation.getParam('token')
    if (this.props.command.desserts && this.props.command.desserts.map(o => {
      const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': token
        }
      }
      axios.get('https://welon.fr/api/desserts/infos/' + o[0], c)
      .then(result => {
        t = result.data
          str = 
          `
          <tr>
          <td class="service">${t[0].name}</td>
          <td class="desc">${t[0].description}</td>
          <td class="unit">${t[0].price}€</td>
          <td class="qty">${o[1]}</td>
          </tr>
          `
        this.setState({
          menu: this.state.menu + str
        })
        })
    })) {

    }
   }

      // get details of boissons for the bail

   boissons = () => {
    token = this.props.navigation.getParam('token')
    if (this.props.command.boissons && this.props.command.boissons.map(o => {
      const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': token
        }
      }
      axios.get('https://welon.fr/api/boissons/infos/' + o[0], c)
      .then(result => {
        t = result.data
          str = 
          `
          <tr>
          <td class="service">${t[0].name}</td>
          <td class="desc">${t[0].description}</td>
          <td class="unit">${t[0].price}€</td>
          <td class="qty">${o[1]}</td>
          </tr>
          `
        this.setState({
          menu: this.state.menu + str
        })
        })
    })) {

    }
   }

      // get details of menu for the bail


   menu () {
    token = this.props.navigation.getParam('token')
    if (this.props.command.menus && this.props.command.menus.map(o => 
      {
        const c = {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-access-token': token
          }
        }
      axios.get('https://welon.fr/api/menu/' + o[0], c)
      .then(result => {
        t = result.data
          str = 
          `
          <tr>
          <td class="service">${t[0].name}</td>
          <td class="desc">${t[0].description}</td>
          <td class="unit">${t[0].price}€</td>
          <td class="qty">${o[1]}</td>
          </tr>
          `
        this.setState({ menu: this.state.menu + str})
      })
    })) {}
   }

   onShare = async () => {
    // Load data for the pdf export
    this.menu()
    this.plat()
    this.entrees()
    this.desserts()
    this.boissons()
    
    // create html for the bail
    header = 
     `
     <!DOCTYPE html>
     <html lang="en">
       <head>
         <meta charset="utf-8">
         <title>Facture </title>
         <link rel="stylesheet" href="style.css" media="all" />
         <style>
           .clearfix:after {
             content: "";
             display: table;
             clear: both;
           }
     
           a {
             color: #5D6975;
             text-decoration: underline;
           }
     
           body {
             position: relative;
             width: 21cm;  
             height: 29.7cm; 
             margin: 0 auto; 
             color: #001028;
             background: #FFFFFF; 
             font-family: Arial, sans-serif; 
             font-size: 12px; 
             font-family: Arial;
           }
     
           header {
             padding: 10px 0;
             margin-bottom: 30px;
           }
     
           #logo {
             text-align: center;
             margin-bottom: 10px;
           }
     
           #logo img {
             width: 90px;
           }
     
           h1 {
             border-top: 1px solid  #5D6975;
             border-bottom: 1px solid  #5D6975;
             color: #5D6975;
             font-size: 2.4em;
             line-height: 1.4em;
             font-weight: normal;
             text-align: center;
             margin: 0 0 20px 0;
             background: url(dimension.png);
           }
     
           #project {
             float: left;
           }
     
           #project span {
             color: #5D6975;
             text-align: right;
             width: 52px;
             margin-right: 10px;
             display: inline-block;
             font-size: 0.8em;
           }
     
           #company {
             float: right;
             text-align: right;
           }
     
           #project div,
           #company div {
             white-space: nowrap;        
           }
     
           table {
             width: 100%;
             border-collapse: collapse;
             border-spacing: 0;
             margin-bottom: 20px;
           }
     
           table tr:nth-child(2n-1) td {
             background: #F5F5F5;
           }
     
           table th,
           table td {
             text-align: center;
           }
     
           table th {
             padding: 5px 20px;
             color: #5D6975;
             border-bottom: 1px solid #C1CED9;
             white-space: nowrap;        
             font-weight: normal;
           }
     
           table .service,
           table .desc {
             text-align: left;
           }
     
           table td {
             padding: 20px;
             text-align: right;
           }
     
           table td.service,
           table td.desc {
             vertical-align: top;
           }
     
           table td.unit,
           table td.qty,
           table td.total {
             font-size: 1.2em;
           }
     
           table td.grand {
             border-top: 1px solid #5D6975;;
           }
     
           #notices .notice {
             color: #5D6975;
             font-size: 1.2em;
           }
     
           footer {
             color: #5D6975;
             width: 100%;
             height: 30px;
             position: absolute;
             bottom: 0;
             border-top: 1px solid #C1CED9;
             padding: 8px 0;
             text-align: center;
           }
         </style>
       </head>
       <body>
         <header class="clearfix">
           <h1>Facture #${this.props.command._id}</h1>
           <div id="company" class="clearfix">
             <div>Welon</div>
             <div>40 Boulevard de la marquette,<br /> 31000, Toulouse</div>
             <div> http://137.116.226.116/ </div>
           </div>
           <div id="project">
             <div><span>DATE</span> ${this.props.command.created_at}</div>
           </div>
         </header>
         <main>
           <table>
             <thead>
               <tr>
                 <th class="service"> Nom </th>
                 <th class="desc">DESCRIPTION</th>
                 <th>PRICE</th>
                 <th>QTE</th>
               </tr>
             </thead>
             <tbody>
     `
        
        bottom = `
        <tr>
          <td colspan="3" class="grand total">TOTAL</td>
          <td class="grand total">${this.props.command.price}€</td>
        </tr>
        </tbody>
        </table>
        </main>
        <footer>
        Invoice was created on a computer and is valid without the signature and seal.
        </footer>
        </body>
        </html>`

        m = this.state.menu
        html = header + m + bottom
        const pdf = await Print.printToFileAsync({
        html: html,
        base64: false,
     })


    try {
      const result = await Share.share({
        message: 'Welon: Voici votre facture',
        url: pdf.uri
      });
    } catch (error) {
      alert(error.message);
    }
  };

    componentDidMount() {
        obj = {
            restaurant: this.props.navigation.state.params.restaurant,
            token: this.props.navigation.state.params.token
        }
        this.props.getCommand(obj)
        this.props.linkCmd(obj)
    }


    // redirection for the notation
    redirect = () => {
      this.props.navigation.navigate("Restaurant", 
      { restaurant: this.props.command.id_restaurant,
        command: this.props.command, 
        token: this.props.navigation.getParam('token')})
    }

    render() {
      
        token = this.props.navigation.getParam('token')
        menu = this.props.command.menus
        plat = this.props.command.plats
        entrees = this.props.command.entrees
        desserts = this.props.command.desserts
        boissons = this.props.command.boissons

    return (
      <Container style={{margin: 10}}>
        <Content>
          <List>
            <MenuList token={token} menu={menu} />
            <PlatList token={token} plat={plat}/>
            <DessertList token={token} desserts={desserts}/>
            <EntreList token={token} entrées={entrees}/> 
            <BoissonList token={token} boissons={boissons}/>        
          </List>
          
          <View style={{flex: 1, justifyContent: 'space-evenly', alignItems: 'center', marginVertical: 24}}>
            <Text> Total: {this.props.command.price} € </Text> 
            <Button title="Exportez la facture" onPress={this.onShare} color="#c40474"/>

          <GradientButton
            style={{ marginVertical: 8 }}
            text="Suivant"
            textStyle={{ fontSize: 20 }}
            gradientBegin="#00e600"
            gradientEnd="#33ff33"
            gradientDirection="diagonal"
            height={60}
            width={300}
            radius={15}
            impact
            impactStyle='Light'
            onPressAction={this.redirect}
          />
   
          </View>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    margin: 30,
    marginTop: 560
  }
});

const mapStateToProps = (state) => {
    return {
      error: state.command.error,
      command: state.command.command,
      loading: state.command.loading,
      linkError: state.command.linkError,
      linkLoading: state.command.linkLoading,
      linked: state.command.linked
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      getCommand: (text) => {dispatch(getCommand(text))},
      linkCmd: (obj) => {dispatch(linkCmd(obj))}
     }
  }

export default connect(mapStateToProps, mapDispatchToProps)(Facture)