import React from 'react';
import axios from 'axios'
import { connect } from 'react-redux'
import { StyleSheet } from 'react-native'
import ProgressiveImage from '../../ProgressiveImage'
import { ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';



class DessertRequest extends React.Component {

  state = {
    t: {}
  }

  componentDidMount() {
    const c = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token': this.props.token
      }
    }
      axios.get('https://welon.fr/api/desserts/infos/' + this.props.dessert[0], c)
      .then(result => {
        t = result.data
        this.setState({t: t[0]})
      })
  }

  render() {

    return (
      <ListItem thumbnail>
       <Left>
       <ProgressiveImage
            source={require('../../../assets/dessert-min.jpeg')}
            resizeMode="cover"
            style={styles.img}/>
        </Left>
        <Body>
          <Text> {this.state.t.name} </Text>
          <Text note numberOfLines={1}> {this.state.t.description}</Text>
        </Body>
        <Right>
          <Button transparent>
            <Text> Prix : {this.state.t.price} €</Text>
            <Text> Qté : {this.props.dessert[1]}</Text>
          </Button>
        </Right>
      </ListItem>
    );
  }
}

const styles = StyleSheet.create({
  img: {
    width: 57,
    height: 53
  }
})

const mapStateToProps = (state) => {
  return {
    error: state.plat.oneError,
    onePlat: state.plat.onePlat,
    loading: state.plat.oneLoading
  }
}

export default connect(mapStateToProps)(DessertRequest)
