import React from 'react';

import DessertDetail from './DessertDetail'

import { ListItem, Text, List } from 'native-base';


class DessertList extends React.Component {


  render() {

      return (
      <List>
          {
            this.props.desserts && this.props.desserts.map(obj => {
                return(
                  <DessertDetail token={this.props.token} dessert={obj} key={obj[0]}/>
              )
            })
          }
    </List>
    );
  }
}


export default (DessertList)
