import React from 'react';

import EntréesDetail from './EntréesDetail'

import { ListItem, Text, List } from 'native-base';


class EntréesList extends React.Component {


  render() {

      return (
      <List>
          {
            this.props.entrées && this.props.entrées.map(obj => {
                return(
                  <EntréesDetail token={this.props.token} entrées={obj} key={obj[0]}/>
              )
            })
          }
    </List>
    );
  }
}


export default (EntréesList)
