import React from 'react';
import axios from 'axios'
import { connect } from 'react-redux'
import { StyleSheet } from 'react-native'
import ProgressiveImage from '../../ProgressiveImage'
import { ListItem, Text, Left, Body, Right, Button } from 'native-base';

class PlatRequest extends React.Component {

  state = {
    t: {},
    visible: false,
    starCount: 3.5,
    quality: 0,
    quantity: 0,
    price: 0,
  }



  rate = () => {
    obj = {
      quantity: this.state.quantity,
      quality: this.state.quality,
      price: this.state.price,
      id: this.props.plat._id,
      token: this.props.token
    }
    this.props.ratePlat(obj)
  }
  
  componentDidMount() {
    const c = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'x-access-token': this.props.token
      }
    }

      axios.get('https://welon.fr/api/plats/infos/' + this.props.plat[0], c)
      .then(result => {
        t = result.data
        this.setState({t: t[0]})
      })
  }

  render() {

    return (
      <ListItem thumbnail>
       <Left>
       <ProgressiveImage
                source={require('../../../assets/plat-min.jpg')}
                resizeMode="cover"
                style={styles.img}/>
        </Left>
        <Body>
          <Text> {this.state.t.name} </Text>
          <Text note numberOfLines={1}> {this.state.t.description}</Text>
        </Body>
        <Right>
          <Button transparent>
            <Text> Prix : {this.state.t.price} €</Text>
            <Text> Qté : {this.props.plat[1]}</Text>
          </Button>
        </Right>
      </ListItem>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    error: state.plat.oneError,
    onePlat: state.plat.onePlat,
    loading: state.plat.oneLoading
  }
}

const styles = StyleSheet.create({
  img: {
    width: 57,
    height: 53
  }
})

const mapDispatchToProps = (dispatch) => {
  return {
    ratePlat: (text) => {dispatch(ratePlat(text))}
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlatRequest)
