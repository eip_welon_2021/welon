import React from 'react';

import PlatDetail from './PlatDetail'

import { ListItem, Text, List } from 'native-base';


class PlatList extends React.Component {


  render() {

      return (
      <List>
          {
            this.props.plat && this.props.plat.map(obj => {
                return(
                  <PlatDetail token={this.props.token} plat={obj} key={obj[0]}/>
              )
            })
          }
    </List>
    );
  }
}


export default (PlatList)
