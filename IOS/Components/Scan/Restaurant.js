import React from 'react';
import { connect } from 'react-redux'
import PlatList from './PlatList'
import DessertList from './DessertList'
import EntréesList from './EntréesList'
import BoissonList from './BoissonList'
import {  Button } from 'react-native';
import { Container, Content, List} from 'native-base';
import { getAllPlat } from '../../Store/Actions/platActions' 
import { getAllDessert } from '../../Store/Actions/dessertActions'
import { getAllEntrées } from '../../Store/Actions/entréesActions'
import { getAllBoisson } from '../../Store/Actions/boissonActions'
import Headers from '../Drawer/Headers';

class Restaurant extends React.Component {
  
  static navigationOptions = {
    headerStyle: {
       height: 80,
     },
     title: "Notation",     
     headerRight: (
       <Headers/>
     )
   }

   done = () => {
    this.props.navigation.navigate("Home")
  }


    componentDidMount() {
      // Get content of the restaurant for the notaion
        this.props.dispatch(getAllEntrées({token: this.props.navigation.getParam('token'), restaurant: this.props.navigation.getParam('restaurant')}))
        this.props.dispatch(getAllPlat({token: this.props.navigation.getParam('token'),restaurant: this.props.navigation.getParam('restaurant')}))
        this.props.dispatch(getAllDessert({token: this.props.navigation.getParam('token'), restaurant: this.props.navigation.getParam('restaurant')}))
        this.props.dispatch(getAllBoisson({token: this.props.navigation.getParam('token'), restaurant: this.props.navigation.getParam('restaurant')}))
      }

  render() {

    return (
        <Container>
        <Content>
          <List>
            {
              this.props.entrées 
              ?
              <EntréesList token={this.props.navigation.getParam('token')} entrées={this.props.entrées}/>
              :
              null
            }
            {
              this.props.plat 
              ?
              <PlatList token={this.props.navigation.getParam('token')} plat={this.props.plat}/>
              :
              null
            }
            {
              this.props.dessert 
              ?
              <DessertList token={this.props.navigation.getParam('token')} dessert={this.props.dessert}/>
              :
              null
            }
            {
              this.props.boisson 
              ?
              <BoissonList token={this.props.navigation.getParam('token')} boisson={this.props.boisson}/>          
              :
              null
            }
          </List>
          <Button style={{ marginTop: 10}} title="Terminé" onPress={this.done}/>

        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      entrées: state.entrées.entrées,
      plat: state.plat.plat,
      dessert: state.dessert.dessert,
      boisson: state.boisson.boisson
    }
  }


export default connect(mapStateToProps)(Restaurant)
