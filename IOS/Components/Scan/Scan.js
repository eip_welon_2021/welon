import React from 'react'
import Camera from './camera'
import { connect } from 'react-redux'
import Headers from '../Drawer/Headers'
import { AsyncStorage, Text} from 'react-native'



class Scan extends React.Component {

    state = ({
        token: ''
      })

      // set header 

      static navigationOptions = {
        headerTransparent: true,
        headerStyle: {
          height: 80,
        },
        headerLeft: (
          <Text style={{fontSize:28, 
            marginLeft: 20, 
            paddingTop: 20, 
            color: '#929492'}}>Scanner QR Code</Text>
        )
      }

    componentDidMount () {
      // get token from storage if already login
        AsyncStorage.getItem('token').then((result) => {
          if (result) {
            this.setState({
              token: result,
            })
          }
        })
        this.setState({
          token: this.props.token,
        })
      }

    render() {
        return (
            <Camera token={this.state.token} nav={this.props}/>
        )
    }
}

const mapStateToProps = (state) => {
    return {
      token: state.auth.user.token
    }
  }


export default connect(mapStateToProps)(Scan)
