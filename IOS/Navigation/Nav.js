import { createAppContainer, createSwitchNavigator,  } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createMaterialTopTabNavigator, createBottomTabNavigator } from 'react-navigation-tabs'
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';
import Icon from "react-native-vector-icons/FontAwesome";


import React from 'react'


import Home from '../Components/Home'
import SignIn from '../Components/Auth/SignIn'
import SignUp from '../Components/Auth/SignUp'
import Logout from '../Components/Auth/Logout'
import SignUpBiss from '../Components/Auth/SignUpBis'

import Plat from '../Components/Plat/Plat'
import Menu from '../Components/Menu/Menu'
import Dessert from '../Components/Dessert/Dessert'
import Entrées from '../Components/Entrées/Entrées'
import Boisson from '../Components/Boisson/Boisson'

import Scan from '../Components/Scan/Scan'
import Facture from '../Components/Scan/Facture'
import Profile from '../Components/Profil/Profil'
import Restaurant from '../Components/Scan/Restaurant';
import MenuSummary from '../Components/Menu/MenuSummary';


import Headers from '../Components/Drawer/Headers'
import Password from '../Components/Profil/Password';
import History from '../Components/Profil/History';
import Information from '../Components/Information';
import Reward from '../Components/Profil/Reward'
import Delete from '../Components/Auth/Delete'
import { Image, View } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import Map from '../Components/Map/Map';


/// Tab Navigator stack
const TabScreen = createMaterialTopTabNavigator(
  {
    Menu : { screen: Menu },
    Entrées: { screen: Entrées },
    Plat: { screen: Plat },
    Dessert: { screen: Dessert },
    Boisson: { screen: Boisson}
  },
  {
    navigationOptions: {
      headerTransparent: true,
      headerTintColor: '#00C851',
      headerStyle: {
        height: 60,
      },
      headerRight: (
        <Headers/>
      )
    },

    tabBarPosition: 'top',
    swipeEnabled: true,
    animationEnabled: true,
    tabBarOptions: {
      scrollEnabled: true,
      activeTintColor: '#000000',
      inactiveTintColor: '#9d9c9b',
      style: {
        backgroundColor: 'transparent',
        marginTop:100
      },
      labelStyle: {
        textAlign: 'center',
      },
      indicatorStyle: {
        borderBottomColor: '#00C851',
        borderBottomWidth: 2,
      },
    },
  }
);

// AUth stack
const AuthStack = createStackNavigator({
    SignIn: {
        screen: SignIn
    },
    SignUp: {
        screen: SignUp
    },
    SignUpBiss: {
        screen: SignUpBiss
    },
    Information: {
      screen: Information
    }
},
    {
    navigationOptions: {
        headerTransparent: true,
    },
    mode: 'modal',
    headerBackTitleVisible: 'true'
})

// HOME STACCK
const HomeStack = createStackNavigator({
    Home: {
        screen: Home
    },
    tab: TabScreen,
    MenuSummary: { screen: MenuSummary}
})

//Scan Stack
const ScanStack = createStackNavigator({
  Scan: {
    screen: Scan
  },
  Restaurant: {
    screen: Restaurant
  },
  Facture: {
    screen: Facture
  }
}, {
  initialRouteName: "Scan"
})


// Profilstack
const ProfileStack = createStackNavigator({
  Profile: {
    screen: Profile
  },
  Modifier: {
    screen: Password
  },
  Historique: {
    screen: History
  },
  Logout: {
    screen: Logout
  },
  Delete: {
    screen: Delete
  }
})


// Bottom Navigator Stack
const bottomTabNavigator = createBottomTabNavigator(
  {
    Restaurant: {
      screen: HomeStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="home" size={25} color={tintColor} />
        )
      }
    },
    Map: {
      screen: Map,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="map" size={25} color={tintColor} />
        )
      }
    },
    Scan: {
      screen: ScanStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="qrcode" size={25} color={tintColor} />
        )
      }
    },
    Profil: {
      screen: ProfileStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="user" size={25} color={tintColor} />
        )
      }
    },
  },
  {
    initialRouteName: 'Restaurant',
    tabBarOptions: {
      activeTintColor: '#00C851'
    }
  }
);

const MyDrawerNavigator = createDrawerNavigator({
  Home: {
    screen: HomeStack,
  },
  Scan: {
    screen: ScanStack,
    initialRouteName: "Scan",
  },
  Profil: {
    screen: ProfileStack
  },
}, {
  initialRouteName: "Home",
  drawerPosition: "right",
  contentOptions: {
    activeTintColor: '#87B56A',
  },
});

// Main Stack
const Nav = createSwitchNavigator({
    App: bottomTabNavigator,
    Auth: AuthStack,
}, {
    initialRouteName: 'Auth',
})

export default createAppContainer(Nav)
