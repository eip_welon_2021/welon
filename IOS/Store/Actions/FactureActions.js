import axios from 'axios'

const url = "https://welon.fr/api/"


export const getCommand = (obj) => {


    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
      }

    return (dispatch) => {

        console.log(obj)

        dispatch({ type:'GET_COMMAND_BEGIN'})
        axios.get(url + 'command/' + obj.restaurant, c)
        .then(result => {
            dispatch({ type:'GET_COMMAND_SUCCESS', payload: result.data[0]})
        })
        .catch((err) => {
            console.log(err)
            dispatch({ type:'GET_COMMAND_ERROR', err: err.response.data })
        })
    }
}



export const historique = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
    }

    return (dispatch) => {
        dispatch({ type:'GET_HISTORY_BEGIN'})
        axios.get(url + 'command/user', c)
        .then(result => {
            dispatch({ type:'GET_HISTORY_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'GET_HISTORY_ERROR', err: err})
        })
    }

}

// Request to send link the command
export const linkCmd = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
    }

    return (dispatch) => {
        dispatch({ type:'LINK_BEGIN'})
        axios.put(url + 'command/' + obj.restaurant, c)
        .then(result => {
            console.log(result)
            dispatch({ type:'LINK_SUCCESS'})
        })
        .catch((err) => {
            dispatch({ type:'LINK_ERROR', err: err})
        })
    }

}