import axios from 'axios'
import qs from 'querystring'
const fetch = require('node-fetch');


const config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  }

const url = "https://welon.fr/api/auth/"

export const signUp = (user) => {


    const requestBody = qs.stringify({
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email,
        phone: user.phone,
        password: user.password
    })


    return (dispatch) => {
        dispatch({ type:'SIGNUP_BEGIN'})
        axios.post(url + 'register/user', requestBody, config)
        .then(result => {
            console.log(result)
            dispatch({ type:'SIGNUP_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'SIGNUP_ERROR', err: err.response.data })
        })
    }
    
}

export const signIn = (user) => {


    const requestBody = qs.stringify({
        email: user.email,
        password: user.password
    })
    
    return (dispatch, getState) => {
        dispatch({ type:'SIGNIN_BEGIN'})
        axios.post(url + 'login/user', requestBody, config)
        .then(result => {
            dispatch({ type:'SIGNIN_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            console.log(err)
            dispatch({ type:'SIGNIN_ERROR', err: err.response.data })
        })
    }
    
}

export const updatePwd = (user) => {


    const requestBody = qs.stringify({
        email: user.email,
        new_password: user.new_password,
        old_password: user.old_password
    })
    
    return (dispatch) => {
        dispatch({ type:'UPDATE_PASSWORD_BEGIN'})
        axios.post(url + 'password/reset/user', requestBody)
        .then(result => {
            dispatch({ type:'UPDATE_PASSWORD_SUCCESS'})
        })
        .catch((err) => {
            dispatch({ type:'UPDATE_PASSWORD_ERROR', err: err.response.data })
        })
    }
    
}

export const logout = (user) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': user.token
        }
      }

    return (dispatch) => {
        dispatch({ type:'LOGOUT_BEGIN'})
        axios.get(url + 'logout', c)
        .then(result => {
            dispatch({ type:'LOGOUT_SUCCESS'})
        })
        .catch((err) => {
            dispatch({ type:'LOGOUT_ERROR', err: err.response.data })
        })
    }
}

export const deleteUser = (obj) => {

    const requestBody = qs.stringify({
        email: obj.email,
    })

    console.log(obj)
    return (dispatch) => {
        dispatch({ type:'DELETE_USER_BEGIN'})


        const url ='https://welon.fr/api/auth/user';
        const headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-access-token': obj.token

        }
        const data = {
          "email": obj.email
        }
        
        fetch(url, { method: 'DELETE', headers: headers, body: requestBody})
        .then((res) => {
             return res.json()
        })
        .then((json) => {
          console.log(json);
          dispatch({ type:'DELETE_USER_SUCCESS'})
        })
        .catch((err) => {
            console.log(err)
            dispatch({ type:'DELETE_USER_ERROR', err: err.response.data })
        })
    }
}

export const me = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
      }

          return (dispatch) => {
        dispatch({ type:'ME_BEGIN'})
        axios.get(url + 'me', c)
        .then(result => {
            dispatch({ type:'ME_SUCCESS', payload: result.data})
        })
        .catch((err) => {

            dispatch({ type:'ME_ERROR', err: err.response.data })
        })
    }
}