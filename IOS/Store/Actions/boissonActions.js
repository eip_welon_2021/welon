import axios from 'axios'
import qs from 'querystring'

const url = "https://welon.fr/api/"

export const getAllBoisson = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
      }

    return (dispatch) => {
        dispatch({ type:'GET_ALL_BOISSON_BEGIN'})
        axios.get(url + 'boissons/' + obj.restaurant, c)
        .then(result => {
            dispatch({ type:'GET_ALL_BOISSON_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'GET_ALL_BOISSON_ERROR', err: err.response })
        })
    }
}

export const rateBoisson = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
      }

      const requestBody = qs.stringify({
        quality: obj.quality,
        quantity: obj.quantity,
        price: obj.price,
    })

    return (dispatch) => {
        dispatch({ type:'RATE_BOISSON_BEGIN'})
        axios.post(url + 'rating/boissons/' + obj.id, requestBody, c)
        .then(result => {
            dispatch({ type:'RATE_BOISSON_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'RATE_BOISSON_ERROR', err: err.response.data })
        })
    }
}