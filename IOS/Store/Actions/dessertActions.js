import axios from 'axios'
import qs from 'querystring'

const url = "https://welon.fr/api/"


export const getAllDessert = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
      }

    return (dispatch) => {
        dispatch({ type:'GET_ALL_DESSERT_BEGIN'})
        axios.get(url + 'desserts/' + obj.restaurant, c)
        .then(result => {
            dispatch({ type:'GET_ALL_DESSERT_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'GET_ALL_DESSERT_ERROR', err: err.response.data })
        })
    }
}

export const rateDessert = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
      }

      const requestBody = qs.stringify({
        quality: obj.quality,
        quantity: obj.quantity,
        price: obj.price,
    })

    return (dispatch) => {
        dispatch({ type:'RATE_DESSERT_BEGIN'})
        axios.post(url + 'rating/desserts/' + obj.id, requestBody, c)
        .then(result => {
            dispatch({ type:'RATE_DESSERT_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'RATE_DESSERT_ERROR', err: err.response.data })
        })
    }
}