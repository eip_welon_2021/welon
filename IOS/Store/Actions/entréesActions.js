import axios from 'axios'
import qs from 'querystring'


const url = "https://welon.fr/api/"

export const getAllEntrées = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
      }

    return (dispatch) => {
        dispatch({ type:'GET_ALL_ENTRES_BEGIN'})
        axios.get(url + 'entrees/' + obj.restaurant, c)
        .then(result => {
            dispatch({ type:'GET_ALL_ENTRES_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'GET_ALL_ENTRES_ERROR', err: err.response.data })
        })
    }
}

export const getEntrées = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
      }

      return (dispatch) => {
        dispatch({ type:'GET_ENTRES_BEGIN'})
        axios.get(url + 'entrees/infos/' + obj.entrées._id, c)
        .then(result => {
            dispatch({ type:'GET_ENTRES_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'GET_ENTRES_ERROR', err: err.response.data })
        })
    }
}

// Request yo send notations
export const rateEntrées = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
      }

      const requestBody = qs.stringify({
        quality: obj.quality,
        quantity: obj.quantity,
        price: obj.price,
    })

    return (dispatch) => {
        dispatch({ type:'RATE_ENTRES_BEGIN'})
        axios.post(url + 'rating/entrees/' + obj.id, requestBody, c)
        .then(result => {
            dispatch({ type:'RATE_ENTRES_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'RATE_ENTRES_ERROR', err: err.response.data })
        })
    }
}