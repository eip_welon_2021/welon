import axios from 'axios'

const url = "https://welon.fr/api/"


export const getAllMenu = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
      }

    return (dispatch) => {
        dispatch({ type:'GET_ALL_MENU_BEGIN'})
        axios.get(url + 'menus/' + obj.restaurant._id, c)
        .then(result => {
            dispatch({ type:'GET_ALL_MENU_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'GET_ALL_MENU_ERROR', err: err.response.data })
        })
    }
}

export const getMenu = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
      }

      return (dispatch) => {
        dispatch({ type:'GET_MENU_BEGIN'})
        axios.get(url + 'menu/' + obj.menu._id , c)
        .then(result => {
            dispatch({ type:'GET_MENU_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'GET_MENU_ERROR', err: err.response.data })
        })
    }
}