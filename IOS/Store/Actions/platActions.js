import axios from 'axios'
import qs from 'querystring'

const url = "https://welon.fr/api/"


export const getAllPlat = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
      }

    return (dispatch) => {
        dispatch({ type:'GET_ALL_PLAT_BEGIN'})
        axios.get(url + 'plats/' + obj.restaurant, c)
        .then(result => {
            dispatch({ type:'GET_ALL_PLAT_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'GET_ALL_PLAT_ERROR', err: err.response.data })
        })
    }
}

export const ratePlat = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
      }

      const requestBody = qs.stringify({
        quality: obj.quality,
        quantity: obj.quantity,
        price: obj.price,
    })

    return (dispatch) => {
        dispatch({ type:'RATE_PLAT_BEGIN'})
        axios.post(url + 'rating/plats/' + obj.id, requestBody, c)
        .then(result => {
            dispatch({ type:'RATE_PLAT_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'RATE_PLAT_ERROR', err: err.response.data })
        })
    }
}

export const getOnelPlat = (obj) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': obj.token
        }
      }

    return (dispatch) => {
        dispatch({ type:'GET_PLAT_BEGIN'})
        axios.get(url + 'plats/infos/' + obj.plat, c)
        .then(result => {
            dispatch({ type:'GET_PLAT_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'GET_PLAT_ERROR', err: err.response.data })
        })
    }
}