import axios from 'axios'

const url = "https://welon.fr/api/auth/"


export const getAllRestaurant = (user) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': user
        }
      }

    return (dispatch) => {
        dispatch({ type:'GET_ALL_RESTAURANT_BEGIN'})
        axios.get(url + 'restaurant', c)
        .then(result => {
            dispatch({ type:'GET_ALL_RESTAURANT_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'GET_ALL_RESTAURANT_ERROR', err: err.response.data })
        })
    }
}

export const getBestRestaurant = (user) => {

    const c = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'x-access-token': user
        }
      }

    return (dispatch) => {
        dispatch({ type:'GET_ALL_RESTAURANT_BEGIN'})
        axios.get("https://welon.fr/api/rating/restaurant/top", c)
        .then(result => {
            console.log(result)
            dispatch({ type:'GET_ALL_RESTAURANT_SUCCESS', payload: result.data})
        })
        .catch((err) => {
            dispatch({ type:'GET_ALL_RESTAURANT_ERROR', err: err.response.data })
        })
    }
}