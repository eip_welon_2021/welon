const initialState = {
    signInError: null,
    signUpError: null,
    logoutError: null,

    error: null,
    logIn: false,
    logout: false,
    signUpSuccess: false,
    signUpLoading: false,


    user: {},
    me: {},
    meLoading: false,

    update: false,
    updateError: null,
    updateLoading: false,

    deleteError: null,
    deleteLoading: false,
    deleteSuccess: false,
}

const authReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'SIGNUP_BEGIN':

        return {
            ...state,
            signUpError: null,
            logIn: false,
            signUpSuccess: false,
            signUpLoading: true,


        }
        case 'SIGNUP_ERROR':
        return {
            ...state,
            signUpError: action.err,
            logIn: false,
            signUpSuccess: false,

        }
        case 'SIGNUP_SUCCESS':
        return {
            ...state,
            signUpError: null,
            signUpSuccess: true,
            logout: false,
            deleteSuccess: false,
            signUpLoading: false,
            user: action.payload
        }

        case 'SIGNIN_BEGIN':
        return {
            ...state,
            signInError: null,
            logIn: false
        }
        case 'SIGNIN_ERROR':
        return {
            ...state,
            signInError: action.err,
            logIn: false
        }
        case 'SIGNIN_SUCCESS':
        return {
            ...state,
            signInError: null,
            logIn: true,
            logout:false,
            deleteSuccess: false,
            user: action.payload
        }

        case 'DELETE_USER_BEGIN':
        return {
            ...state,
            deleteLoading: true,
        }
        case 'DELETE_USER_ERROR':
        return {
            ...state,
            deleteError: action.err,
        }
        case 'DELETE_USER_SUCCESS':
        return {
            ...state,
            deleteError: null,
            deleteSuccess: true,
            logIn: false,
            logout:true,
        }

        case 'LOGOUT_ERROR':
        return {
            ...state,
            logoutError: action.err,
            logout: false
        }
        case 'LOGOUT_SUCCESS':
        return {
            ...state,
            logoutError: null,
            logout: true,
            logIn: false
        }
        case 'LOGOUT_BEGIN':
        return {
            ...state,
            logoutError: null,
            logout: false,
        }

        case 'ME_BEGIN':
        return {
            ...state,
            error: null,
            meLoading: true
        }
        case 'ME_ERROR':
        return {
            ...state,
            error: action.err,
            meLoading: false
        }
        case 'ME_SUCCESS':
        return {
            ...state,
            error: null,
            me: action.payload,
            meLoading: false
        }

        case 'UPDATE_PASSWORD_BEGIN':
        return {
            ...state,
            updateError: null,
            updateLoading: true
        }
        case 'UPDATE_PASSWORD_ERROR':
        return {
            ...state,
            updateError: action.err,
            updateLoading: false
        }
        case 'UPDATE_PASSWORD_SUCCESS':
        return {
            ...state,
            updateError: null,
            update: true,
            updateLoading: false
        }
    }
    return state
}

export default authReducer