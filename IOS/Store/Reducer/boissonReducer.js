const initialState = {
    error: null,
    boisson: [],
    loading: false,
    rateError: null,
    rateLoading: false
}

const boissonReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'GET_ALL_BOISSON_BEGIN':
        return {
            ...state,
            loading: true,  
        }
        case 'GET_ALL_BOISSON_ERROR':
        return {
            ...state,
            loading: false,
            boisson: [],
            error: action.err
        }
        case 'GET_ALL_BOISSON_SUCCESS':
        return {
            ...state,
            loading: false,
            error: null,
            boisson: action.payload
        }

        case 'RATE_BOISSON_BEGIN':
            return {
                ...state,
                rateLoading: true,
            }
        case 'RATE_BOISSON_ERROR':
            return {
                ...state,
                rateLoading: false,
                rateError: action.err
            }
        case 'RATE_BOISSON_SUCCESS':
            return {
                ...state,
                rateLoading: false,
                rateError: null,
            }
    }
    return state
}

export default boissonReducer