const initialState = {
    error: null,
    dessert: [],
    loading: false
}

const dessertReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'GET_ALL_DESSERT_BEGIN':
        return {
            ...state,
            loading: true,  
        }
        case 'GET_ALL_DESSERT_ERROR':
        return {
            ...state,
            loading: false,
            dessert: [],
            error: action.err,
            rateError: null,
            rateLoading: false
        }
        case 'GET_ALL_DESSERT_SUCCESS':
        return {
            ...state,
            loading: false,
            error: null,
            dessert: action.payload
        }

        case 'RATE_DESSERT_BEGIN':
        return {
            ...state,
            rateLoading: true,
        }
        case 'RATE_DESSERT_ERROR':
        return {
            ...state,
            rateLoading: false,
            rateError: action.err
        }
        case 'RATE_DESSERT_SUCCESS':
        return {
            ...state,
            rateLoading: false,
            rateError: null,
        }
    }
    return state
}

export default dessertReducer