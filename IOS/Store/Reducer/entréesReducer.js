const initialState = {
    error: null,
    entrées: [],
    loading: false,
    oneError: null,
    oneEntrées: {},
    oneLoading: false,
    rateError: null,
    rateLoading: false
}

const entréesReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'GET_ALL_ENTRES_BEGIN':
        return {
            ...state,
            loading: true,  
        }
        case 'GET_ALL_ENTRES_ERROR':
        return {
            ...state,
            loading: false,
            entrées: [],
            error: action.err
        }
        case 'GET_ALL_ENTRES_SUCCESS':
        return {
            ...state,
            loading: false,
            error: null,
            entrées: action.payload
        }

        case 'GET_ENTRES_BEGIN':
        return {
            ...state,
            oneLoading: true,
        }
        case 'GET_ENTRES_ERROR':
        return {
            ...state,
            oneLoading: false,
            oneEntrées: {},
            oneError: action.err
        }
        case 'GET_ENTRES_SUCCESS':
        return {
            ...state,
            oneLoading: false,
            oneError: null,
            oneEntrées: action.payload[0]
        }

        case 'RATE_ENTRES_BEGIN':
        return {
            ...state,
            rateLoading: true,
        }
        case 'RATE_ENTRES_ERROR':
        return {
            ...state,
            rateLoading: false,
            rateError: action.err
        }
        case 'RATE_ENTRES_SUCCESS':
        return {
            ...state,
            rateLoading: false,
            rateError: null,
        }
    }
    return state
}

export default entréesReducer