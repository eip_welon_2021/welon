const initialState = {
    error: null,
    command: {},
    loading: false,
    history: [],
    linkError: null,
    linkLoading: false,
    linked: true
}

const commandReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'GET_COMMAND_BEGIN':
        return {
            ...state,
            loading: true,  
            error: null
        }
        case 'GET_COMMAND_ERROR':
        return {
            ...state,
            loading: false,
            command: [],
            error: action.err
        }
        case 'GET_COMMAND_SUCCESS':
        return {
            ...state,
            loading: false,
            error: null,
            command: action.payload
        }

        case 'GET_HISTORY_BEGIN':
        return {
            ...state,
            loading: true,  
            error: null
        }
        case 'GET_HISTORY_ERROR':
        return {
            ...state,
            loading: false,
            history: [],
            error: action.err
        }
        case 'GET_HISTORY_SUCCESS':
        return {
            ...state,
            loading: false,
            error: null,
            history: action.payload
        }

        case 'LINK_BEGIN':
        return {
            ...state,
            linkError: null,
            linkLoading: true,
            linked: false
        }
        case 'LINK_ERROR':
        return {
            ...state,
            linkError: action.err,
            linkLoading: false,
            linked: false
        }
        case 'LINK_SUCCESS':
        return {
            ...state,
            linkError: null,
            linked: true,
            linkLoading: false
        }
    }
    return state
}

export default commandReducer