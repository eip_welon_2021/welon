const initialState = {
    error: null,
    menu: [],
    loading: false,
    oneMenuError: null,
    oneMenuLoading: null,
    oneMenu: {}
}

const menuReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'GET_ALL_MENU_BEGIN':
        return {
            ...state,
            loading: true,  
        }
        case 'GET_ALL_MENU_ERROR':
        return {
            ...state,
            loading: false,
            menu: [],
            error: action.err
        }
        case 'GET_ALL_MENU_SUCCESS':
        return {
            ...state,
            loading: false,
            error: null,
            menu: action.payload
        }

        case 'GET_MENU_BEGIN':
        return {
            ...state,
            oneMenuLoading: true,  
            oneMenu: {}
        }
        case 'GET_MENU_ERROR':
        return {
            ...state,
            oneMenuLoading: false,
            oneMenu: {},
            oneMenuError: action.err
        }
        case 'GET_MENU_SUCCESS':
        return {
            ...state,
            oneMenuLoading: false,
            oneMenuError: null,
            oneMenu: action.payload[0]
        }
    }
    return state
}

export default menuReducer