const initialState = {
    error: null,
    plat: [],
    loading: false,
    oneError: null,
    onePlat: {},
    oneLoading: false,
    rateError: null,
    rateLoading: false
}

const platReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'GET_ALL_PLAT_BEGIN':
        return {
            ...state,
            loading: true,
            
        }
        case 'GET_ALL_PLAT_ERROR':
        return {
            ...state,
            loading: false,
            plat: [],
            error: action.err
        }
        case 'GET_ALL_PLAT_SUCCESS':
        return {
            ...state,
            loading: false,
            error: null,
            plat: action.payload
        }

        case 'GET_PLAT_BEGIN':
        return {
            ...state,
            oneLoading: true,
        }
        case 'GET_PLAT_ERROR':
        return {
            ...state,
            oneLoading: false,
            onePlat: [],
            oneError: action.err
        }
        case 'GET_PLAT_SUCCESS':
        return {
            ...state,
            oneLoading: false,
            oneError: null,
            onePlat: action.payload[0]
        }

        case 'RATE_PLAT_BEGIN':
        return {
            ...state,
            rateLoading: true,
        }
        case 'RATE_PLAT_ERROR':
        return {
            ...state,
            rateLoading: false,
            rateError: action.err
        }
        case 'RATE_PLAT_SUCCESS':
        return {
            ...state,
            rateLoading: false,
            rateError: null,
        }
    }
    return state
}

export default platReducer