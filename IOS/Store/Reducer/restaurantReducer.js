const initialState = {
    error: null,
    restaurant: [],
    loading: false
}

const restaurantReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'GET_ALL_RESTAURANT_BEGIN':
        return {
            ...state,
            loading: true,
            
        }
        case 'GET_ALL_RESTAURANT_ERROR':
        return {
            ...state,
            loading: false,
            restaurant: [],
            error: action.err
        }
        case 'GET_ALL_RESTAURANT_SUCCESS':
        return {
            ...state,
            loading: false,
            error: null,
            restaurant: action.payload
        }
    }
    return state
}

export default restaurantReducer