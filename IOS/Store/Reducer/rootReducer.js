import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { combineReducers } from 'redux'

import platReducer from './platReducer'
import authReducer from './authReducer'
import menuReducer from './menuReducer'
import factureReducer from './factureReducer'
import dessertReducer from './dessertReducer'
import entréesReducer from './entréesReducer'
import boissonReducer from './boissonReducer'
import restaurantReducer from './restaurantReducer'


// Add reducer to the store
const rootReducer = combineReducers ({
    auth: authReducer,
    plat: platReducer,
    menu: menuReducer,
    command: factureReducer,
    dessert: dessertReducer,
    entrées: entréesReducer,
    boisson: boissonReducer,
    restaurant: restaurantReducer
})

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware))

export { store }