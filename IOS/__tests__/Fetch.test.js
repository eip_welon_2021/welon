import { Fetch } from '../Store/Fetch';


  test('the fetch fails with an error', async () => {

    const config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body : {
            email: 'toto',
            password: 'toto'
        }
      }

    try {
      await Fetch('http://137.116.226.116/api/auth/login/user', config)
    } catch (e) {
      expect(e).toMatch('error');
    }
  });




  test('the data is peanut butter', async () => {

    const config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }

    const data = await Fetch('https://facebook.github.io/react-native/movies.json', config);
    expect.anything()
  });