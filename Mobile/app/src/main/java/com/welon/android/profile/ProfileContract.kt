package com.welon.android.profile

class ProfileContract {
    interface View {
        fun onClickLogOut(view: android.view.View)
        fun onClickGoToChangePassword(view: android.view.View)
        fun onClickGoToHistory(view: android.view.View)
    }
}