# API Welon

## Before launching

### Environment variables

You need to set 4 environment variables to launch properly this project:
- `DB_URL` used to tell where is the database
- `SECRET` used to encrypt the jwt
- `access_key` used as an access key for aws s3 image storage cloud
- `secret_key` used as a key to access aws s3 image storage cloud

### Npm Scripts

- `start` Starts the API with node and with prod options
- `dev` Starts the API with nodemon and with dev options
- `test` Unit test

