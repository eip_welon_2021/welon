const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

//Enable bodyParser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded( {extended:true} ));

//Enable CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-access-token");
  next();
});

//Enable assets storage
app.use(express.static('assets'));

/*
** Routes
*/

// Routes Authentification
var userController = require('./controllers/userController');
var restaurantController = require('./controllers/restaurantController');
var authController  = require('./controllers/authController');
app.use('/api/auth/', userController);
app.use('/api/auth/', restaurantController);
app.use('/api/auth/', authController);

// Routes Menus & Entrees & Plats & Desserts & Boissons
var menuController = require('./controllers/menuController');
var entreeController = require('./controllers/entreeController');
var platController = require('./controllers/platController');
var dessertController = require('./controllers/dessertController');
var boissonController = require('./controllers/boissonController');

app.use('/api', menuController);
app.use('/api', entreeController);
app.use('/api', platController);
app.use('/api', dessertController);
app.use('/api', boissonController);

// Routes QRcodes
var qrcodeRouter = require('./controllers/QRcodeController');
app.use('/api', qrcodeRouter);

// Routes chatbot
var chatbotRouter = require('./chatbot/chatbot');
app.use('/api', chatbotRouter);

// Routes dashboard & rating
var dashboardController = require('./controllers/dashboardController');
var ratingController = require('./controllers/ratingController');

app.use('/api', dashboardController);
app.use('/api', ratingController);

//Routes commandes
var commandeController = require('./controllers/commandeController');
app.use('/api', commandeController);

//Routes upload photos
var uploadPhotoController = require('./../upload');
app.use('/api', uploadPhotoController);

/*
** Errors Management
*/

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  console.log(err.message);

  // render the error page
  res.status(err.status || 500);
  res.json({error : err});
});

module.exports = app;
