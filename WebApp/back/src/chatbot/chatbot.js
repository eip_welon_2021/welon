// routes/chatbot/chatbot.js
var express = require('express');
var router = express.Router();
var sapcai = require('sapcai').default

var build = new sapcai.build('6dc64c066d8b808f8ae39ca4224c5e0c');

router.post('/chatbot', (req, RES, next) => {
    if (!req.body.message)
        return RES.status(400).send("Il manque un champ dans le body.");

    build.dialog({ type: 'text', content: req.body.message }, { conversationId: '1' })
    .then(function (REPONSE) {

        return RES.status(200).json({ 'success': 'message received', 'response': REPONSE.messages[0].content.toString() });
    })
    .catch(function(error) {
        console.error(error);
        return RES.status(500).send("échec de la requête au chatbot.");
    });

});

module.exports = router;