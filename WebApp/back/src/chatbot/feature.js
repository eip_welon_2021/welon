// routes/chatbot/chatbot.js
var express = require('express');
var router = express.Router();
var http = require('http');
const { Botkit } = require('botkit');
const { WebAdapter } = require('botbuilder-adapter-web');


const controller = new Botkit({
    webhook_uri: '/chatbot',
    adapter: WebAdapter,
    storage
});

/* Quand le controller à finit de charger */
controller.ready(() => {

    /* Intention de salutations */
    controller.hears(['hi', 'hello', 'bonjour', 'Bonjour', 'Bonsoir'], ['message'], async (bot, message) => {

        await bot.reply(message, 'Hey bonjour ! ^_^');
    });

    /* Intention de presentation */
    controller.hears(['Welon', 'Présente Welon', 'C\'est quoi Welon ?', 'c\'est quoi Welon', 'présente moi Welon', 'Welon ?', 'C\'est quoi Welon ?'], ['message'], async (bot, message) => {

        await bot.reply(message, 'Le projet Welon est une équipe, de 7 étudiants à l’Epitech, qui se préoccupe de son environnement et plus particulièrement, dans le milieu où les futures générations évolueront. L’enjeu écologique est aujourd’hui un axe majeur de notre société actuelle. C’est pourquoi nous nous devons de commencer à agir face à cette grande épreuve qu’est le gaspillage alimentaire. Chez Welon, nous souhaitons minimiser l’impact du gaspillage alimentaire dans les restaurants de manière simple, intuitive et innovante. Notre solution est une plateforme qui rapproche les restaurants partenaires de leurs clients. Pour se faire, les consommateurs seront récompensés de leur fidélité en donnant des retours sur les plats mangés. Une fois ces derniers analysés, ils formeront un ensemble de données utiles pour nos partenaires. Ces données permettront aux restaurants d’optimiser les portions et la qualité de leurs plats.');
    });

    /* Intention d'aide */
    controller.hears(['help', 'Help', 'HELP'], ['message'], async (bot, message) => {

        await bot.reply(message, 'Pour l\'instant je suis capable de :');
        await bot.reply(message, 'Te présenter Welon,  notre projet. Tu peux écrire [c\'est quoi welon ?].');
        await bot.reply(message, 'Comprendre des salutations et un au revoir... ^_^');
    });

    /* Cancel anay active dialogs */
    controller.interrupts('quit', 'message', async (bot, message) => {

        await bot.reply(message, 'Quiting');
        await bot.cancelAllDialogs();
    });

    /* WelonFirst */
    controller.hears(['WelonFirst'], ['message'], async (bot, message) => {

        await bot.reply(message, '#WelonNumberOne');
    });

    /* Incompréhension */
    controller.hears('.*', 'message', async (bot, message) => {
        console.log()
        await bot.reply(message, 'Oups, je ne comprends pas désolé...Tu peux écrire [help] si tu as besoin d\'aide.');
    });
});