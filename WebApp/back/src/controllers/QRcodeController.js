// routes/QRcode.js
var express = require('express');
var router = express.Router();

var verifyToken = require('../services/modules/verifyToken');
var QRCode = require('qrcode');
var Restaurant = require('../models/restaurantModel');

// GET QRcode, requests to /api/qrcode
router.get('/qrcode', verifyToken, (req, res, next) => {
    QRCode.toFile("./uploads/" + req.userId + ".png", req.userId, function (err) {
        if (err) {
            console.log(err);
            return res.status(400).send("Internal server error.");
        }
        return res.status(200).sendFile(req.userId + ".png", {root: __dirname + "/../../uploads/"}, function(err) {
            if (err)
                console.log(err);
        });
    })
});

// GET QRcode, requests to /api/qrcode/:id
router.get('/qrcode/:id', verifyToken, (req, res, next) => {
    Restaurant.find({_id: req.params.id}, function(err) {
        if (err)
            return res.status(400).send("Aucun restaurant trouvé.");
        else {
            QRCode.toFile("./uploads/" + req.params.id + ".png", req.params.id, function (err) {
                if (err) {
                    console.log(err);
                    return res.status(400).send("Erreur interne du serveur.");
                }
                return res.status(200).sendFile(req.params.id + ".png", {root: __dirname + "/../../uploads/"}, function(err) {
                    if (err)
                        console.log(err);
                });
            })
        }
    })
});

module.exports = router;