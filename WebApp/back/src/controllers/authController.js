// /routes/auth/authController.js

var express = require('express');
var router = express.Router();

var User = require('../models/userModel');
var Restaurant = require('../models/restaurantModel');
var userService = require('../services/userServices');
var restaurantService = require('../services/restaurantServices');
var verifyToken = require('../services/modules/verifyToken');

// GET user infos
router.get('/me', verifyToken, async (req, res) => {
    var user = await userService.findUserById(req.userId);
    if (user.user)
        return res.status(200).send(user.user);
    var restaurant = await restaurantService.findRestaurantById(req.userId);
    if (restaurant.restaurant)
        return res.status(200).send(restaurant.restaurant);
    else
        return res.status(400).send("Auncun utilisateur & restaurant trouvé.");
});

var infos = require('../services/modules/token');
// VERIFY RESTAURANT EMAIL
router.get('/email/verification/restaurant/:token', async (req, res) => {
    var id = await infos.getIdViaToken(req.params.token);
    Restaurant.updateOne({_id: id}, {verified: true})
    .then(doc => {
        return res.status(200).redirect("http://www.welon.fr/verified?token=" + req.params.token);
    })
    .catch(err => {
        console.log(err);
        return res.status(400).send("Aucun restaurant trouvé.");
    })
});

// VERIFY USER EMAIL
router.get('/email/verification/user/:id', function(req, res) {
    User.updateOne({_id: req.params.id}, {verified: true})
    .then(doc => {
        return res.status(200).redirect("http://www.welon.fr/verified");
    })
    .catch(err => {
        console.log(err);
        return res.status(400).send("Aucun utilisateur trouvé.");
    })
});

var pwd = require('../services/modules/checkpassword');
var bcrypt = require('bcryptjs');

// RESET RESTAURANT PASSWORD
router.post('/password/reset/restaurant', function(req, res) {
    if (!req.body.new_password || !req.body.email || !req.body.old_password)
        return res.status(400).send("Il manque un champs dans le body: new_password or email or old_password");
    if (!pwd.checkpassword(req.body.new_password))
        return res.status(400).send("Le mot de passe doit contenir 8 characteres minimum incluant un chiffre, une minuscule et une majuscule.");
    var hashedPassword = bcrypt.hashSync(req.body.new_password, 8);
    Restaurant.updateOne({email: req.body.email}, {password: hashedPassword})
    .then(doc => {
        console.log(doc, 'change the password');
        return res.status(200).send("Mot de passe modifié.");
    })
    .catch(err => {
        console.log(err);
        return res.status(400).send("Aucun restaurant trouvé.");
    })
});

// RESET USER PASSWORD
router.post('/password/reset/user', function(req, res) {
    if (!req.body.new_password || !req.body.email || !req.body.old_password)
        return res.status(400).send("Il manque un champs dans le body: new_password or email or old_password");
    if (!pwd.checkpassword(req.body.new_password))
        return res.status(400).send("Le mot de passe doit contenir 8 characteres minimum incluant un chiffre, une minuscule et une majuscule.");
    var hashedPassword = bcrypt.hashSync(req.body.new_password, 8);
    User.updateOne({email: req.body.email}, {password: hashedPassword})
    .then(doc => {
        console.log(doc, 'change the password');
        return res.status(200).send("Mot de passe modifié.");
    })
    .catch(err => {
        console.log(err);
        return res.status(400).send("Aucun utilisateur trouvé.");
    })
});

// UPDATE a restaurant by ID
router.put('/update/restaurant/:id', verifyToken, function(req, res) {
    Restaurant.updateOne({_id: req.params.id}, {verified: true})
    .then(doc => {
        return res.status(200).send("Profil vérifié!");
    })
    .catch(err => {
        return res.status(400).send("Profil pas trouvé");
    })
});

// UPDATE a user by ID
router.put('/update/user/:id', verifyToken, function(req, res) {
    User.updateOne({_id: req.params.id}, {verified: true})
    .then(doc => {
        return res.status(200).send("Profil vérifié!");
    })
    .catch(err => {
        return res.status(400).send("Profil pas trouvé");
    })
});

// GET logout
router.get('/logout', verifyToken, function(req, res) {
    return res.status(200).send("Utilisateur déconnecté.");
});

module.exports = router;