// routes/menus/boisson.js
var express = require('express');
var router = express.Router();

var verifyToken = require('../services/modules/verifyToken');
var boissonService = require('../services/boissonServices');

// GET all boissons for the connected restaurant, requests to /api/boissons
router.get('/boissons', verifyToken, async (req, res) => {
    boissonService.boissonFindByRestaurant(req.userId)
    .then((boissons) => {
        return res.status(200).send(boissons);
    })
    .catch((err) => {
        return res.status(400).send(err);
    })
});

// GET all boissons for the connected restaurant, requests to /api/boissons
router.get('/boissons/:id', verifyToken, (req, res, next) => {
    boissonService.boissonFindByRestaurant(req.params.id)
    .then((boissons) => {
        return res.status(200).send(boissons);
    })
    .catch((err) => {
        return res.status(400).send(err);
    })
});

// GET a boissons
router.get('/boissons/infos/:id', verifyToken, (req, res, next) => {
    boissonService.boissonFindById(req.params.id, res)
    .then((boissons) => {
        return res.status(200).send(boissons);
    })
    .catch((err) => {
        return res.status(400).send(err);
    })
});

// POST create a boisson, requests to /api/boissons
router.post('/boissons', verifyToken, (req, res, next) => {
    if (!req.body.name || !req.body.desc || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    if (req.body.name.length > 20)
        return res.status(400).send("Le nom entré est trop long. (limite 20 charactères)");
    if (req.body.desc.length > 40)
        return res.status(400).send("La description entrée est trop longue. (limite 40 charactères)");
    var newBoisson = {
        id_restaurant: req.userId,
        name: req.body.name,
        description: req.body.desc,
        price: req.body.price
    };
    boissonService.boissonCreate(newBoisson)
    .then((boissons) => {
        return res.status(200).send(boissons);
    })
    .catch((err) => {
        return res.status(400).send(err);
    })
});

// PUT update a boisson, requests to /api/boissons/id_boissons
router.put('/boissons/:id', verifyToken, (req, res, next) => {
    if (!req.body.name || !req.body.desc || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    if (req.body.name.length > 20)
        return res.status(400).send("Le nom entré est trop long. (limite 20 charactères)");
    if (req.body.desc.length > 40)
        return res.status(400).send("La description entrée est trop longue. (limite 40 charactères)");
    var newValues = {
        id_restaurant: req.userId,
        name: req.body.name,
        description: req.body.desc,
        price: req.body.price
    };
   boissonService.boissonUpdate(req.params.id, newValues)
   .then((boissons) => {
        return res.status(200).send(boissons);
    })
    .catch((err) => {
        return res.status(400).send(err);
    })
});

// DELETE a boisson, requests to /api/boissons/id_boissons
router.delete('/boissons/:id', verifyToken, (req, res, next) => {
    boissonService.boissonDelete(req.params.id, res)
    .then((boissons) => {
        return res.status(200).send(boissons);
    })
    .catch((err) => {
        return res.status(400).send(err);
    })
});

module.exports = router;