// /routes/controllers/assetlinks.js

var express = require('express');
var router = express.Router();
var ObjectID = require('mongodb').ObjectID;

var verifyToken = require('../services/modules/verifyToken');
var commandeService = require('../services/commandeServices');
var Price = require('../services/modules/priceModule');

// GET HISTORIC OF COMMAND OF A USER
router.get('/command/user', verifyToken, function(req, res) {
    commandeService.commandeFindByUser(req.userId)
    .then(commandes => {
        return res.status(200).send(commandes);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// GET COMMANDS OF A RESTAURANT
router.get('/command/restaurant', verifyToken, function(req, res) {
    commandeService.commandeFindByRestaurant(req.userId)
    .then(commandes => {
        return res.status(200).send(commandes);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// LINK USER TO COMMAND
router.get('/command/:id', verifyToken, async function(req, res) {
    commandeService.commandeAddUser(req.params.id, req.userId)
    .then(() => {
        commandeService.commandeFindById(req.params.id)
        .then(commandes => {
            return res.status(200).send(commandes);
        })
        .catch(err => {
            return res.status(400).send(err);
        })
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// GET QRCODE BY COMMAND ID
router.get('/command/qrcode/:id', verifyToken, function(req, res) {
    commandeService.commandeQrcode(req.params.id)
    .then(filename => {
        return res.status(200).sendFile(filename, {root: __dirname + "./../../uploads/"}, function(err) {
            if (err)
                res.status(400).send("Echoue à l'envoie du fichier.");
        });
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// DELETE COMMAND
router.delete('/command/:id', verifyToken, function(req, res) {
    commandeService.commandeDelete(req.params.id)
    .then(commandes => {
        return res.status(200).send(commandes);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// UPDATE COMMAND
router.put('/command/:id', verifyToken, async function(req, res) {
    if (!req.body.menus && !req.body.entrees && !req.body.plats && !req.body.desserts && !req.body.boissons)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    try {
        var total = await Price.getAllPrices(req.body);
    } catch (err) {
        return res.status(400).send(err);
    }
    var newValues = {
        id_restaurant: req.userId,
        menus: req.body.menus,
        entrees: req.body.entrees,
        plats: req.body.plats,
        desserts: req.body.desserts,
        boissons: req.body.boissons,
        price: total,
        created_at: req.body.created_at ? req.body.created_at : Date.now(),
    };
    commandeService.commandeUpdate(req.params.id, newValues)
    .then(commandes => {
        return res.status(200).send(commandes);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// CREATE COMMAND
router.post('/commands', verifyToken, async function(req, res) {
    // CHECK BODY ERRORS
    if (!req.body.menus && !req.body.entrees && !req.body.plats && !req.body.desserts && !req.body.boissons)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    try {
        // VERIFY PRICE OF COMMAND
        var total = await Price.getAllPrices(req.body);
    } catch (err) {
        return res.status(400).send(err);
    }
    var newId = new ObjectID().toHexString();
    var newCommande = {
        _id: newId,
        id_restaurant: req.userId,
        menus: req.body.menus,
        entrees: req.body.entrees,
        plats: req.body.plats,
        desserts: req.body.desserts,
        boissons: req.body.boissons,
        price: total,
        created_at: new Date(),
    };
    // CREATE COMMAND
    commandeService.commandeCreate(newCommande)
    .then(filename => {
        // SEND QR CODE
        return res.status(200).sendFile(filename, {root: __dirname + "./../../uploads/"}, function(err) {
            if (err)
                return res.status(400).send("Echoue à l'envoie du fichier");
        });
    })
    .catch(err => {
        return res.status(400).send(err);
    })
})

module.exports = router;