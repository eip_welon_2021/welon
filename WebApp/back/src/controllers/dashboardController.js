// routes/controllers/dashboard.js
var express = require('express');
var router = express.Router();

var verifyToken = require('../services/modules/verifyToken');
var dashboardService = require('../services/dashboardServices');

// GET general ratings for a restaurant
router.get('/dashboard/rating/general', verifyToken, (req, res, next) => {
    var rank = -1;
    dashboardService.getRestaurantRating(req.userId)
    .then(r => {
        rank = r;
        dashboardService.generalRating(req.userId)
        .then(ratings => {
            ratings["rank"] = rank;
            return res.status(200).send(ratings);
        })
        .catch(err => {
            return res.status(400).send(err);
        })
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// GET best ratings for a restaurant
router.get('/dashboard/rating/best', verifyToken, async (req, res) => {
    try {
        var best = {
            best_entree: await dashboardService.getBestEntree(req.userId),
            best_plat: await dashboardService.getBestPlat(req.userId),
            best_dessert: await dashboardService.getBestDessert(req.userId),
            best_boisson: await dashboardService.getBestBoisson(req.userId),
            best_menu: await dashboardService.getBestMenu(req.userId),
        };
        return res.status(200).send(best);
    } catch (err) {
        return res.status(400).send(err);
    }
});

// GET worst ratings for a restaurant
router.get('/dashboard/rating/worst', verifyToken, async (req, res) => {
    try {
        var worst = {
            worst_entree: await dashboardService.getWorstEntree(req.userId),
            worst_plat: await dashboardService.getWorstPlat(req.userId),
            worst_dessert: await dashboardService.getWorstDessert(req.userId),
            worst_boisson: await dashboardService.getWorstBoisson(req.userId),
            worst_menu: await dashboardService.getWorstMenu(req.userId),
        };
        return res.status(200).send(worst);
    } catch (err) {
        return res.status(400).send(err);
    }
});


// GET most rated plates for a restaurant
router.get('/dashboard/rating/most/rated', verifyToken, async (req, res) => {
    try {
        var mostRated = {
            most_entree: await dashboardService.getMostEntree(req.userId),
            most_plat: await dashboardService.getMostPlat(req.userId),
            most_dessert: await dashboardService.getMostDessert(req.userId),
            most_boisson: await dashboardService.getMostBoisson(req.userId),
            most_menu: await dashboardService.getMostMenu(req.userId),
        };
        return res.status(200).send(mostRated);
    } catch (err) {
        return res.status(400).send(err);
    }
});

// GET less rated plates for a restaurant
router.get('/dashboard/rating/less/rated', verifyToken, async (req, res) => {
    try {
        var lessRated = {
            less_entree: await dashboardService.getLessEntree(req.userId),
            less_plat: await dashboardService.getLessPlat(req.userId),
            less_dessert: await dashboardService.getLessDessert(req.userId),
            less_boisson: await dashboardService.getLessBoisson(req.userId),
            less_menu: await dashboardService.getLessMenu(req.userId),
        };
        return res.status(200).send(lessRated);
    } catch (err) {
        return res.status(400).send(err);
    }
});

const dayjs = require('dayjs');

// Turnover between 2 dates
router.get('/dashboard/turnover', verifyToken, async (req, res) => {
    const time = req.query.time;
    const restaurantId = req.userId;
    if (!time) {
        return res.status(400).send('Il manque le champs time dans le body');
    }
    let total = 0;
    let listOfDays = [];
    for (let i = 0; i < time; i++) {
        const startOfDay = dayjs().subtract(i, 'day').hour(0).minute(0).second(0).millisecond(0);
        const endOfDay = dayjs().subtract(i, 'day').hour(23).minute(59).second(59).millisecond(999);
        let today = await dashboardService.getTurnover(restaurantId, startOfDay, endOfDay);
        if (!today) {
            today = 0;
        }
        total += today
        listOfDays.push(today);
    }
    return res.status(200).json({ total: total, listOfDays: listOfDays });
});


module.exports = router;