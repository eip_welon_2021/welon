// routes/menus/dessert.js
var express = require('express');
var router = express.Router();

var verifyToken = require('../services/modules/verifyToken');
var dessertService = require('../services/dessertServices');

// GET all desserts for the connected restaurant, requests to /api/desserts
router.get('/desserts', verifyToken, async (req, res) => {
    dessertService.dessertFindByRestaurant(req.userId)
    .then(desserts => {
        return res.status(200).send(desserts);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// GET all desserts for the id_restaurant, requests to /api/desserts
router.get('/desserts/:id', verifyToken, async (req, res) => {
    dessertService.dessertFindByRestaurant(req.params.id)
    .then(desserts => {
        return res.status(200).send(desserts);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// GET a dessert by ID
router.get('/desserts/infos/:id', verifyToken, async (req, res) => {
    dessertService.dessertFindById(req.params.id)
    .then(desserts => {
        return res.status(200).send(desserts);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// POST create a dessert, requests to /api/desserts
router.post('/desserts', verifyToken, async (req, res) => {
    if (!req.body.name || !req.body.desc || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    if (req.body.name.length > 20)
        return res.status(400).send("Le nom entré est trop long. (limite 20 charactères)");
    if (req.body.desc.length > 40)
        return res.status(400).send("La description entrée est trop longue. (limite 40 charactères)");
    var newDessert = {
        id_restaurant: req.userId,
        name: req.body.name,
        description: req.body.desc,
        price: req.body.price
    };
    dessertService.dessertCreate(newDessert)
    .then(desserts => {
        return res.status(200).send(desserts);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// PUT update a dessert, requests to /api/desserts/id_desserts
router.put('/desserts/:id', verifyToken, async (req, res) => {
    if (!req.body.name || !req.body.desc || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    if (req.body.name.length > 20)
        return res.status(400).send("Le nom entré est trop long. (limite 20 charactères)");
    if (req.body.desc.length > 40)
        return res.status(400).send("La description entrée est trop longue. (limite 40 charactères)");
    var newValues = {
        id_restaurant: req.userId,
        name: req.body.name,
        description: req.body.desc,
        price: req.body.price
    };
    dessertService.dessertUpdate(req.params.id, newValues)
    .then(desserts => {
        return res.status(200).send(desserts);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// DELETE a dessert, requests to /api/desserts/id_desserts
router.delete('/desserts/:id', verifyToken, async (req, res) => {
    dessertService.dessertDelete(req.params.id)
    .then(desserts => {
        return res.status(200).send(desserts);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

module.exports = router;