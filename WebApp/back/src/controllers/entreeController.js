// routes/menus/entree.js
var express = require('express');
var router = express.Router();

var verifyToken = require('../services/modules/verifyToken');
var entreeService = require('../services/entreeServices');

// GET all entrees for the connected restaurant, requests to /api/entrees
router.get('/entrees', verifyToken, async (req, res) => {
    entreeService.entreeFindByRestaurant(req.userId)
    .then(entrees => {
        return res.status(200).send(entrees);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// GET all entrees for the id_restaurant, requests to /api/entrees
router.get('/entrees/:id', verifyToken, async (req, res) => {
    entreeService.entreeFindByRestaurant(req.params.id)
    .then(entrees => {
        return res.status(200).send(entrees);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// GET an entrie by ID
router.get('/entrees/infos/:id', verifyToken, async (req, res) => {
    entreeService.entreeFindById(req.params.id)
    .then(entrees => {
        return res.status(200).send(entrees);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// POST create an entree, requests to /api/entrees
router.post('/entrees', verifyToken, async (req, res) => {
    if (!req.body.name || !req.body.desc || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    if (req.body.name.length > 20)
        return res.status(400).send("Le nom entré est trop long. (limite 20 charactères)");
    if (req.body.desc.length > 40)
        return res.status(400).send("La description entrée est trop longue. (limite 40 charactères)");
    var newEntree = {
        id_restaurant: req.userId,
        name: req.body.name,
        description: req.body.desc,
        price: req.body.price
    };
    entreeService.entreeCreate(newEntree)
    .then(entrees => {
        return res.status(200).send(entrees);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// PUT update an entree, requests to /api/entrees/id_entrees
router.put('/entrees/:id', verifyToken, async (req, res) => {
    if (!req.body.name || !req.body.desc || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    if (req.body.name.length > 20)
        return res.status(400).send("Le nom entré est trop long. (limite 20 charactères)");
    if (req.body.desc.length > 40)
        return res.status(400).send("La description entrée est trop longue. (limite 40 charactères)");
    var newValues = {
        id_restaurant: req.userId,
        name: req.body.name,
        description: req.body.desc,
        price: req.body.price
    };
    entreeService.entreeUpdate(req.params.id, newValues)
    .then(entrees => {
        return res.status(200).send(entrees);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// DELETE an entree, requests to /api/entrees/id_entrees
router.delete('/entrees/:id', verifyToken, async (req, res) => {
    entreeService.entreeDelete(req.params.id)
    .then(entrees => {
        return res.status(200).send(entrees);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

module.exports = router;