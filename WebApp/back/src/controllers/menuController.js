// routes/menus/restaurant.js
var express = require('express');
var router = express.Router();

var menuService = require('../services/menuServices');
var menuModule = require('../services/modules/menuModule');
var verifyToken = require('../services/modules/verifyToken');

// GET all menus, requests to /api/menus
router.get('/menus', verifyToken, async (req, res) => {
    menuService.menuGetAll()
    .then(menus => {
        return res.status(200).send(menus);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// GET all menus of a restaurant, requests to /api/menus/restaurant
router.get('/menus/restaurant', verifyToken, async (req, res) => {
    menuModule.findByRestaurant(req.userId)
    .then(list_of_menus => {
        return res.status(200).send(list_of_menus);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// GET a menu, requests to /api/menu/id_menu
router.get('/menu/:id', verifyToken, async (req, res) => {
    menuService.menuFindByMenu(req.params.id, res)
    .then(menus => {
        return res.status(200).send(menus);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// GET all menus of a restaurant, requests to /api/menus/id_restaurant
router.get('/menus/:id', verifyToken, async (req, res) => {
    menuService.menuFindByRestaurant(req.params.id)
    .then(menus => {
        return res.status(200).send(menus);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// POST create a menu, requests to /api/menu/restaurant
router.post('/menu/restaurant', verifyToken, async (req, res) => {
    if (!req.body.name || !req.body.desc || !req.body.price || !req.body.entrees
        || !req.body.plats || !req.body.desserts || !req.body.boissons) {
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    }
    if (req.body.name.length > 20)
        return res.status(400).send("Le nom entré est trop long. (limite 20 charactères)");
    if (req.body.desc.length > 40)
        return res.status(400).send("La description entrée est trop longue. (limite 40 charactères)");
    var newMenu = {
        id_restaurant: req.userId,
        name: req.body.name,
        description: req.body.desc,
        price: req.body.price,
        id_entree: req.body.entrees,
        id_plat: req.body.plats,
        id_dessert: req.body.desserts,
        id_boisson: req.body.boissons,
    };
    menuService.menuCreate(newMenu)
    .then(menus => {
        return res.status(200).send(menus);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// PUT update a menu, requests to /api/menu/id_menu
router.put('/menu/:id', verifyToken, async (req, res) => {
    if (!req.body.name || !req.body.desc || !req.body.price || !req.body.entrees
        || !req.body.plats || !req.body.desserts || !req.body.boissons) {
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    }
    if (req.body.name.length > 20)
        return res.status(400).send("Le nom entré est trop long. (limite 20 charactères)");
    if (req.body.desc.length > 40)
        return res.status(400).send("La description entrée est trop longue. (limite 40 charactères)");
    var newValues = {
        id_restaurant: req.userId,
        name: req.body.name,
        description: req.body.desc,
        price: req.body.price,
        id_entree: req.body.entrees,
        id_plat: req.body.plats,
        id_dessert: req.body.desserts,
        id_boisson: req.body.boissons,
    };
    menuService.menuUpdate(req.params.id, newValues)
    .then(menus => {
        return res.status(200).send(menus);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// DELETE a menu, requests to /api/menu/id_menu
router.delete('/menu/:id', verifyToken, async (req, res) => {
    menuService.menuDelete(req.params.id)
    .then(menus => {
        return res.status(200).send(menus);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

module.exports = router;