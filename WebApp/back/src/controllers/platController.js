// routes/menus/plat.js
var express = require('express');
var router = express.Router();

var verifyToken = require('../services/modules/verifyToken');
var platService = require('../services/platServices');

// GET all plats for the connected restaurant, requests to /api/plats
router.get('/plats', verifyToken, async (req, res) => {
    platService.platFindByRestaurant(req.userId)
    .then(plats => {
        return res.status(200).send(plats);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// GET all plats for the id_restaurant, requests to /api/plats
router.get('/plats/:id', verifyToken, async (req, res) => {
    platService.platFindByRestaurant(req.params.id)
    .then(plats => {
        return res.status(200).send(plats);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// GET a plate by ID
router.get('/plats/infos/:id', verifyToken, async (req, res) => {
    platService.platFindById(req.params.id)
    .then(plats => {
        return res.status(200).send(plats);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// POST create a plat, requests to /api/plats
router.post('/plats', verifyToken, async (req, res) => {
    if (!req.body.name || !req.body.desc || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    if (req.body.name.length > 20)
        return res.status(400).send("Le nom entré est trop long. (limite 20 charactères)");
    if (req.body.desc.length > 40)
        return res.status(400).send("La description entrée est trop longue. (limite 40 charactères)");
    var newPlat = {
        id_restaurant: req.userId,
        name: req.body.name,
        description: req.body.desc,
        price: req.body.price
    };
    platService.platCreate(newPlat)
    .then(plats => {
        return res.status(200).send(plats);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// PUT update a plat, requests to /api/plats/id_plats
router.put('/plats/:id', verifyToken, async (req, res) => {
    if (!req.body.name || !req.body.desc || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    if (req.body.name.length > 20)
        return res.status(400).send("Le nom entré est trop long. (limite 20 charactères)");
    if (req.body.desc.length > 40)
        return res.status(400).send("La description entrée est trop longue. (limite 40 charactères)");
    var newValues = {
        id_restaurant: req.userId,
        name: req.body.name,
        description: req.body.desc,
        price: req.body.price
    };
    platService.platUpdate(req.params.id, newValues)
    .then(plats => {
        return res.status(200).send(plats);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

// DELETE a plat, requests to /api/plats/id_plats
router.delete('/plats/:id', verifyToken, async (req, res) => {
    platService.platDelete(req.params.id)
    .then(plats => {
        return res.status(200).send(plats);
    })
    .catch(err => {
        return res.status(400).send(err);
    })
});

module.exports = router;