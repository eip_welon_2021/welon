// routes/menus/boisson.js
var express = require('express');
var router = express.Router();

var verifyToken = require('../services/modules/verifyToken');
var ratingService = require('../services/ratingServices');

// Store Restaurant ID in Ratings Collection
var getRestaurantEntree = require('../services/modules/getRestaurantEntree');
var getRestaurantPlat = require('../services/modules/getRestaurantPlat');
var getRestaurantDessert = require('../services/modules/getRestaurantDessert');
var getRestaurantBoisson = require('../services/modules/getRestaurantBoisson');
var getRestaurantMenu = require('../services/modules/getRestaurantMenu');

// Update average ratings in Entree | Plat | Dessert | Boisson | Menu Collections
var ratingModule = require('../services/modules/ratingModule');


// GET User feedbacks
router.get('/history', verifyToken, (req, res, next) => {
    ratingService.getHistory(req.userId, res);
})

// DELETE user delete a feedback by id
router.delete('/history/:id_rated', verifyToken, (req, res, next) => {
    ratingService.deleteRating(req.params.id_rated, res);
})

// POST a rate for boisson, requests to /api/rating/boissons
router.post('/rating/boissons/:id_boisson', verifyToken, getRestaurantBoisson, ratingModule.updateAverageBoisson, (req, res, next) => {
    if (!req.params.id_boisson || !req.body.quality || !req.body.quantity || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    var newRate = {
        id_user: req.userId,
        id_restaurant: req.id_restaurant,
        id_rated: req.params.id_boisson,
        type: 'boisson',
        rate_quality: req.body.quality,
        rate_quantity: req.body.quantity,
        rate_price: req.body.price,
        created_at: Date.now()
    };
    ratingService.ratingCreate(newRate, res);
});

// PATCH a rate for boisson, requests to /api/rating/boissons
router.patch('/rating/boissons/:id_boisson', verifyToken, getRestaurantBoisson, (req, res, next) => {
    if (!req.params.id_boisson || !req.body.quality || !req.body.quantity || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    var newRate = {
        id_user: req.userId,
        id_restaurant: req.id_restaurant,
        id_rated: req.params.id_boisson,
        type: 'boisson',
        rate_quality: req.body.quality,
        rate_quantity: req.body.quantity,
        rate_price: req.body.price,
        created_at: Date.now()
    };
    ratingService.ratingUpdate(req.params.id_boisson, req.userId, newRate, res);
});

// GET the average of ratings for boisson, requests to /api/rating/boissons/average
router.get('/rating/boissons/average/:id_boisson', verifyToken, (req, res, next) => {
    if (!req.params.id_boisson)
        return res.status(400).send("Aucun ID n'a été communiqué.");
    ratingService.getRatingAverage(req.params.id_boisson, 'boisson', res);
});

// POST a rate for entree, requests to /api/rating/entrees
router.post('/rating/entrees/:id_entree', verifyToken, getRestaurantEntree, ratingModule.updateAverageEntree, (req, res, next) => {
    if (!req.params.id_entree || !req.body.quality || !req.body.quantity || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    var newRate = {
        id_user: req.userId,
        id_restaurant: req.id_restaurant,
        id_rated: req.params.id_entree,
        type: 'entree',
        rate_quality: req.body.quality,
        rate_quantity: req.body.quantity,
        rate_price: req.body.price,
        created_at: Date.now()
    };
    ratingService.ratingCreate(newRate, res);
});

// PATCH a rate for entree, requests to /api/rating/entrees
router.patch('/rating/entrees/:id_entree', verifyToken, getRestaurantEntree, (req, res, next) => {
    if (!req.params.id_entree || !req.body.quality || !req.body.quantity || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    var newRate = {
        id_user: req.userId,
        id_restaurant: req.id_restaurant,
        id_rated: req.params.id_entree,
        type: 'entree',
        rate_quality: req.body.quality,
        rate_quantity: req.body.quantity,
        rate_price: req.body.price,
        created_at: Date.now()
    };
    ratingService.ratingUpdate(req.params.id_entree, req.userId, newRate, res);
});

// GET the average of ratings for entrees, requests to /api/rating/entrees/average
router.get('/rating/entrees/average/:id_entree', verifyToken, (req, res, next) => {
    if (!req.params.id_entree)
        return res.status(400).send("Aucun ID n'a été communiqué.");
    ratingService.getRatingAverage(req.params.id_entree, 'entree', res);
});

// POST a rate for plat, requests to /api/rating/plats
router.post('/rating/plats/:id_plat', verifyToken, getRestaurantPlat, ratingModule.updateAveragePlat, (req, res, next) => {
    if (!req.params.id_plat || !req.body.quality || !req.body.quantity || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    var newRate = {
        id_user: req.userId,
        id_restaurant: req.id_restaurant,
        id_rated: req.params.id_plat,
        type: 'plat',
        rate_quality: req.body.quality,
        rate_quantity: req.body.quantity,
        rate_price: req.body.price,
        created_at: Date.now()
    };
    ratingService.ratingCreate(newRate, res);
});

// PATCH a rate for plat, requests to /api/rating/plats
router.patch('/rating/plats/:id_plat', verifyToken, getRestaurantPlat, (req, res, next) => {
    if (!req.params.id_plat || !req.body.quality || !req.body.quantity || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    var newRate = {
        id_user: req.userId,
        id_restaurant: req.id_restaurant,
        id_rated: req.params.id_plat,
        type: 'plat',
        rate_quality: req.body.quality,
        rate_quantity: req.body.quantity,
        rate_price: req.body.price,
        created_at: Date.now()
    };
    ratingService.ratingUpdate(req.params.id_plat, req.userId, newRate, res);
});

// GET the average of ratings for plats, requests to /api/rating/plats/average
router.get('/rating/plats/average/:id_plat', verifyToken, (req, res, next) => {
    if (!req.params.id_plat)
        return res.status(400).send("Aucun ID n'a été communiqué.");
    ratingService.getRatingAverage(req.params.id_plat, 'plat', res);
});

// POST a rate for desserts, requests to /api/rating/desserts
router.post('/rating/desserts/:id_dessert', verifyToken, getRestaurantDessert, ratingModule.updateAverageDessert, (req, res, next) => {
    if (!req.params.id_dessert || !req.body.quality || !req.body.quantity || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    var newRate = {
        id_user: req.userId,
        id_restaurant: req.id_restaurant,
        id_rated: req.params.id_dessert,
        type: 'dessert',
        rate_quality: req.body.quality,
        rate_quantity: req.body.quantity,
        rate_price: req.body.price,
        created_at: Date.now()
    };
    ratingService.ratingCreate(newRate, res);
});

// PATCH a rate for desserts, requests to /api/rating/desserts
router.patch('/rating/desserts/:id_dessert', verifyToken, getRestaurantDessert, (req, res, next) => {
    if (!req.params.id_dessert || !req.body.quality || !req.body.quantity || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    var newRate = {
        id_user: req.userId,
        id_restaurant: req.id_restaurant,
        id_rated: req.params.id_dessert,
        type: 'dessert',
        rate_quality: req.body.quality,
        rate_quantity: req.body.quantity,
        rate_price: req.body.price,
        created_at: Date.now()
    };
    ratingService.ratingUpdate(req.params.id_dessert, req.userId, newRate, res);
});

// GET the average of ratings for desserts, requests to /api/rating/desserts/average
router.get('/rating/desserts/average/:id_dessert', verifyToken, (req, res, next) => {
    if (!req.params.id_dessert)
        return res.status(400).send("Aucun ID n'a été communiqué.");
    ratingService.getRatingAverage(req.params.id_dessert, 'dessert', res);
});

// POST a rate for menu, requests to /api/rating/menus
router.post('/rating/menus/:id_menu', verifyToken, getRestaurantMenu, ratingModule.updateAverageMenu, (req, res, next) => {
    if (!req.params.id_menu || !req.body.quality || !req.body.quantity || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    var newRate = {
        id_user: req.userId,
        id_restaurant: id_restaurant,
        id_rated: req.params.id_menu,
        type: 'menu',
        rate_quality: req.body.quality,
        rate_quantity: req.body.quantity,
        rate_price: req.body.price,
        created_at: Date.now()
    };
    ratingService.ratingCreate(newRate, res);
});

// PATCH a rate for menu, requests to /api/rating/menus
router.patch('/rating/menus/:id_menu', verifyToken, getRestaurantMenu, (req, res, next) => {
    if (!req.params.id_menu || !req.body.quality || !req.body.quantity || !req.body.price)
        return res.status(400).send("Les champs n'ont pas été remplis correctement.");
    var newRate = {
        id_user: req.userId,
        id_restaurant: id_restaurant,
        id_rated: req.params.id_menu,
        type: 'menu',
        rate_quality: req.body.quality,
        rate_quantity: req.body.quantity,
        rate_price: req.body.price,
        created_at: Date.now()
    };
    ratingService.ratingUpdate(req.params.id_menu, req.userId, newRate, res);
});

// GET the average of ratings for menus, requests to /api/rating/menus/average
router.get('/rating/menus/average/:id_menu', verifyToken, (req, res, next) => {
    if (!req.params.id_menu)
        return res.status(400).send("Aucun ID n'a été communiqué.");
    ratingService.getRatingAverage(req.params.id_menu, 'menu', res);
});

router.get('/rating/restaurant/top', (req, res, next) => {
    ratingService.getTopFive(res);
});

router.get('/rating/restaurant/top/plats', verifyToken, (req, res, next) => {
    ratingService.getTopFivePlats(req, res, next);
})

router.get('/rating/restaurant/:id_restaurant', verifyToken, (req, res, next) => {
    if (!req.params.id_restaurant)
        return res.status(400).send("Aucun ID n'a été communiqué.");
    ratingService.getRankRest(req.params.id_restaurant, res);
});

module.exports = router;