//routes/auth/authRestaurant.js

var express = require('express');
var router = express.Router();

var bcrypt = require('bcryptjs');
var validator = require("email-validator");
var pwd = require('../services/modules/checkpassword');

var restaurantService = require('../services/restaurantServices');
var verifyToken = require('../services/modules/verifyToken');

// POST Register a restaurant
router.post('/register/restaurant', function(req, res) {
    if (!req.body.email || !req.body.password || !req.body.name || !req.body.address || !req.body.phone || !req.body.longitude || !req.body.latitude)
      return res.status(400).send("Il manque un ou des champ(s).");
    if (!validator.validate(req.body.email))
        return res.status(400).send("Email invalide ou déjà pris.");
    if (!pwd.checkpassword(req.body.password))
        return res.status(400).send("Le mot de passe doit contenir 8 characteres minimum incluant un chiffre, une minuscule et une majuscule.");

    var hashedPassword = bcrypt.hashSync(req.body.password, 8);

    var data = {
        email : req.body.email,
        password : hashedPassword,
        name: req.body.name,
        address: req.body.address,
        phone: req.body.phone,
        longitude: req.body.longitude,
        latitude: req.body.latitude
    }
    restaurantService.restaurantRegister(data, res);
});

// POST login a restaurant
router.post('/login/restaurant', function(req, res) {
    if (!req.body.email || !req.body.password)
      return res.status(400).send("Il manque un champ (email / password).");
    restaurantService.restaurantLogin(req.body.email, req.body.password, res);
});

// GET all restaurants, requests to /api/auth/restaurant
// TODO
router.get('/restaurant', (req, res, next) => {
  restaurantService.restaurantFindAll(res);
});

// DELETE a restaurant, requests to /api/auth/restaurant
router.delete('/restaurant', verifyToken, async (req, res, next) => {
  const email = req.body.email;
  const restaurant = await restaurantService.findRestaurantById(req.userId);
  if (restaurant?.restaurant.email !== email) {
    return res.status(403).send('Only the restaurant himself can delete his account');
  }
  restaurantService.restaurantRemoveOne(req.body.email, res);
});

// UPDATE a restaurant
router.put('/restaurant', verifyToken, (req, res, next) => {
  if (!req.body.name || !req.body.phone || !req.body.address || !req.body.longitude || !req.body.latitude)
      return res.status(400).send("Les champs n'ont pas été remplis correctement.");
  var newValues = {
    name: req.body.name,
    phone: req.body.phone,
    address: req.body.address,
    longitude: req.body.longitude,
    latitude: req.body.latitude,
  }
  restaurantService.updateRestaurant(req.userId, newValues)
  .then((restaurant) => {
        return res.status(200).send(restaurant);
    })
    .catch((err) => {
        return res.status(400).send(err);
    })
});

module.exports = router;