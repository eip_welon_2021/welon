// /routes/auth/authUser.js

var express = require('express');
var router = express.Router();

var validator = require("email-validator");
var pwd = require('../services/modules/checkpassword');
var bcrypt = require('bcryptjs');

var userService = require('../services/userServices');
const verifyToken = require('../services/modules/verifyToken');

// POST create a user, requests to /api/auth/register/user
router.post('/register/user', function(req, res) {
    if (!req.body.email || !req.body.password || !req.body.firstname || !req.body.lastname)
      return res.status(400).send("Il manque un champ (email / password / firstname / lastname).");
    if (!validator.validate(req.body.email))
        return res.status(400).send("Email invalide ou déjà pris.");
    if (!pwd.checkpassword(req.body.password))
        return res.status(400).send("Le mot de passe doit contenir 8 characteres minimum incluant un chiffre, une minuscule et une majuscule.");

    var hashedPassword = bcrypt.hashSync(req.body.password, 8);

    var data = {
        email : req.body.email,
        password : hashedPassword,
        firstname: req.body.firstname,
        lastname: req.body.lastname
    }
    userService.userRegister(data, res);
});

// POST verify login for a user, requests to /api/auth/login/user
router.post('/login/user', function(req, res) {
    if (!req.body.email || !req.body.password)
      return res.status(400).send("Il manque un champ (email / password).");
    userService.userLogin(req.body.email, req.body.password, res);
});

// GET all users, requests to /api/user
router.get('/user', (req, res, next) => {
    userService.userFindAll(res);
});

// DELETE a user, requests to /api/user
router.delete('/user', verifyToken, async (req, res, next) => {
    const email = req.body.email;
    const user = await userService.findUserById(req.userId);
    console.log(user);
    if (user?.user.email !== email) {
        return res.status(403).send('Only the user himself can delete his account');
    }
    userService.userRemoveOne(email, res);
});

module.exports = router;