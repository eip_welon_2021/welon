// models/boisson.js
const mongoose = require('mongoose');

const boissonSchema = mongoose.Schema({
    id_restaurant: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    price: {
        type: String,
        required: true,
    },
    general_average: {
        type: Number,
    },
    quality_average: {
        type: Number,
    },
    quantity_average: {
        type: Number,
    },
    price_average: {
        type: Number,
    },
    rating_numbers: {
        type: Number,
    },
});

module.exports = mongoose.model('Boisson', boissonSchema);