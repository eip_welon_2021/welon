// models/commande.js
const mongoose = require('mongoose');
var ObjectID = require('mongodb').ObjectID;

const commandeSchema = mongoose.Schema({
    _id: {
        type: ObjectID,
        required: true,
    },
    id_restaurant: {
        type: String,
        required: true,
    },
    id_user: {
        type: String,
    },
    menus: {
        type: [[String]],
    },
    entrees: {
        type: [[String]],
    },
    plats: {
        type: [[String]],
    },
    desserts: {
        type: [[String]],
    },
    boissons: {
        type: [[String]],
    },
    price: {
        type: Number,
    },
    created_at: {
        type: Date,
    }
});

module.exports = mongoose.model('Commande', commandeSchema);