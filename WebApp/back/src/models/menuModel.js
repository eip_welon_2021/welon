// models/menu.js
const mongoose = require('mongoose');

const menuSchema = mongoose.Schema({
    id_restaurant: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    price: {
        type: String,
        required: true,
    },
    id_entree: {
        type: [String],
    },
    id_plat: {
        type: [String],
    },
    id_dessert: {
        type: [String],
    },
    id_boisson: {
        type: [String],
    },
    general_average: {
        type: Number,
    },
    quality_average: {
        type: Number,
    },
    quantity_average: {
        type: Number,
    },
    price_average: {
        type: Number,
    },
    rating_numbers: {
        type: Number,
    },
});

module.exports = mongoose.model('Menu', menuSchema);