// models/rating.js
const mongoose = require('mongoose');

const ratingSchema = mongoose.Schema({
    id_user: {
        type: String,
        required: true,
    },
    id_restaurant: {
        type: String,
    },
    id_rated: {
        type: String,
        required: true,
    },
    type: {
        type: String,
        required: true,
    },
    rate_quality: {
        type: Number,
        required: true,
    },
    rate_quantity: {
        type: Number,
        required: true,
    },
    rate_price: {
        type: Number,
        required: true,
    },
    created_at: {
        type: Date,
        required: true
    }
});

module.exports = mongoose.model('Rating', ratingSchema);