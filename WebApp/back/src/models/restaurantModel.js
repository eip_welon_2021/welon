// models/restaurant.js
const mongoose = require('mongoose');

const restaurantSchema = mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    address: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
    },
    img: { 
        type: String,
    },
    verified: {
        type: Boolean,
        default: false,
    },
    longitude: {
        type: Number,
    },
    latitude: {
        type: Number,
    },
    image: {
        file_id: String,
        contentType: String,
    },
    rank: {
        type: String,
    }
});

module.exports = mongoose.model('Restaurant', restaurantSchema);