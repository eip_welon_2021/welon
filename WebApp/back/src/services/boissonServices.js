var boissonModel = require('../models/boissonModel');
const { resolve } = require('path');

class BoissonService {

    async boissonFindById(_id_boisson) {
        return new Promise((resolve, reject) => {
            boissonModel.find({_id: _id_boisson})
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Boisson pas trouvée.");
            });
        })
    }

    async boissonFindByRestaurant(_id_restaurant) {
        return new Promise((resolve, reject) => {
            boissonModel.find({id_restaurant: _id_restaurant})
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Boisson pas trouvée.");
            });
        });
    }

    async boissonCreate(newBoisson) {
        return new Promise((resolve, reject) => {
            var newB = new boissonModel(newBoisson);
            newB.save()
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Problème lors de la création de la boisson.");
            })
        })
    }

    async boissonUpdate(id, newValues) {
        return new Promise((resolve, reject) => {
            boissonModel.updateOne({_id: id}, newValues)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucune boisson trouvée avec cet id " + id + ".");
            })
        })
    }

    async boissonDelete(id) {
        return new Promise((resolve, reject) => {
            boissonModel.remove({ _id: id })
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Boisson pas trouvée.");
            });
        })
    }

    async getNameByMenus(ids_boisson) {
        return new Promise((resolve, reject) => {
            boissonModel.find({_id: {$in: ids_boisson}})
            .then(docs => {
                var names = [];
                for (var i = 0; i < docs.length; i++) {
                    names.push(docs[i].name);
                }
                resolve(names);
            })
            .catch(err => {
                resolve([]);
            })
        })
    }
}

var boissonService = new BoissonService();

module.exports = boissonService;