var commandeModel = require('../models/commandeModel');
var restaurantServices = require('./restaurantServices');
var QRCode = require('qrcode');

class CommandeService {

    async commandeAddUser(id, userId) {
        return new Promise((resolve, reject) => {
            var newValues = {
                id_user: userId
            }
            commandeModel.updateOne({_id: id}, newValues)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucune commande trouvée à cet id.");
            })
        })
    }

    async commandeFindById(id) {
        return new Promise((resolve, reject) => {
            commandeModel.find({_id: id})
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucune commande trouvée.");
            });
        })
    }

    async commandeFindByUser(_id_user) {
        return new Promise((resolve, reject) => {
            commandeModel.find({id_user: _id_user})
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucune commande trouvée.");
            });
        })
    }

    async commandeFindByRestaurant(_id_restaurant) {
        return new Promise((resolve, reject) => {
            commandeModel.find({id_restaurant: _id_restaurant})
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucune commande trouvée.");
            });
        })
    }

    async commandeQrcode(_id_command) {
        return new Promise((resolve, reject) => {
            commandeModel.find({_id: _id_command})
            .then(docs => {
                QRCode.toFile("./uploads/" + _id_command + ".png", _id_command, function (err) {
                    if (err)
                        reject("Echoue à la création du qrcode.");
                    resolve(_id_command + ".png");
                });
            })
            .catch(err => {
                reject("Aucune commande trouvée.");
            })
        })
    }

    async commandeUpdate(id, newValues) {
        return new Promise((resolve, reject) => {
            commandeModel.updateOne({_id: id}, newValues)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucune commande trouvée avec cet id " + id + ".");
            })
        })
    }

    async commandeCreate(newCommande) {
        return new Promise((resolve, reject) => {
            restaurantServices.findRestaurantById(newCommande.id_restaurant)
            .then(() => {
                    var newC = new commandeModel(newCommande);
                    newC.save()
                    .catch(err => {
                        reject("Echoue à l'enregistrement d'une commande.");
                    })
                    QRCode.toFile("./uploads/" + newCommande._id + ".png", newCommande._id, function (err) {
                        if (err)
                            reject("Echoue à la création du qrcode.");
                        resolve(newCommande._id + ".png");
                    })
            })
            .catch(err => {
                reject("Mauvais id restaurant.");
            })
        })
    }

    async commandeDelete(id) {
        return new Promise((resolve, reject) => {
            commandeModel.remove({ _id: id })
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucune commande n'a été trouvée avec cet id " + id + ".");
            });
        })
    }
}

var commandeService = new CommandeService();

module.exports = commandeService;