var entreeModel = require('../models/entreeModel');
var platModel = require('../models/platModel');
var dessertModel = require('../models/dessertModel');
var boissonModel = require('../models/boissonModel');
var menuModel = require('../models/menuModel');
var ratingModel = require('../models/ratingModel');
const commandModel = require('../models/commandeModel');

class DashboardService {

    async generalRating(id_resto) {
        return new Promise((resolve, reject) => {
            var quality = 0;
            var quantity = 0;
            var price = 0;
            var entree_quantity = 0;
            var plat_quantity = 0;
            var dessert_quantity = 0;
            var boisson_quantity = 0;
            var menu_quantity = 0;
            ratingModel.find({id_restaurant: id_resto})
            .then(docs => {
                for (var i = 0; i < docs.length; i++) {
                    quality += docs[i].rate_quality;
                    quantity += docs[i].rate_quantity;
                    price += docs[i].rate_price;
                    if (docs[i].type == "entree")
                        entree_quantity++;
                    else if (docs[i].type == "plat")
                        plat_quantity++;
                    else if (docs[i].type == "dessert")
                        dessert_quantity++;
                    else if (docs[i].type == "boisson")
                        boisson_quantity++;
                    else if (docs[i].type == "menu")
                        menu_quantity++;
                }
                var average = {
                    ratings_number: docs.length,
                    quality_average: quality / docs.length,
                    quantity_average: quantity / docs.length,
                    price_average: price / docs.length,
                    general_average: ((quality / docs.length) + (quantity / docs.length) + (price / docs.length)) / 3,
                    entree_quantity: entree_quantity,
                    plat_quantity: plat_quantity,
                    dessert_quantity: dessert_quantity,
                    boisson_quantity: boisson_quantity,
                    menu_quantity: menu_quantity,
                };
                resolve(average);
            })
            .catch(err => {
                reject("Aucune note trouvée avec cet id " + id + ".");
            });
        })
    }
    
    async getRestaurantRating(id_restaurant) {
        return new Promise((resolve, reject) => {
            var rest = {};
            ratingModel.find()
            .then(docs => {
                for (var i = 0; i < docs.length; i++) {
                    if (rest.hasOwnProperty(docs[i].id_restaurant)) {
                        rest[docs[i].id_restaurant]['rating'] += docs[i].rate_quantity + docs[i].rate_price + docs[i].rate_quality;
                        rest[docs[i].id_restaurant]['nb'] += 3;
                    } else {
                        rest[docs[i].id_restaurant] = {'rating': 0, 'nb': 3};
                        rest[docs[i].id_restaurant]['rating'] = docs[i].rate_quantity + docs[i].rate_quality + docs[i].rate_price;
                    }
                }
                var list = [];
                var rate = {};
                for (var r in rest) {
                    if (rest.hasOwnProperty(r)) {
                        rate = rest[r]['rating'] / rest[r]['nb']
                        list.push({restaurant: r, rate: rate})
                    }
                }
                list.sort((a, b) => (a.rate > b.rate) ? 1 : -1)
                var index = list.findIndex(x => x.restaurant === id_restaurant)
                if (index === -1) {
                    return {message: "This restaurant has no ratings."};
                }
                resolve({rank: list.length - index - 1, nbRestaurant: list.length});
            })
            .catch(err => {
                reject(err);
            })
        })
    }

    async getBestEntree(id_resto) {
        return new Promise((resolve, reject) => {
            entreeModel.find({id_restaurant: id_resto}).sort({general_average:-1}).limit(5)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }

    async getBestPlat(id_resto) {
        return new Promise((resolve, reject) => {
            platModel.find({id_restaurant: id_resto}).sort({general_average:-1}).limit(5)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        });
    }
    
    async getBestDessert(id_resto) {
        return new Promise((resolve, reject) => {
            dessertModel.find({id_restaurant: id_resto}).sort({general_average:-1}).limit(5)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getBestBoisson(id_resto) {
        return new Promise((resolve, reject) => {
            boissonModel.find({id_restaurant: id_resto}).sort({general_average:-1}).limit(5)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getBestMenu(id_resto) {
        return new Promise((resolve, reject) => {
            menuModel.find({id_restaurant: id_resto}).sort({general_average:-1}).limit(5)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getWorstEntree(id_resto) {
        return new Promise((resolve, reject) => {
            entreeModel.find({id_restaurant: id_resto}).sort({general_average:+1}).limit(5)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getWorstPlat(id_resto) {
        return new Promise((resolve, reject) => {
            platModel.find({id_restaurant: id_resto}).sort({general_average:+1}).limit(5)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getWorstDessert(id_resto) {
        return new Promise((resolve, reject) => {
            dessertModel.find({id_restaurant: id_resto}).sort({general_average:+1}).limit(5)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getWorstBoisson(id_resto) {
        return new Promise((resolve, reject) => {
            boissonModel.find({id_restaurant: id_resto}).sort({general_average:+1}).limit(5)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getWorstMenu(id_resto) {
        return new Promise((resolve, reject) => {
            menuModel.find({id_restaurant: id_resto}).sort({general_average:+1}).limit(5)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }

    async getMostEntree(id_resto) {
        return new Promise((resolve, reject) => {
            entreeModel.find({id_restaurant: id_resto}).sort({rating_numbers:-1}).limit(1)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getMostPlat(id_resto) {
        return new Promise((resolve, reject) => {
            platModel.find({id_restaurant: id_resto}).sort({rating_numbers:-1}).limit(1)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getMostDessert(id_resto) {
        return new Promise((resolve, reject) => {
            dessertModel.find({id_restaurant: id_resto}).sort({rating_numbers:-1}).limit(1)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getMostBoisson(id_resto) {
        return new Promise((resolve, reject) => {
            boissonModel.find({id_restaurant: id_resto}).sort({rating_numbers:-1}).limit(1)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getMostMenu(id_resto) {
        return new Promise((resolve, reject) => {
            menuModel.find({id_restaurant: id_resto}).sort({rating_numbers:-1}).limit(1)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }

    async getLessEntree(id_resto) {
        return new Promise((resolve, reject) => {
            entreeModel.find({id_restaurant: id_resto}).sort({rating_numbers:+1}).limit(1)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getLessPlat(id_resto) {
        return new Promise((resolve, reject) => {
            platModel.find({id_restaurant: id_resto}).sort({rating_numbers:+1}).limit(1)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getLessDessert(id_resto) {
        return new Promise((resolve, reject) => {
            dessertModel.find({id_restaurant: id_resto}).sort({rating_numbers:+1}).limit(1)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getLessBoisson(id_resto) {
        return new Promise((resolve, reject) => {
            boissonModel.find({id_restaurant: id_resto}).sort({rating_numbers:+1}).limit(1)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    
    async getLessMenu(id_resto) {
        return new Promise((resolve, reject) => {
            menuModel.find({id_restaurant: id_resto}).sort({rating_numbers:+1}).limit(1)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject(err);
            })
        })
    }    

    async getTurnover(restaurantId, startDate, endDate) {
        return new Promise((resolve, reject) => {
            commandModel.find({id_restaurant: restaurantId, created_at: { $gte: startDate, $lte: endDate }})
            .then(docs => {
                let total = 0;
                for (let i = 0; i < docs.length; i++) {
                    total += docs[i].price;
                }
                resolve(total);
            })
            .catch(err => {
                console.log(err);
                reject(err);
            })
        });
    }


}

var dashboardService = new DashboardService();

module.exports = dashboardService;