var dessertModel = require('../models/dessertModel');

class DessertService {

    async dessertFindById(id_dessert) {
        return new Promise((resolve, reject) => {
            dessertModel.find({_id: id_dessert})
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucun dessert trouvé.");
            });
        })
    }

    async dessertFindByRestaurant(_id_restaurant) {
        return new Promise((resolve, reject) => {
            dessertModel.find({id_restaurant: _id_restaurant})
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucune dessert trouvé.");
            });
        })
    }

    async dessertCreate(newDessert) {
        return new Promise((resolve, reject) => {
            var newD = new dessertModel(newDessert);
            newD.save()
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("La création du dessert a échoué.");
            })
        })
    }

    async dessertUpdate(id, newValues) {
        return new Promise((resolve, reject) => {
            dessertModel.updateOne({_id: id}, newValues)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucun dessert trouvé avec cet id " + id + ".");
            })
        })
    }

    async dessertDelete(id) {
        return new Promise((resolve, reject) => {
            dessertModel.remove({ _id: id })
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucun dessert n'a été trouvé avec cet id " + id + ".");
            });
        })
    }

    async getNameByMenus(ids_dessert) {
        return new Promise((resolve, reject) => {
            dessertModel.find({_id: {$in: ids_dessert}})
            .then(docs => {
                var names = [];
                for (var i = 0; i < docs.length; i++) {
                    names.push(docs[i].name);
                }
                resolve(names);
            })
            .catch(err => {
                resolve([]);
            })
        })
    }
}

var dessertService = new DessertService();

module.exports = dessertService;