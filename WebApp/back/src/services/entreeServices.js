var entreeModel = require('../models/entreeModel');

class EntreeService {

    async entreeFindById(id_entree) {
        return new Promise((resolve, reject) => {
            entreeModel.find({_id: id_entree})
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucune entree trouvée.");
            });
        })
    }

    async entreeFindByRestaurant(_id_restaurant) {
        return new Promise((resolve, reject) => {
            entreeModel.find({id_restaurant: _id_restaurant})
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucune entree trouvée.");
            });
        })
    }

    async entreeCreate(newEntree) {
        return new Promise((resolve, reject) => {
            var newE = new entreeModel(newEntree);
            newE.save()
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("La création de l'entrée a échoué.");
            })
        })
    }

    async entreeUpdate(id, newValues) {
        return new Promise((resolve, reject) => {
            entreeModel.updateOne({_id: id}, newValues)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucune entrée trouvée avec cet id " + id + ".");
            })
        })
    }

    async entreeDelete(id) {
        return new Promise((resolve, reject) => {
            entreeModel.remove({ _id: id })
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucune entree n'a été trouvée avec cet id " + id + ".");
            });
        })
    }

    async getNameByMenus(ids_entree) {
        return new Promise((resolve, reject) => {
            entreeModel.find({_id: {$in: ids_entree}})
            .then(docs => {
                var names = [];
                for (var i = 0; i < docs.length; i++) {
                    names.push(docs[i].name);
                }
                resolve(names);
            })
            .catch(err => {
                resolve([]);
            })
        })
    }
}

var entreeService = new EntreeService();

module.exports = entreeService;