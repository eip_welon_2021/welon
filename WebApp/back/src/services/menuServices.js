var menuModel = require('../models/menuModel');

class MenuService {

    async menuGetAll() {
        return new Promise((resolve, reject) => {
            menuModel.find()
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucun menu n'a été trouvé.");
            })
        })
    }

    async menuFindByMenu(_id_menu) {
        return new Promise((resolve, reject) => {
            menuModel.find({_id: _id_menu})
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucun menu trouvé avec cet id " + req.params.id + ".");
            })
        })
    }

    async menuFindByRestaurant(_id_restaurant) {
        return new Promise((resolve, reject) => {
        menuModel.find({id_restaurant: _id_restaurant})
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucune menu trouvé.");
            });
        })
    }

    async menuCreate(newMenu) {
        return new Promise((resolve, reject) => {
            var newM = new menuModel(newMenu);
            newM.save()
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("La création du menu a échoué.");
            })
        })
    }

    async menuUpdate(id, newValues) {
        return new Promise((resolve, reject) => {
            menuModel.updateOne({_id: id}, newValues)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucun menu trouvé avec cet id " + id + ".");
            })
        })
    }

    async menuDelete(id) {
        return new Promise((resolve, reject) => {
            menuModel.remove({ _id: id })
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucun menu n'a été trouvé avec cet id " + id + ".");
            });
        })
    }
}

var menuService = new MenuService();

module.exports = menuService;