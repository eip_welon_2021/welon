//getRestaurantBoisson.js
var Boissons = require('../../models/boissonModel');

function getRestaurantBoisson(req, res, next) {
  Boissons.findOne({_id: req.params.id_boisson})
  .exec()
  .then(docs => {
      req.id_restaurant = docs.id_restaurant;
      next();
  })
  .catch(err => {
      console.log(err);
      return res.status(400).send("Aucune boisson trouvée.");
  });
}

module.exports = getRestaurantBoisson;