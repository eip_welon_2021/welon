//getRestaurantDessert.js
var Desserts = require('../../models/dessertModel');

function getRestaurantDessert(req, res, next) {
  Desserts.findOne({_id: req.params.id_dessert})
  .exec()
  .then(docs => {
      req.id_restaurant = docs.id_restaurant;
      next();
  })
  .catch(err => {
      console.log(err);
      return res.status(400).send("Aucun dessert trouvé.");
  });
}

module.exports = getRestaurantDessert;