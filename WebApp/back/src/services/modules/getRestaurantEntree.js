//getRestaurantEntree.js
var Entrees = require('../../models/entreeModel');

function getRestaurantEntree(req, res, next) {
  Entrees.findOne({_id: req.params.id_entree})
  .exec()
  .then(docs => {
      req.id_restaurant = docs.id_restaurant;
      next();
  })
  .catch(err => {
      console.log(err);
      return res.status(400).send("Aucune entrée trouvée.");
  });
}

module.exports = getRestaurantEntree;