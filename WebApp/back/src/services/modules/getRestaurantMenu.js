//getRestaurantMenu.js
var Menus = require('../../models/menuModel');

function getRestaurantMenu(req, res, next) {
  Menus.findOne({_id: req.params.id_menu})
  .exec()
  .then(docs => {
      req.id_restaurant = docs.id_restaurant;
      next();
  })
  .catch(err => {
      console.log(err);
      return res.status(400).send("Aucun menu trouvé.");
  });
}

module.exports = getRestaurantMenu;