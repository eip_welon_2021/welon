//getRestaurantPlat.js
var Plats = require('../../models/platModel');

function getRestaurantPlat(req, res, next) {
  Plats.findOne({_id: req.params.id_plat})
  .exec()
  .then(docs => {
      req.id_restaurant = docs.id_restaurant;
      next();
  })
  .catch(err => {
      console.log(err);
      return res.status(400).send("Aucun plat trouvé.");
  });
}

module.exports = getRestaurantPlat;