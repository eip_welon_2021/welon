const nodemailer = require('nodemailer');

module.exports = {

    setTransport: function() {
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'welon2021@gmail.com',
                pass: 'Welon01Welon01'
            }
        });
        return transporter;
    },

    sendWelcomeRestaurant: async function(user_id, receiver, token) {
        var transporter = this.setTransport();
        var mailOptions = {
            from: 'welon2021@gmail.com',
            to: receiver,
            subject: 'Confirmation d\'inscription',
            html: 'Welcome!<br><br>Merci d\'avoir rejoint la plateforme Welon. Nous avons créé ce projet afin d\'aider les restaurants à lutter contre le gaspilllage alimentaire.<br>N\'hésitez pas à nous communiquer vos retours afin que l\'on puisse continuellement améliorer notre plateforme. Si vous avez des questions, vous pouvez nous envoyer un message à cette adresse mail : <a href="mailto:welon2021@gmail.com">welon2021@gmail.com</a>.<br/>Nous restons à votre écoute et nous sommes heureux de pouvoir collaborer avec vous.<br><br><a href="https://welon.fr/api/auth/email/verification/restaurant/' + token + '">Confirmez votre compte<a><br><br>La Groupe Welon<br><br>Si vous ne vous êtes pas inscrit sur la plateforme Welon, contactez nous <a href="mailto:welon2021@gmail.com">ici</a>.'
        };
        transporter.sendMail(mailOptions, function(err, info) {
            if (err)
                console.log(err);
            else
                console.log(info);
        });
    },

    sendWelcomeUser: async function(user_id, receiver) {
        var transporter = this.setTransport();
        var mailOptions = {
            from: 'welon2021@gmail.com',
            to: receiver,
            subject: 'Confirmation d\'inscription',
            html: 'Welcome!<br><br>Merci d\'avoir rejoint la plateforme Welon. Nous avons créé ce projet afin d\'aider les restaurants à lutter contre le gaspilllage alimentaire.<br>N\'hésitez pas à nous communiquer vos retours afin que l\'on puisse continuellement améliorer notre plateforme. Si vous avez des questions, vous pouvez nous envoyer un message à cette adresse mail : <a href="mailto:welon2021@gmail.com">welon2021@gmail.com</a>.<br/>Nous restons à votre écoute et nous sommes heureux de pouvoir collaborer avec vous.<br><br><a href="https://welon.fr/api/auth/email/verification/user/' + user_id + '">Confirmez votre compte<a><br><br>La Groupe Welon<br><br>Si vous ne vous êtes pas inscrit sur la plateforme Welon, contactez nous <a href="mailto:welon2021@gmail.com">ici</a>.'
        };
        transporter.sendMail(mailOptions, function(err, info) {
            if (err)
                console.log(err);
            else
                console.log(info);
        });
    },

    sendpassword: async function(user_id, receiver) {
        var transporter = this.setTransport();
        var mailOptions = {
            from: 'welon2021@gmail.com',
            to: receiver,
            subject: 'Changez votre mot de passe',
            html: 'Cliquez sur le lien suivant pour changer votre mot de passe: <a href="">ici<a>'
        };
        transporter.sendMail(mailOptions, function(err, info) {
            if (err)
                console.log(err);
            else
                console.log(info);
        });
    }
};