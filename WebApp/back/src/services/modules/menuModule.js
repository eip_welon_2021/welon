var menuServices = require('../menuServices');
var entreeServices = require('../entreeServices');
var platServices = require('../platServices');
var dessertServices = require('../dessertServices');
var boissonServices = require('../boissonServices');

class menuModule {

    async menusToObject(menus) {
        for (var i = 0; i < menus.length; i++) {
            try {
                var name_of_entrees = await entreeServices.getNameByMenus(menus[i].id_entree);
                var name_of_plats = await platServices.getNameByMenus(menus[i].id_plat);
                var name_of_desserts = await dessertServices.getNameByMenus(menus[i].id_dessert);
                var name_of_boissons = await boissonServices.getNameByMenus(menus[i].id_boisson);
                menus[i].id_entree = name_of_entrees;
                menus[i].id_plat = name_of_plats;
                menus[i].id_dessert = name_of_desserts;
                menus[i].id_boisson = name_of_boissons;
            } catch (err) {
                throw err;
            }
        }
        return menus;
    }

    async findByRestaurant(id_resto) {
        try {
            var menus = await  menuServices.menuFindByRestaurant(id_resto);
            var newMenus = await this.menusToObject(menus);
            return newMenus;
        } catch (err) {
            throw err;
        }
    }
}

var menuModules = new menuModule();

module.exports = menuModules;