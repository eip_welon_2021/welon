// ADD PRICES TO COMMAND
var menu = require('../../models/menuModel');
var entree = require('../../models/entreeModel');
var plat = require('../../models/platModel');
var dessert = require('../../models/dessertModel');
var boisson = require('../../models/boissonModel');

async function getMenusPrice(menus) {
    return new Promise((resolve, reject) => {
        if (!menus)
            resolve(0);
        var list_of_id = []
        for (i=0; i < menus.length; i++) {
            list_of_id.push(menus[i][0])
        }
        menu.find({_id: {$in: list_of_id}})
        .then(docs => {
            var price = 0;
            for (i = 0; i < docs.length; i++) {
                price += docs[i].price * parseInt(menus[i][1]);
            }
            resolve(price);
        })
        .catch(err => {
            reject(err);
        })
    })
}

async function getEntreesPrice(entrees) {
    return new Promise((resolve, reject) => {
        if (!entrees)
            resolve(0);
        var list_of_id = []
        for (i=0; i < entrees.length; i++) {
            list_of_id.push(entrees[i][0])
        }
        entree.find({_id: {$in: list_of_id}})
        .then(docs => {
            var price = 0;
            for (i = 0; i < docs.length; i++) {
                price += docs[i].price * parseInt(entrees[i][1]);
            }
            resolve(price);
        })
        .catch(err => {
            reject(err);
        })
    })
}

async function getPlatsPrice(plats) {
    return new Promise((resolve, reject) => {
        if (!plats)
            resolve(0);
        var list_of_id = []
        for (i=0; i < plats.length; i++) {
            list_of_id.push(plats[i][0])
        }
        plat.find({_id: {$in: list_of_id}})
        .then(docs => {
            var price = 0;
            for (i = 0; i < docs.length; i++) {
                price += docs[i].price * parseInt(plats[i][1]);
            }
            resolve(price);
        })
        .catch(err => {
            reject(err);
        })
    })
}

async function getDessertsPrice(desserts) {
    return new Promise((resolve, reject) => {
        if (!desserts)
            resolve(0);
        var list_of_id = []
        for (i=0; i < desserts.length; i++) {
            list_of_id.push(desserts[i][0])
        }
        dessert.find({_id: {$in: list_of_id}})
        .then(docs => {
            var price = 0;
            for (i = 0; i < docs.length; i++) {
                price += docs[i].price * parseInt(desserts[i][1]);
            }
            resolve(price);
        })
        .catch(err => {
            reject(err);
        })
    })
}

async function getBoissonsPrice(boissons) {
    return new Promise((resolve, reject) => {
        if (!boissons)
            resolve(0);
        var list_of_id = []
        for (i=0; i < boissons.length; i++) {
            list_of_id.push(boissons[i][0])
        }
        boisson.find({_id: {$in: list_of_id}})
        .then(docs => {
            var price = 0;
            for (i = 0; i < docs.length; i++) {
                price += docs[i].price * parseInt(boissons[i][1]);
            }
            resolve(price);
        })
        .catch(err => {
            console.log(err.message)
            reject(err);
        })
    })
}

exports.getAllPrices = async (commande) => {
    try {
        var priceMenus = await getMenusPrice(commande.menus);
        var priceEntrees = await getEntreesPrice(commande.entrees);
        var pricePlats = await getPlatsPrice(commande.plats);
        var priceDesserts = await getDessertsPrice(commande.desserts);
        var priceBoissons = await getBoissonsPrice(commande.boissons);
        var total = priceMenus + priceEntrees + pricePlats + priceDesserts + priceBoissons;
        console.log(priceMenus, priceEntrees, pricePlats, priceDesserts, priceBoissons)
        return total;
    } catch (err) {
        throw "Wrong ID in your list.";
    }
}