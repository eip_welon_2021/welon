//updateAverage.js
var Menus = require('../../models/menuModel');
var Entrees = require('../../models/entreeModel');
var Plats = require('../../models/platModel');
var Desserts = require('../../models/dessertModel');
var Boissons = require('../../models/boissonModel');
var Ratings = require('../../models/ratingModel');

exports.updateAverageEntree = function (req, res, next) {
    var quality = 0;
    var quantity = 0;
    var price = 0;
    quality += parseInt(req.body.quality);
    quantity += parseInt(req.body.quantity);
    price += parseInt(req.body.price);
    Ratings.find({id_rated: req.params.id_entree})
    .then(docs => {
        for (var i = 0; i < docs.length; i++) {
            quality += docs[i].rate_quality;
            quantity += docs[i].rate_quantity;
            price += docs[i].rate_price;
        }
        var numbers = docs.length + 1;
        var average = {
            quality_average: quality / numbers,
            quantity_average: quantity / numbers,
            price_average: price / numbers,
            general_average: ((quality / numbers) + (quantity / numbers) + (price / numbers)) / 3,
            rating_numbers: numbers,
        };
        Entrees.updateOne({_id: req.params.id_entree}, average)
        .then(docs => {
            console.log(docs);
            next();
        })
        .catch(err => {
            console.log(err);
        })
    })
    .catch(err => {
        console.log(err);
    })
}

exports.updateAveragePlat = function (req, res, next) {
    var quality = 0;
    var quantity = 0;
    var price = 0;
    quality += parseInt(req.body.quality);
    quantity += parseInt(req.body.quantity);
    price += parseInt(req.body.price);
    Ratings.find({id_rated: req.params.id_plat})
    .then(docs => {
        for (var i = 0; i < docs.length; i++) {
            quality += docs[i].rate_quality;
            quantity += docs[i].rate_quantity;
            price += docs[i].rate_price;
        }
        var numbers = docs.length + 1;
        var average = {
            quality_average: quality / numbers,
            quantity_average: quantity / numbers,
            price_average: price / numbers,
            general_average: ((quality / numbers) + (quantity / numbers) + (price / numbers)) / 3,
            rating_numbers: numbers,
        };
        Plats.updateOne({_id: req.params.id_plat}, average)
        .then(docs => {
            console.log(docs);
            next();
        })
        .catch(err => {
            console.log(err);
        })
    })
    .catch(err => {
        console.log(err);
    })
}

exports.updateAverageDessert = function (req, res, next) {
    var quality = 0;
    var quantity = 0;
    var price = 0;
    quality += parseInt(req.body.quality);
    quantity += parseInt(req.body.quantity);
    price += parseInt(req.body.price);
    Ratings.find({id_rated: req.params.id_dessert})
    .then(docs => {
        for (var i = 0; i < docs.length; i++) {
            quality += docs[i].rate_quality;
            quantity += docs[i].rate_quantity;
            price += docs[i].rate_price;
        }
        var numbers = docs.length + 1;
        var average = {
            quality_average: quality / numbers,
            quantity_average: quantity / numbers,
            price_average: price / numbers,
            general_average: ((quality / numbers) + (quantity / numbers) + (price / numbers)) / 3,
            rating_numbers: numbers,
        };
        Desserts.updateOne({_id: req.params.id_dessert}, average)
        .then(docs => {
            console.log(docs);
            next();
        })
        .catch(err => {
            console.log(err);
        })
    })
    .catch(err => {
        console.log(err);
    })
}

exports.updateAverageBoisson = function (req, res, next) {
    var quality = 0;
    var quantity = 0;
    var price = 0;
    quality += parseInt(req.body.quality);
    quantity += parseInt(req.body.quantity);
    price += parseInt(req.body.price);
    Ratings.find({id_rated: req.params.id_boisson})
    .then(docs => {
        for (var i = 0; i < docs.length; i++) {
            quality += docs[i].rate_quality;
            quantity += docs[i].rate_quantity;
            price += docs[i].rate_price;
        }
        var numbers = docs.length + 1;
        var average = {
            quality_average: quality / numbers,
            quantity_average: quantity / numbers,
            price_average: price / numbers,
            general_average: ((quality / numbers) + (quantity / numbers) + (price / numbers)) / 3,
            rating_numbers: numbers,
        };
        Boissons.updateOne({_id: req.params.id_boisson}, average)
        .then(docs => {
            console.log(docs);
            next();
        })
        .catch(err => {
            console.log(err);
        })
    })
    .catch(err => {
        console.log(err);
    })
}

exports.updateAverageMenu = function (req, res, next) {
    var quality = 0;
    var quantity = 0;
    var price = 0;
    quality += parseInt(req.body.quality);
    quantity += parseInt(req.body.quantity);
    price += parseInt(req.body.price);
    Ratings.find({id_rated: req.params.id_menu})
    .then(docs => {
        for (var i = 0; i < docs.length; i++) {
            quality += docs[i].rate_quality;
            quantity += docs[i].rate_quantity;
            price += docs[i].rate_price;
        }
        var numbers = docs.length + 1;
        var average = {
            quality_average: quality / numbers,
            quantity_average: quantity / numbers,
            price_average: price / numbers,
            general_average: ((quality / numbers) + (quantity / numbers) + (price / numbers)) / 3,
            rating_numbers: numbers,
        };
        Menus.updateOne({_id: req.params.id_menu}, average)
        .then(docs => {
            console.log(docs);
            next();
        })
        .catch(err => {
            console.log(err);
        })
    })
    .catch(err => {
        console.log(err);
    })
}