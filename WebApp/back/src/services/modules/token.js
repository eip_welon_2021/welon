// /routes/auth/verifyToken.js
var jwt = require('jsonwebtoken');

exports.getIdViaToken = async (token) => {
    return new Promise((resolve, reject) => {
      jwt.verify(token, process.env.SECRET, function(err, decoded) {
        if (err)
            reject('L\'authentification du jeton à échoué.');
        resolve(decoded.id);
      });
    })
}