// /routes/auth/verifyToken.js
var jwt = require('jsonwebtoken');

function verifyToken(req, res, next) {
  var token = req.headers['x-access-token'];
  if (!token)
    return res.status(400).send({ auth: false, message: 'Aucun jeton fourni.' });
  jwt.verify(token, process.env.SECRET, function(err, decoded) {
    if (err)
        return res.status(500).send({ auth: false, message: 'L\'authentification du jeton à échoué.' });
    // if everything good, save to request for use in other routes
    req.userId = decoded.id;
    next();
  });
}

module.exports = verifyToken;