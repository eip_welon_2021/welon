var platModel = require('../models/platModel');

class PlatService {

    async platFindById(id_plat) {
        return new Promise((resolve, reject) => {
            platModel.find({_id: id_plat})
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucun plat trouvé.");
            });
        })
    }

    async platFindByRestaurant(_id_restaurant) {
        return new Promise((resolve, reject) => {
            platModel.find({id_restaurant: _id_restaurant})
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucun plat trouvé.");
            });
        })
    }

    async platCreate(newPlat) {
        return new Promise((resolve, reject) => {
            var newP = new platModel(newPlat);
            newP.save()
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("La création du plat a échoué.");
            })
        })
    }

    async platUpdate(id, newValues) {
        return new Promise((resolve, reject) => {
            platModel.updateOne({_id: id}, newValues)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucun plat trouvé avec cet id " + id + ".");
            })
        })
    }

    async platDelete(id) {
        return new Promise((resolve, reject) => {
            platModel.remove({ _id: id })
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucun plat n'a été trouvé avec cet id " + id + ".");
            });
        })
    }

    async getNameByMenus(ids_plats) {
        return new Promise((resolve, reject) => {
            platModel.find({_id: {$in: ids_plats}})
            .then(docs => {
                var names = [];
                for (var i = 0; i < docs.length; i++) {
                    names.push(docs[i].name);
                }
                resolve(names);
            })
            .catch(err => {
                resolve([]);
            })
        })
    }
}

var platService = new PlatService();

module.exports = platService;