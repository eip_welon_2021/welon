var ratingModel = require('../models/ratingModel');

class RatingService {

    // Create a rating
    async ratingCreate(newRating, res) {
        var newR = new ratingModel(newRating);
        newR.save()
        .then(docs => {
            console.log(docs);
            return res.status(200).send(docs);
        })
        .catch(err => {
            console.log(err);
            return res.status(500).send("La création de la note a échoué.");
        });
    }

    // Update a rating
    async ratingUpdate(idRated, idUser, newValues, res) {
        ratingModel.updateOne({id_rated: idRated, id_user: idUser}, newValues)
        .then(docs => {
            console.log(docs);
            return res.status(200).send(docs);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send("Aucune note trouvée avec cet id " + id + ".");
        });
    }

    // Get average rating of a meal
    async getRatingAverage(idRated, type, res) {
        var quality = 0;
        var quantity = 0;
        var price = 0;
        ratingModel.find({id_rated: idRated, type: type})
        .then(docs => {
            for (var i = 0; i < docs.length; i++) {
                quality += docs[i].rate_quality;
                quantity += docs[i].rate_quantity;
                price += docs[i].rate_price;
            }
            var average = {
                number: docs.length,
                quality: quality / docs.length,
                quantity: quantity / docs.length,
                price: price / docs.length
            };
            console.log(average);
            return res.status(200).send(average);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send("Aucune note trouvée avec cet id " + id + ".");
        });
    }

    // Get rating history of a user
    async getHistory(idUser, res) {
        ratingModel.find({id_user: idUser})
        .then(docs => {
            return res.status(200).send(docs);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send("Aucun utilisateur trouvé.");
        })
    }

    // Delete Rating
    async deleteRating(idRated, res) {
        ratingModel.remove({_id: idRated})
        .then(docs => {
            return res.status(200).send(docs);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send("Aucun retour trouvé.");
        })
    }

    // Get top 5 resstaurants
    async getTopFive(res) {
        var rest = {};
        ratingModel.find()
        .then(docs => {
            for (var i = 0; i < docs.length; i++) {
                if (rest.hasOwnProperty(docs[i].id_restaurant)) {
                    rest[docs[i].id_restaurant]['rating'] += docs[i].rate_quantity + docs[i].rate_price + docs[i].rate_quality;
                    rest[docs[i].id_restaurant]['nb'] += 3;
                } else {
                    rest[docs[i].id_restaurant] = {'rating': 0, 'nb': 3};
                    rest[docs[i].id_restaurant]['rating'] = docs[i].rate_quantity + docs[i].rate_quality + docs[i].rate_price;
                }
            }
            var top = [];
            var rate = {};
            for (var r in rest) {
                if (rest.hasOwnProperty(r)) {
                    rate = rest[r]['rating'] / rest[r]['nb']
                    if (top.length < 5) {
                        top.push({"restaurant": r, "rate": rate})
                    } else {
                        var min = 10;
                        var idx = -1;
                        for (var i = 0; i < top.length; i++) {
                            if (top[i]['rate'] < min) {
                                min = top[i]['rate'];
                                idx = i;
                            }
                        }
                        if (rate > min) {
                            top[idx]["restaurant"] = r;
                            top[i]["rate"] = rate
                        }
                    }
                }
            }
            return res.status(200).send(top);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send("Error");
        });
    }

    // Get top 5 dishes for a restaurant
    async getTopFivePlats(req, res, next) {
        var rest = {};
        ratingModel.find({id_restaurant: req.userId})
        .then(docs => {
            for (var i = 0; i < docs.length; i++) {
                if (rest.hasOwnProperty(docs[i]._id)) {
                    rest[docs[i]._id]['rating'] += docs[i].rate_quantity + docs[i].rate_price + docs[i].rate_quality;
                    rest[docs[i]._id]['nb'] += 3;
                } else {
                    rest[docs[i]._id] = {'rating': 0, 'nb': 3};
                    rest[docs[i]._id]['rating'] = docs[i].rate_quantity + docs[i].rate_quality + docs[i].rate_price;
                }
            }
            var top = [];
            var rate = {};
            for (var r in rest) {
                if (rest.hasOwnProperty(r)) {
                    rate = rest[r]['rating'] / rest[r]['nb']
                    if (top.length < 5) {
                        top.push({"plat": r, "rate": rate})
                    } else {
                        var min = 10;
                        var idx = -1;
                        for (var i = 0; i < top.length; i++) {
                            if (top[i]['rate'] < min) {
                                min = top[i]['rate'];
                                idx = i;
                            }
                        }
                        if (rate > min) {
                            top[idx]["plat"] = r;
                            top[idx]["rate"] = rate
                        }
                    }
                }
            }
            return res.status(200).send(top);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send("Error");
        });
    }

    // Get the rank of the given rstaurant
    async getRankRest(id_restaurant, res) {
        var rest = {};
        ratingModel.find()
        .then(docs => {
            for (var i = 0; i < docs.length; i++) {
                if (rest.hasOwnProperty(docs[i].id_restaurant)) {
                    rest[docs[i].id_restaurant]['rating'] += docs[i].rate_quantity + docs[i].rate_price + docs[i].rate_quality;
                    rest[docs[i].id_restaurant]['nb'] += 3;
                } else {
                    rest[docs[i].id_restaurant] = {'rating': 0, 'nb': 3};
                    rest[docs[i].id_restaurant]['rating'] = docs[i].rate_quantity + docs[i].rate_quality + docs[i].rate_price;
                }
            }
            var list = [];
            var rate = {};
            for (var r in rest) {
                if (rest.hasOwnProperty(r)) {
                    rate = rest[r]['rating'] / rest[r]['nb']
                    list.push({restaurant: r, rate: rate})
                }
            }
            list.sort((a, b) => (a.rate > b.rate) ? 1 : -1)
            var index = list.findIndex(x => x.restaurant === id_restaurant)
            if (index === -1) {
                return res.status(400).send({message: "This restaurant has no ratings."});
            } else {
                return res.status(200).send({rank: list.length - index - 1, nbRestaurant: list.length});
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send("Error");
        });
    }
    
}

var ratingService = new RatingService();

module.exports = ratingService;