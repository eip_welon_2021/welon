var restaurantModel = require('../models/restaurantModel');
var ratingModel = require('../models/ratingModel');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var email = require('./modules/mailerModule');
var fs = require('fs');

class RestaurantService {

    async restaurantLogin(_email, _password, res) {
        restaurantModel.findOne({ email: _email }, function (err, restaurant) {
            if (err)
                return res.status(500).send('Erreur ineterne du serveur.');
            if (!restaurant)
                return res.status(404).send('Aucun restaurant trouvé.');
            if (!restaurant.verified)
                return res.status(401).send('Votre email n\'est pas vérifié.');
            var passwordIsValid = bcrypt.compareSync(_password, restaurant.password);
            if (!passwordIsValid)
                return res.status(400).send("Mot de passe incorrect.");
            var token = jwt.sign({ id: restaurant._id }, process.env.SECRET);
            return res.status(200).send({ auth: true, token: token });
          });
    }

    async restaurantRegister(data, res) {
        restaurantModel.create({
            email : data.email,
            password : data.password,
            name: data.name,
            address: data.address,
            phone: data.phone,
            longitude: data.longitude,
            latitude: data.latitude
        },
        function (err, restaurant) {
            if (err) {
                return res.status(400).send("Cet email existe déjà.")
            }
            // create a token
            var token = jwt.sign({ id: restaurant._id }, process.env.SECRET);
            //send email confirmation
            email.sendWelcomeRestaurant(restaurant._id, restaurant.email, token);
            return res.status(200).send({ auth: true, token: token });
        });
    }


    // TODO
    async restaurantFindAll(res) {
        this.getRestaurantsRating()
        .then(ladder => {
            restaurantModel.find()
            .exec()
            .then(docs => {
                var restau = [];
                for (var doc in docs) {
                    docs[doc].rank = ladder[docs[doc]._id] - 1;
                }
                return res.status(200).send(docs);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send("Aucun restaurant trouvé.");
            });
        })
        .catch(err => {
            return res.status(400).send(err);
        });
    }

    async getRestaurantsRating() {
        return new Promise((resolve, reject) => {
            var rest = {};
            ratingModel.find()
            .then(docs => {
                for (var i = 0; i < docs.length; i++) {
                    if (rest.hasOwnProperty(docs[i].id_restaurant)) {
                        rest[docs[i].id_restaurant]['rating'] += docs[i].rate_quantity + docs[i].rate_price + docs[i].rate_quality;
                        rest[docs[i].id_restaurant]['nb'] += 3;
                    } else {
                        rest[docs[i].id_restaurant] = {'rating': 0, 'nb': 3};
                        rest[docs[i].id_restaurant]['rating'] = docs[i].rate_quantity + docs[i].rate_quality + docs[i].rate_price;
                    }
                }
                var list = [];
                var rate = {};
                for (var r in rest) {
                    if (rest.hasOwnProperty(r)) {
                        rate = rest[r]['rating'] / rest[r]['nb']
                        list.push({restaurant: r, rate: rate})
                    }
                }
                list.sort((a, b) => (a.rate > b.rate) ? -1 : 1)
                var ladder = {}
                for (var i = 0; i < list.length; i++) {
                    ladder[list[i].restaurant] = i + 1
                }
                resolve(ladder);
            })
            .catch(err => {
                console.log(err);
                reject(err);
            })
        })
    }

    async restaurantRemoveOne(_email, res) {
        const restaurantDeleted = await restaurantModel.findOneAndRemove({ email: _email })
        if (!restaurantDeleted) {
            return res.status(400).send("Aucun restaurant trouvé.");
        }
        return res.status(200).json({email: _email, message: 'restaurant supprimé'});
    }

    async findRestaurantById(id_restaurant) {
        return new Promise((resolve, reject) => {
            restaurantModel.findById(id_restaurant, {password: 0}, function (err, restaurant) {
                if (err)
                    reject({message: "Un problème est survenu lors de la recherche d'un restaurant."});
                if (!restaurant)
                    resolve({restaurant: undefined});
                resolve({restaurant: restaurant});
            });
        })
    }

    async getPhoto(id_restaurant) {
        return new Promise((resolve, reject) => {
            restaurantModel.find({_id: id_restaurant})
            .then(docs => {
                resolve(docs[0].image);
            })
            .catch(err => {
                reject(err);
            })
        })
    }

    async uploadPhoto(imagePath, contentType) {
        return new Promise((resolve, reject) => {
            console.log(imagePath)
            fs.readFileSync(imagePath)
            .catch(err => {
                console.log(err)
            })
            console.log("zer")
            var image = {
                data: fs.readFileSync(imagePath),
                contentType: contentType
            }
            console.log(image);
            restaurantModel.updateOne({_id: id_restaurant}, {img: image})
            .then(docs => {
                resolve({message: "Done", docs});
            })
            .catch(err => {
                reject(err);
            })
        })
    }

    async updateRestaurant(id, newValues) {
        return new Promise((resolve, reject) => {
            restaurantModel.updateOne({_id: id}, newValues)
            .then(docs => {
                resolve(docs);
            })
            .catch(err => {
                reject("Aucun restaurant trouvée avec cet id " + id + ".");
            })
        })
    }
}

var restaurantService = new RestaurantService();

module.exports = restaurantService;