var userModel = require('../models/userModel');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var email = require('./modules/mailerModule');

class UserService {

    async userLogin(_email, _password, res) {
        userModel.findOne({ email: _email }, function (err, user) {
            if (err)
              return res.status(500).send('Erreur ineterne du serveur.');
            if (!user)
              return res.status(400).send('Aucun utilisateur trouvé.');
            if (!user.verified)
              return res.status(401).send('Votre email n\'est pas vérifié.');
            var passwordIsValid = bcrypt.compareSync(_password, user.password);
            if (!passwordIsValid)
              return res.status(400).send("Mot de passe incorrect.");
            var token = jwt.sign({ id: user._id }, process.env.SECRET);
            return res.status(200).send({ auth: true, token: token });
          });
    }

    async userRegister(data, res) {
        userModel.create({
            email : data.email,
            password : data.password,
            firstname: data.firstname,
            lastname: data.lastname
        },
        function (err, user) {
            if (err) {
                return res.status(400).send("Cet email existe déjà.")
            }
            // create a token
            var token = jwt.sign({ id: user._id },  process.env.SECRET);
            email.sendWelcomeUser(user._id, user.email);
            return res.status(200).send({ auth: true, token: token });
        }); 
    }

    async userFindAll(res) {
        userModel.find()
        .exec()
        .then(docs => {
            console.log(docs);
            return res.status(200).send(docs);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send("Aucun n'utilisateur trouvé.");
        });
    }

    async userRemoveOne(_email, res) {
        const userDeleted = await userModel.findOneAndRemove({ email: _email });
        if (!userDeleted) {
            return res.status(400).send("Aucun n'utilisateur trouvé.");
        }
        return res.status(200).json({email: _email, message: 'utilisateur supprimé'});
    }

    async findUserById(id_user) {
        return new Promise((resolve, reject) => {
            userModel.findById(id_user, { password: 0 }, function (err, user) {
                if (err)
                    reject({message: "Un problème est survenu lors de la recherche d'utilisateur."});
                if (!user) {
                    resolve({user: undefined});
                }
                else
                    resolve({user: user});
            });
        })
    }
}

var userService = new UserService();

module.exports = userService;