//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
var expect  = require('chai').expect;
var request = require('request');

/*
* Test the /GET route me
*/
// describe ('/GET me', function() {
//     it('it should not GET a not connected user', function(done){
//         request('http://137.116.226.116/api/auth/me', function(error, response, body) {
//             expect(response.statusCode).to.equal(400);
//             done();
//         });
//     });
// });

/*
* Test the /GET route logout
*/
// describe ('/GET logout', function() {
//     it('it should not disconnect if no user is connected', function(done){
//         request('http://137.116.226.116/api/auth/logout', function(error, response, body) {
//             expect(response.statusCode).to.equal(400);
//             done();
//         });
//     });
// });

/*
* Test the /POST route login
*/
// describe('/POST login', () => {
//     it('it should not logged in a wrong user', (done) => {
//         let body = {
//             email: "zozo",
//             password: "zozo",
//         }
//         request.post({url: "http://137.116.226.116/api/auth/login/restaurant", formData: body}, function(error, response, body) {
//             expect(response.statusCode).to.equal(400);
//             done();
//         });
//     });
// });

// /*
// * Test the /POST route register
// */
// describe('/POST register', () => {
//     it('it should not register an invalid email', (done) => {
//         let body = {
//             email: "zozo",
//             password: "zozo",
//             name: "zozo",
//             address: "zozo",
//             phone: "zozo",
//             img: "zozo"
//         }
//         request.post({url: "http://137.116.226.116/api/auth/register/restaurant", formData: body}, function(error, response, body) {
//             expect(response.statusCode).to.equal(400);
//             done();
//         });
//     });
// });

// /*
// * Test the /POST route register
// */
// describe('/POST register', () => {
//     it('it should not register an invalid password', (done) => {
//         let body = {
//             email: "zozo@gmail.com",
//             password: "zozo",
//             name: "zozo",
//             address: "zozo",
//             phone: "zozo",
//             img: "zozo"
//         }
//         request.post({url: "http://137.116.226.116/api/auth/register/restaurant", formData: body}, function(error, response, body) {
//             expect(response.statusCode).to.equal(400);
//             done();
//         });
//     });
// });