//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
var expect  = require('chai').expect;
var request = require('request');

/*
* Test the /GET drinks
*/
describe ('/GET all drinks of a restaurant', function() {
    it('it should GET all drinks', function(done){
        request('http://137.116.226.116/api/boissons', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});

/*
* Test the /GET drinks local
*/
describe ('/GET all drinks of a restaurant local', function() {
    it('it should GET all drinks', function(done){
        request('http://localhost:8080/api/boissons', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});