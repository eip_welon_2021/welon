//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
var expect  = require('chai').expect;
var request = require('request');

/*
* Test the /GET route /command/user
*/
describe ('/GET USER COMMAND', function() {
    it('it should not GET a not connected user', function(done){
        request('http://137.116.226.116/api/command/user', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});

/*
* Test the /GET route /command/user
*/
describe ('/GET USER COMMAND local', function() {
    it('it should not GET a not connected user', function(done){
        request('http://localhost:8080/api/command/user', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});

/*
* Test the /GET route /command/restaurant
*/
describe ('/GET RESTAURANT COMMAND', function() {
    it('it should not GET a not connected restaurant', function(done){
        request('http://137.116.226.116/api/command/restaurant', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});

/*
* Test the /GET route /command/restaurant
*/
describe ('/GET RESTAURANT COMMAND local', function() {
    it('it should not GET a not connected restaurant', function(done){
        request('http://localhost:8080/api/command/restaurant', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});

/*
* Test the /GET route /command/qrcode/:id
*/
describe ('/GET COMMAND QRCODE', function() {
    it('it should not GET a qrcode for a none existing command', function(done){
        request('http://137.116.226.116/api/command/qrcode/azerty', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});

/*
* Test the /GET route /command/qrcode/:id
*/
describe ('/GET COMMAND QRCODE local', function() {
    it('it should not GET a qrcode for a none existing command', function(done){
        request('http://localhost:8080/api/command/qrcode/azerty', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});

/*
* Test the /POST route /commands
*/
describe('/POST CREATE A COMMAND', () => {
    it('it should not create a command with wrong parameters', (done) => {
        let body = {
            id_restaurant: 'zerrret',
            menus: '[[]]',
            entrees: '[[]]',
            plats: '[[]]',
            desserts: '[[]]',
            boissons: '[[]]',
            price: 'total',
            created_at: Date.now(),
        }
        request.post({url: "http://137.116.226.116/api/auth/login/restaurant", formData: body}, function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});