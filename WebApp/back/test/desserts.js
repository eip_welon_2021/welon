//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
var expect  = require('chai').expect;
var request = require('request');

/*
* Test the /GET desserts
*/
describe ('/GET all desserts of a restaurant', function() {
    it('it should GET all desserts', function(done){
        request('http://137.116.226.116/api/desserts', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});

describe ('/GET all desserts of a restaurant local', function() {
    it('it should GET all desserts', function(done){
        request('http://localhost:8080/api/desserts', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});