//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
var expect  = require('chai').expect;
var request = require('request');

/*
* Test the /GET menus
*/
describe ('/GET all menus', function() {
    it('it should GET all menus', function(done){
        request('http://137.116.226.116/api/menus', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});

describe ('/GET all menus local', function() {
    it('it should GET all menus', function(done){
        request('http://localhost:8080/api/menus', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});

/*
* Test the /GET menus
*/
describe ('/GET menus of a restaurant', function() {
    it('it should GET all menus of a restaurant', function(done){
        request('http://137.116.226.116/api/menus/restaurant', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});

describe ('/GET menus of a restaurant local', function() {
    it('it should GET all menus of a restaurant', function(done){
        request('http://localhost:8080/api/menus/restaurant', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});