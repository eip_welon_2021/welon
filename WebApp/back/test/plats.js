//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
var expect  = require('chai').expect;
var request = require('request');

/*
* Test the /GET plates
*/
describe ('/GET all plates of a restaurant', function() {
    it('it should GET all plates', function(done){
        request('http://137.116.226.116/api/plats', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});

describe ('/GET all plates of a restaurant local', function() {
    it('it should GET all plates', function(done){
        request('http://localhost:8080/api/plats', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});