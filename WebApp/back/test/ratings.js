//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
var expect  = require('chai').expect;
var request = require('request');

/*
* Test the /GET Rating General
*/
describe ('/GET Rating General', function() {
    it('it should GET general ratings for a restaurant', function(done){
        request('http://137.116.226.116/api/dashboard/rating/general', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});

describe ('/GET Rating General local', function() {
    it('it should GET general ratings for a restaurant', function(done){
        request('http://localhost:8080/api/dashboard/rating/general', function(error, response, body) {
            expect(response.statusCode).to.equal(400);
            done();
        });
    });
});

/*
* Test the /GET Rating Best
*/
// describe ('/GET Rating Best', function() {
//     it('it should GET the best rated plates for a restaurant', function(done){
//         request('http://137.116.226.116/api/dashboard/rating/best', function(error, response, body) {
//             expect(response.statusCode).to.equal(400);
//             done();
//         });
//     });
// });

/*
* Test the /GET Rating Worst
*/
// describe ('/GET Rating Worst', function() {
//     it('it should GET worst rated plates for a restaurant', function(done){
//         request('http://137.116.226.116/api/dashboard/rating/worst', function(error, response, body) {
//             expect(response.statusCode).to.equal(400);
//             done();
//         });
//     });
// });

/*
* Test the /GET Most rated
*/
// describe ('/GET Most Rated', function() {
//     it('it should GET most rated plates for a restaurant', function(done){
//         request('http://137.116.226.116/api/dashboard/rating/most/rated', function(error, response, body) {
//             expect(response.statusCode).to.equal(400);
//             done();
//         });
//     });
// });

/*
* Test the /GET Less Rated
*/
// describe ('/GET Less Rated', function() {
//     it('it should GET less rated plates for a restaurant', function(done){
//         request('http://137.116.226.116/api/dashboard/rating/less/rated', function(error, response, body) {
//             expect(response.statusCode).to.equal(400);
//             done();
//         });
//     });
// });