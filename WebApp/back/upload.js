const express = require('express');
const router = express.Router();
const fs = require('fs');
const AWS = require('aws-sdk');

const s3 = new AWS.S3({
  accessKeyId: process.env.access_key,
  secretAccessKey: process.env.secret_key
});

const uploadFile = async (fileLocation, fileName, fileType) => {
    return new Promise((resolve, reject) => {
        fs.readFile(fileLocation, (err, data) => {
            if (err)
                reject(err);
             const params = {
                Bucket: 'welonimages', // pass your bucket name
                Key: fileName, // file to be stored
                Body: data,
                ContentType: fileType,
                ACL: "public-read"
            };
            s3.upload(params, function(s3Err, data) {
                if (s3Err)
                    reject(s3Err);
                resolve(data.Location);
            });
         });
     });
};

const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'assets/uploads/')
    },
    filename: function (req, file, cb) {
        if (file.mimetype === 'image/jpeg')
            cb(null, file.fieldname + '-' + Date.now() + '.jpeg')
        else if (file.mimetype === 'image/jpg')
            cb(null, file.fieldname + '-' + Date.now() + '.jpg')
        else if (file.mimetype === 'image/png')
            cb(null, file.fieldname + '-' + Date.now() + '.png')
        else
            cb('Le fichier doit être un .png, .jpg ou .jpeg.')
    }
})
const upload = multer({ storage: storage, limits: { fileSize: 2 * 1024 * 1024 }});

const verifyToken = require('./src/services/modules/verifyToken');
const Restaurant = require('./src/models/restaurantModel');

router.post('/upload/images', verifyToken, upload.single('image'), async (req, res) => {
    var fileName = req.userId;
    if (req.file.mimetype === 'image/png')
        fileName += '.png';
    if (req.file.mimetype === 'image/jpg')
        fileName += '.jpg';
    if (req.file.mimetype === 'image/jpeg')
        fileName += '.jpeg';
    console.log(req.file.mimetype)
    await uploadFile(req.file.path, fileName, req.file.mimetype)
    .then(photo => {
        fs.unlink(req.file.path, (err) => {
            if (err)
                throw err;
        })
        Restaurant.updateOne({_id: req.userId}, {img: photo})
        .then(doc => {
            res.status(200).send(doc);
        })
    })
    .catch(err => {
        res.status(400).send(err);
    })    
});

router.get('/images/:id', async (req, res) => {
    Restaurant.findOne({_id: req.params.id}, { img: 1 })
    .then(doc => {
        res.status(200).send(doc);
    })
    .catch(err => {
        res.status(400).send(err);
    })
});

module.exports = router;