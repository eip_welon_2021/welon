import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardLayoutComponent } from './layouts/dashboard-layout/dashboard-layout.component';
import { EmptyLayoutComponent } from './layouts/empty-layout/empty-layout.component';
import { LoginLayoutComponent } from './layouts/login-layout/login-layout.component';

import { AuthGuard } from './auth/auth.guard';

import { LoginPageComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { VerifyPageComponent } from './pages/verify/verify.component';
import { VerifiedAccountPageComponent } from './pages/account-verified/account-verified.component';

import { MenusComponent } from './pages/menus/menus.component';
import { TutorialsComponent } from './pages/tutorials/tutorials.component';
import { TutorialAndroidComponent } from './pages/tutorial-android/tutorial-android.component';
import { TutorialWebComponent } from './pages/tutorial-web/tutorial-web.component';
import { TutorialIosComponent } from './pages/tutorial-ios/tutorial-ios.component';
import { AssetLinksComponent } from './pages/asset-links/asset-links.component';
import { OrderComponent } from './pages/order/order.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { DeleteAccountComponent } from './pages/delete-account/delete-account.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';

import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { HistoryComponent } from './pages/history/history.component';

const routes: Routes = [
  {
    path: '',
    component: EmptyLayoutComponent,
    children: [
      { path: '', pathMatch: 'full', component: LandingPageComponent },
      { path: 'tutorials', component: TutorialsComponent },
      { path: 'tutorials/android', component: TutorialAndroidComponent },
      { path: 'tutorials/ios', component: TutorialIosComponent },
      { path: 'tutorials/web', component: TutorialWebComponent },
      { path: '.well-known/assetlinks.json', component: AssetLinksComponent }
    ]
  },
  {
    path: '',
    component: DashboardLayoutComponent,
    canActivate: [AuthGuard],
    // user cannot access these pages unless it is register
    // to add new routes its in the CHILDREN element
    children: [
        { path: 'dashboard', component: DashboardComponent },
        { path: 'menus', component: MenusComponent },
        { path: 'order', component: OrderComponent },
        { path: 'profile', component: ProfileComponent },
        { path: 'history', component: HistoryComponent }
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'change/password', component: ChangePasswordComponent },
      { path: 'delete/account', component: DeleteAccountComponent }
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      { path: 'login', component: LoginPageComponent },
      { path: 'verify', component: VerifyPageComponent },
      { path: 'verified', component: VerifiedAccountPageComponent }
    ]
  },
  { path: '**', pathMatch: 'full', component: PageNotFoundComponent }
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
