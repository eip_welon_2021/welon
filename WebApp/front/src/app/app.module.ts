// Routing and Basic Angular Import
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// MDB Bootstrap & Material Import
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatCardModule,
        MatTableModule,
        MatCheckboxModule,
        MatDialogModule } from '@angular/material';
import { ChartsModule, WavesModule } from 'angular-bootstrap-md';
import { MatInputModule } from '@angular/material/input';

// Miscellaneous Imports
import { CookieService } from 'ngx-cookie-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Auth Service Import
import { AuthGuard } from './auth/auth.guard';
import { AuthService } from './auth/auth.service';

// Layout Import
import { EmptyLayoutComponent } from './layouts/empty-layout/empty-layout.component';
import { DashboardLayoutComponent } from './layouts/dashboard-layout/dashboard-layout.component';
import { LoginLayoutComponent } from './layouts/login-layout/login-layout.component';

// Login Page Import
import { LoginPageComponent } from './pages/login/login.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

import { ChatbotComponent } from './components/chatbot/chatbot.component';

// Dashboard Import
import { TopBarDashboardComponent } from './components/navbars/topbar-dashboard/topbar-dashboard.component';

// Pages
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { VerifyPageComponent } from './pages/verify/verify.component';
import { VerifiedAccountPageComponent } from './pages/account-verified/account-verified.component';
import { MenusComponent } from './pages/menus/menus.component';
import { TutorialsComponent } from './pages/tutorials/tutorials.component';
import { TutorialAndroidComponent } from './pages/tutorial-android/tutorial-android.component';
import { AssetLinksComponent } from './pages/asset-links/asset-links.component';
import { OrderComponent } from './pages/order/order.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { DeleteAccountComponent } from './pages/delete-account/delete-account.component';

// Components
import { BarChartComponent } from './components/charts/bar-chart/bar-chart.component';
import { LineChartComponent } from './components/charts/line-chart/line-chart.component';
import { PieChartComponent } from './components/charts/pie-chart/pie-chart.component';
import { DoughnutChartComponent } from './components/charts/doughnut-chart/doughnut-chart.component';

import { OrdertableComponent } from './components/order/ordertable/ordertable.component';
import { OrderrowComponent } from './components/order/orderrow/orderrow.component';
import { OrderModalComponent } from './components/modals/order-modal/order-modal.component';

import { ErrorModalComponent } from './components/modals/error-modal/error-modal.component';
import { UploadModalComponent } from './components/modals/upload-modal/upload-modal.component';
import { SuccessModalComponent } from './components/modals/success-modal/success-modal.component';

import { ProfileImageComponent } from './components/profile-image/profile-image.component';
import { ProfileComponent } from './pages/profile/profile.component';

import { MapsComponent } from './components/maps/maps.component';
import { PasswordInputComponent } from './components/password-input/password-input.component';
import { TutorialIosComponent } from './pages/tutorial-ios/tutorial-ios.component';

import { MapsService } from './services/maps/maps.service';
import { WBtnComponent } from './components/w-btn/w-btn.component';
import { WBtnIconComponent } from './components/w-btn-icon/w-btn-icon.component';
import { WCardComponent } from './components/w-card/w-card.component';

import { NgcCookieConsentModule, NgcCookieConsentConfig } from 'ngx-cookieconsent';
import { environment } from './../environments/environment';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { DescriptionComponent } from './components/landing/description/description.component';
import { DevsComponent } from './components/landing/devs/devs.component';
import { ContactComponent } from './components/landing/contact/contact.component';
import { HistoryComponent } from './pages/history/history.component';
import { DelicacyComponent } from './components/delicacy/delicacy.component';
import { MenuFoodComponent } from './components/menu-food/menu-food.component';
import { MenuFoodModalComponent } from './components/modals/menu-food-modal/menu-food-modal.component';
import { DelicacyModalComponent } from './components/modals/delicacy-modal/delicacy-modal.component';
import { TutorialWebComponent } from './pages/tutorial-web/tutorial-web.component';

// Cookie consent configuration
const cookieConfig: NgcCookieConsentConfig = {
    cookie: {
      domain: environment.cookieDomain
    },
    position: 'bottom-right',
    theme: 'classic',
    palette: {
      popup: {
        background: '#00c851',
        text: '#ffffff',
        link: '#ffffff'
      },
      button: {
        background: '#ffffff',
        text: '#000000',
        border: 'transparent'
      }
    },
    type: 'info',
    content: {
      message: 'Ce site web utilise des cookies pour vous assurer la meilleure expérience de navigation sur notre site.',
      dismiss: 'OK, j\'ai compris!',
      deny: 'Refuser',
      link: 'Plus d\'information',
      href: '',
      header: 'Cookies sur le site !',
      allow: 'Autoriser les cookies'
    }
  };

@NgModule({
  declarations: [
    AppComponent,
    EmptyLayoutComponent,
    LoginPageComponent,
    LoginLayoutComponent,
    DashboardLayoutComponent,
    LoginComponent,
    RegisterComponent,
    ChatbotComponent,
    TopBarDashboardComponent,
    DashboardComponent,
    MenusComponent,
    ErrorModalComponent,
    BarChartComponent,
    LineChartComponent,
    PieChartComponent,
    DoughnutChartComponent,
    TutorialsComponent,
    TutorialAndroidComponent,
    AssetLinksComponent,
    OrderComponent,
    OrdertableComponent,
    OrderrowComponent,
    OrderModalComponent,
    UploadModalComponent,
    ProfileImageComponent,
    ProfileComponent,
    MapsComponent,
    VerifiedAccountPageComponent,
    VerifyPageComponent,
    PasswordInputComponent,
    ChangePasswordComponent,
    SuccessModalComponent,
    TutorialIosComponent,
    WBtnComponent,
    WBtnIconComponent,
    WCardComponent,
    DeleteAccountComponent,
    PageNotFoundComponent,
    LandingPageComponent,
    DescriptionComponent,
    DevsComponent,
    ContactComponent,
    HistoryComponent,
    DelicacyComponent,
    MenuFoodComponent,
    MenuFoodModalComponent,
    DelicacyModalComponent,
    TutorialWebComponent
  ],
  imports: [
    NgcCookieConsentModule.forRoot(cookieConfig),
    MDBBootstrapModule.forRoot(),
    MatInputModule,
    MatTableModule,
    MatCheckboxModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatDialogModule
  ],
  exports: [
    MatSidenavModule,
    ErrorModalComponent,
    SuccessModalComponent,
    UploadModalComponent,
  ],
  providers: [
    MapsService,
    AuthGuard,
    AuthService,
    CookieService
   ],
  bootstrap: [ AppComponent ],
  entryComponents: [
    ErrorModalComponent,
    OrderModalComponent,
    SuccessModalComponent,
    UploadModalComponent,
    DelicacyModalComponent,
    MenuFoodModalComponent
  ],
})
export class AppModule { }
