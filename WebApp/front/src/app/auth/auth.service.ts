import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { ProfileType } from '../types/profile';
import { UserType } from '../types/user';
import { Cookie } from 'src/app/services/cookie/cookie.service';
import { RequestService } from '../services/request/request.service';

@Injectable()
export class AuthService {

    private loggedIn = new BehaviorSubject<boolean>(false); // {1}
    private ip: string;

    get isLoggedIn() {
        this.loggedIn.next(
            (this.cookieService.get('loggedIn') === 'true') ? true : false
        );
        return this.loggedIn.asObservable(); // {2}
    }

    constructor(private router: Router,
        private cookieService: Cookie,
        private requestService: RequestService) {
    }

    logme (user: string, token: string) {
        this.cookieService.set('accessToken', token);
        this.cookieService.set('user', user);
        this.checkVerify()
        .subscribe(
            (res: ProfileType) => {
                if (res.verified === false) {
                    this.router.navigate(['/verify']);
                } else {
                    this.router.navigate(['/dashboard']);
                }
            }
        );
    }

    checkVerify() {
        return new Observable<ProfileType>(observer => {
            this.requestService.get('auth/me')
            .subscribe((res: ProfileType) => {
                if (res.verified) {
                    this.cookieService.set('loggedIn', 'true');
                    this.loggedIn.next(true);
                    this.cookieService.set('id', res._id);
                }
                observer.next(res);
                observer.complete();
            },
            (err) => observer.error(err));
        });
    }

    login(user: UserType) {
        if (user.email !== '' && user.password !== '' ) {  // {3}
            return this.requestService.postNoControl(
                'auth/login/restaurant', {
                    email: user.email,
                    password: user.password
                });
        }
    }

    register(user: UserType) {
        return (this.requestService.postNoControl('auth/register/restaurant', {
            email: user.email,
            password: user.password,
            name: user.name,
            address: user.address,
            longitude: user.longitude,
            latitude: user.latitude,
            phone: user.phone
        }));
  }

    logout() {
        this.requestService.get('auth/logout', 'text/plain')
        .subscribe(
            () => {
                this.cookieService.deleteAll();
                this.loggedIn.next(false);
                this.router.navigate(['/login']);
            }
        );
    }

    // Logout the used without Redirect on login
    logoutAsset() {
        this.cookieService.deleteAll();
        this.loggedIn.next(false);
    }
}
