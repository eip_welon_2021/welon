import { Component, OnInit } from '@angular/core';
import { BarChartService } from 'src/app/services/charts/bar-chart/bar-chart.service';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {

  constructor(private barChartService: BarChartService) {}

  public chartType = 'bar';

  public chartDatasets: Array<any> ;

  public chartLabels: Array<any> ;

  public chartColors: Array<any> = [
    {
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };

  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.barChartService.getData()
    .subscribe(
      (res) => {
        this.chartDatasets =  [
          { data: [res['best_boisson'][0].general_average,
          res['best_plat'][0].general_average,
          res['best_entree'][0].general_average,
          res['best_dessert'][0].general_average],
          label: 'Meilleurs mets' }
        ];

        this.chartLabels = [res['best_boisson'][0].name,
        res['best_plat'][0].name,
        res['best_entree'][0].name,
        res['best_dessert'][0].name];

      },
      (err) => {
        console.log(err);
      }
    );
  }

}
