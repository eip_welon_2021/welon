import { Component, OnInit } from '@angular/core';
import { DoughnutChartService } from 'src/app/services/charts/doughnut-chart/doughnut-chart.service';

@Component({
  selector: 'app-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.scss']
})
export class DoughnutChartComponent implements OnInit {

  constructor( private doughnutService: DoughnutChartService) { }

  public chartType = 'doughnut';

  public chartDatasets: Array<any>;

  public chartLabels: Array<any>;

  public chartColors: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
      hoverBackgroundColor: ['#FF5A5E', '#5AD3D1', '#FFC870', '#A8B3C5', '#616774'],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };

  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.doughnutService.getData()
    .subscribe(
      (res) => {
        this.chartDatasets = [{
          data: [
              res['most_boisson'][0].rating_numbers,
              res['most_plat'][0].rating_numbers,
              res['most_dessert'][0].rating_numbers,
              res['most_entree'][0].rating_numbers],
          label: 'My First dataset'
      }];
        this.chartLabels = [
            res['most_boisson'][0].name,
            res['most_plat'][0].name,
            res['most_dessert'][0].name,
            res['most_entree'][0].name];
      },
      (err) => {
        console.log(err);
      }
    );
  }

}
