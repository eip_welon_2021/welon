import { Component, OnInit } from '@angular/core';
import { LineChartService } from 'src/app/services/charts/line-chart/line-chart.service';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {

  constructor(public lineChart: LineChartService) { }

  public chartType = 'line';

  public chartDatasets: Array<any>;

  public chartLabels: Array<any>;

  public chartColors: Array<any> = [
    {
      backgroundColor: 'rgba(105, 0, 132, .2)',
      borderColor: 'rgba(200, 99, 132, .7)',
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };

  ngOnInit() {
    this.lineChart.getData().subscribe(data => {
        this.chartDatasets = [{data: data.listOfDays.reverse(), label: 'Chiffre d\'affaire'}];
        this.chartLabels = Array.from({length: data.listOfDays.length}, (_, i) => -i).reverse();
    });
  }

  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }
}
