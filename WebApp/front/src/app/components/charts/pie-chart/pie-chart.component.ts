import { Component, OnInit } from '@angular/core';
import { PieChartService } from 'src/app/services/charts/pie-chart/pie-chart.service';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit {

  constructor(private pieService: PieChartService) { }

  public chartType = 'pie';

  public chartDatasets: Array<any> = [
    { data: [300, 50, 100, 40, 120], label: 'My First dataset' }
  ];

  public chartLabels: Array<any> = ['Red', 'Green', 'Yellow', 'Grey', 'Dark Grey'];

  public chartColors: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
      hoverBackgroundColor: ['#FF5A5E', '#5AD3D1', '#FFC870', '#A8B3C5', '#616774'],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };

  ngOnInit() {
    this.getData();
  }
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

  getData() {
    this.pieService.getData()
    .subscribe(
      (res) => {
        this.chartDatasets = [{
          data: [
              res['less_boisson'][0].rating_numbers,
              res['less_plat'][0].rating_numbers,
              res['less_dessert'][0].rating_numbers,
              res['less_entree'][0].rating_numbers
          ],
          label: 'My First dataset'
      }];
        this.chartLabels = [
            res['less_boisson'][0].name,
            res['less_plat'][0].name,
            res['less_dessert'][0].name,
            res['less_entree'][0].name
        ];
      },
      (err) => {
        console.log(err);
      }
    );
  }

}
