import { Component, OnInit } from '@angular/core';
import { ReactApplication } from '../../services/chatbot/react-application';

@Component({
  selector: 'app-chatbot',
  styleUrls: ['./chatbot.component.scss'],
  templateUrl: './chatbot.component.html'
})
export class ChatbotComponent implements OnInit {
    constructor() { }

    ngOnInit() {
        // Create the chatbot component
        ReactApplication.initialize(document.getElementById('cai-webchat-div'));
    }
}
