import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Food } from 'src/app/services/menus/food';
import { DelicacyType } from 'src/app/types/delicacy';

@Component({
  selector: 'app-delicacy',
  templateUrl: './delicacy.component.html',
  styleUrls: ['./delicacy.component.scss']
})
export class DelicacyComponent implements OnInit {

    @Input() food: Food<DelicacyType>;
    @Input() delicacy: DelicacyType;
    @Input() readonly: boolean;
    @Input() type = 'delete';

    @Output() delicacyChange: EventEmitter<DelicacyType>;
    @Output() action: EventEmitter<void>;

    edit = {
        name: false,
        description: false
    };

    constructor() {
        this.delicacyChange = new EventEmitter();
        this.action = new EventEmitter();
        this.readonly = false;
    }

    ngOnInit() { }

    actionClick() {
        this.action.emit();
    }

    updateData(name, value) {
        if (!this.delicacy.name || this.delicacy.name.trim() === ''
            || !this.delicacy.description || this.delicacy.description.trim() === '') {
            return;
        }

        if (!this.readonly) {
            this.delicacy[name] = value;
            this.edit[name] = false;
            this.food.edit(this.delicacy).subscribe(() => {
                this.delicacyChange.emit(this.delicacy);
            }, err => {
                console.log(err);
            });
        }
    }

    changeEdit(name) {
        Object.keys(this.edit).forEach(key => {
            this.edit[key] = false;
        });
        if (!this.readonly && name) {
            this.edit[name] = true;
        }
    }
}
