import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'devs-component',
  templateUrl: './devs.component.html',
  styleUrls: ['./devs.component.scss']
})
export class DevsComponent implements OnInit {

  public devs;

  constructor() {
      // List of devs
      this.devs = [
        {
            name: 'Stéphane Bonzom',
            job: 'Développeur Web Front-end',
            src: 'assets/team/bonzom.jpg',
            link: 'https://www.linkedin.com/in/stephane-bonzom/'
        },
        {
            name: 'Lucas Naman',
            job: 'Leader & Développeur Full-stack',
            src: 'assets/team/naman.jpg',
            link: 'https://www.linkedin.com/in/lucas-naman/'
        },
        {
            name: 'Cédric Ogire',
            job: 'Développeur Android',
            src: 'assets/team/ogire.jpg',
            link: 'https://www.linkedin.com/in/cedric-ogire/'
        },
        {
            name: 'Thomas Roche',
            job: 'Développeur Back-end',
            src: 'assets/team/roche.jpg',
            link: 'https://www.linkedin.com/in/thomas-roche/'
        },
        {
            name: 'Gabriel Sainte-Luce',
            job: 'Ingénieur IA',
            src: 'assets/team/sainte-luce.jpg',
            link: 'https://www.linkedin.com/in/gabriel-sainte-luce/'
        },
        {
            name: 'Yoann Sanchez',
            job: 'Développeur Front & Designer',
            src: 'assets/team/sanchez.jpg',
            link: 'https://www.linkedin.com/in/yoannsanchez/'
        },
        {
            name: 'Joanna Tchokonte',
            job: 'Développeuse iOS',
            src: 'assets/team/tchokonte.jpg',
            link: 'https://www.linkedin.com/in/joanna-tchokonte/'
        }
      ];
  }

  ngOnInit(): void {
  }

}
