import { EventEmitter, Output, Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/auth/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../../pages/login/login.component.scss', './login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm = new FormGroup({
        email : new FormControl('', Validators.required),
        password : new FormControl('', Validators.required)
    });
    @Output() clickBack = new EventEmitter();
    clickedLogin = false;
    error: string;

    constructor(private authService: AuthService) { }

    ngOnInit() {
    }

    cleanErr() {
        this.error = '';
    }

    clickBackBtn() {
        this.clickBack.emit();
    }

    onLogin() {
        if (this.loginForm.valid) {
            this.error = '';
            this.clickedLogin = true;
            this.authService.login({
                email: this.loginForm.value.email,
                password: this.loginForm.value.password
            })
            .subscribe(
                (res) => {
                    this.authService.logme(this.loginForm.value.email, res['token']);
                },
                (err) => {
                    console.log(err);
                    this.clickedLogin = false;
                    this.error = err.error;
                }
            );
        }
        return false;
    }
}
