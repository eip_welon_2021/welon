import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef } from '@angular/core';
import {} from 'googlemaps';
import { MapsService } from 'src/app/services/maps/maps.service';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit {
    private map?: google.maps.Map = null;
    private marker?: google.maps.Marker = null;

    @ViewChild('mapAddress') mapAddress: ElementRef;
    @ViewChild('mapContainer') mapContainer: ElementRef;

    private autocomplete: google.maps.places.Autocomplete;
    private geocoder: google.maps.Geocoder;

    @Output('changeMap') change: EventEmitter<any> = new EventEmitter<any>();

    @Input() height: string;

    private _input: any;
    get input() {
        return this._input;
    }

    @Input()
    set input(val: any) {
        if (this.isInputFilled(val) && this.map) {

            // Set Location if input is already filled
            const location = new google.maps.LatLng(val.latitude, val.longitude);

            this.map.panTo(location);
            this.map.setZoom(15);

            if (this.marker) {
                this.marker.setMap(null);
            }

            const marker = this._mapService.createRestaurantMarker(location);

            marker.setMap(this.map);

            this.marker = marker;
        }
        if (val.address !== undefined && val.address !== '') {
            this.mapAddress.nativeElement.value = val.address;
        }
        this._input = val;
    }
    constructor(private _mapService: MapsService) { }

    ngOnInit(): void {
           // Create the script tag, set the appropriate attributes
           this._mapService.loadMap(
            this.mapContainer.nativeElement,
            {
                streetViewControl: false,
                fullscreenControl: false,
                mapTypeControl: false,
                zoomControl: false
            },
            this.initMap.bind(this)
        );
    }

    initMap(map?: google.maps.Map): void {
        this.mapContainer.nativeElement.style.height = this.height;
        if (!map) {
            // Set error div if map can't be load
            this.mapAddress.nativeElement.style.display = 'none';

            const text = document.createElement('span');
            text.innerHTML = 'La carte n\'est pas disponible. Réessayez plus tard.';
            text.style.color = '#666';
            text.style.display = 'block';
            text.style.textAlign = 'center';

            const img = document.createElement('img');
            img.src = 'assets/pinCross.svg';
            img.style.height = `calc(${this.height} - 90px)`;
            img.style.display = 'block';
            img.style.marginBottom = '10px';

            this.mapContainer.nativeElement.style.padding = '20px 40px';
            this.mapContainer.nativeElement.style.height = `calc(${this.height} + 117px)`;
            this.mapContainer.nativeElement.style.display = 'flex';
            this.mapContainer.nativeElement.style.flexDirection = 'column';
            this.mapContainer.nativeElement.style.justifyContent = 'center';
            this.mapContainer.nativeElement.style.alignItems = 'center';
            this.mapContainer.nativeElement.style.backgroundColor = '#d3cce3';
            this.mapContainer.nativeElement.style.background = 'linear-gradient(to bottom, #d3cce3, #e9e4f0)';

            this.mapContainer.nativeElement.appendChild(img);
            this.mapContainer.nativeElement.appendChild(text);
            return;
        }

        this.map = map;

        // Autocomplete input map
        this.autocomplete = this._mapService.createAutocomplete(
            this.mapAddress.nativeElement,
            this.onPlaceSelected.bind(this)
        );

        this.input = this._input;

        this.geocoder = new google.maps.Geocoder();

        // Add a marker when click on map
        this.map.addListener('click', (e) => {
            if (this.marker) {
                this.marker.setMap(null);
            }

            const marker = this._mapService.createRestaurantMarker(e.latLng);

            this.map.panTo(e.latLng);
            marker.setMap(this.map);

            this.marker = marker;

            // Reverse geocode from location to Address after pointing on a place
            this._mapService.getAddressFromLocation(this.geocoder, e.latLng)
                .then(address => {
                    this.mapAddress.nativeElement.value = address.formatted_address;
                    this.changeData(address.formatted_address, e.latLng);
                }).catch(err => console.log(err));
        });
    }

    onPlaceSelected() {
        const place = this.autocomplete.getPlace();

        if (place === undefined) {
            return ;
        }

        this._mapService.getLocationFromPlaceId(this.geocoder, place.place_id)
        .then(result => {
            // Set marker if place is selected (in input)
            this.map.panTo(result.geometry.location);
            this.map.setZoom(15);

            if (this.marker) {
                this.marker.setMap(null);
            }

            const marker = this._mapService.createRestaurantMarker(result.geometry.location);
            marker.setMap(this.map);

            this.marker = marker;

            this.changeData(place.formatted_address, result.geometry.location);
        }).catch(err => {
            if (err.status !== 'INVALID_REQUEST') {
                console.log(err);
            }
        });
    }

    // Return of data
    changeData(address: string, location: google.maps.LatLng): void {
        this.change.emit({
            address,
            latitude: location.lat(),
            longitude: location.lng()
        });
    }

    // Check if input is valid & filled
    private isInputFilled(val: any) {
        if (val && val.latitude !== undefined && val.longitude !== undefined
            && val.latitude !== '' && val.longitude !== '' && val.address !== '') {
            return true;
        }
        return false;
    }
}
