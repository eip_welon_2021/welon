import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DessertsService } from 'src/app/services/menus/desserts/desserts.service';
import { DishesService } from 'src/app/services/menus/dishes/dishes.service';
import { DrinksService } from 'src/app/services/menus/drinks/drinks.service';
import { EntriesService } from 'src/app/services/menus/entries/entries.service';
import { MenusService } from 'src/app/services/menus/menus/menus.service';
import { DelicacyType } from 'src/app/types/delicacy';
import { MenuType } from 'src/app/types/menu';
import { MatDialog } from '@angular/material/dialog';
import { MenuFoodModalComponent } from '../modals/menu-food-modal/menu-food-modal.component';

@Component({
  selector: 'app-menu-food',
  templateUrl: './menu-food.component.html',
  styleUrls: ['./menu-food.component.scss']
})
export class MenuFoodComponent implements OnInit {

    @Input() menu: MenuType;

    @Output() delete: EventEmitter<void>;

    entries: DelicacyType[] = [];
    dishes: DelicacyType[] = [];
    desserts: DelicacyType[] = [];
    drinks: DelicacyType[] = [];

    constructor(
        private _menusService: MenusService,
        private _entriesService: EntriesService,
        private _dishesService: DishesService,
        private _dessertsService: DessertsService,
        private _drinksService: DrinksService,
        private _matDialog: MatDialog) {
            // Init data & events
            this.delete = new EventEmitter();

            this._entriesService.getAll().subscribe((list: DelicacyType[]) => {
                this.entries = list;
            });

            this._dishesService.getAll().subscribe((list: DelicacyType[]) => {
                this.dishes = list;
            });

            this._dessertsService.getAll().subscribe((list: DelicacyType[]) => {
                this.desserts = list;
            });

            this._drinksService.getAll().subscribe((list: DelicacyType[]) => {
                this.drinks = list;
            });
        }

    ngOnInit() { }

    deleteData() {
       this.delete.emit();
    }

    getDelicacyNameByIdList(list: DelicacyType[] = [], idList: string[] = []) {
        return list.filter(e => idList.includes(e._id)).map(e => e.name);
    }

    openModal() {
        const dialogRef = this._matDialog.open(MenuFoodModalComponent, {
            width: '700px',
            disableClose: true,
            data: {
                menu: JSON.parse(JSON.stringify(this.menu)),
                entries: this.entries,
                dishes: this.dishes,
                desserts: this.desserts,
                drinks: this.drinks
            }
        });

        // edit Menu card
        dialogRef.afterClosed().subscribe(result => {
            if (result.event === 'close') {
                this._menusService.edit(result.data).subscribe(() => {
                    this.menu = result.data;
                }, err => console.log(err));
            }
        });
    }
}
