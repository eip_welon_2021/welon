import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelicacyModalComponent } from './delicacy-modal.component';

describe('DelicacyModalComponent', () => {
  let component: DelicacyModalComponent;
  let fixture: ComponentFixture<DelicacyModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelicacyModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelicacyModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
