import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { DelicacyType } from 'src/app/types/delicacy';

@Component({
  selector: 'app-delicacy-modal',
  templateUrl: './delicacy-modal.component.html',
  styleUrls: ['./delicacy-modal.component.scss']
})
export class DelicacyModalComponent implements OnInit {

    public delicacy: DelicacyType;

    edit = {
        name: false,
        description: false
    };

    constructor(
        public dialogRef: MatDialogRef<DelicacyModalComponent>) {
            this.delicacy = {
                name: undefined,
                description: undefined,
                price: 0
            };
        }

    ngOnInit() {}

    // Click save btn
    closeDialog() {
        if (!this.delicacy.name || this.delicacy.name.trim() === ''
            || !this.delicacy.description || this.delicacy.description.trim() === '') {
            return;
        }
        this.dialogRef.close({ event: 'close', data: this.delicacy});
    }

    // Click cancel btn
    escapeDialog() {
      this.dialogRef.close({ event: 'escape', data: {} });
    }

    // Update data in locally
    updateData(path: string, value: string) {
        this.delicacy[path] = value;
        this.edit[path] = false;
    }

    // Change input location
    changeEdit(name) {
        Object.keys(this.edit).forEach(key => {
            this.edit[key] = false;
        });
        if (name) {
            this.edit[name] = true;
        }
    }

}
