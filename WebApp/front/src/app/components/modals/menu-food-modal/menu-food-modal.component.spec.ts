import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuFoodModalComponent } from './menu-food-modal.component';

describe('MenuFoodModalComponent', () => {
  let component: MenuFoodModalComponent;
  let fixture: ComponentFixture<MenuFoodModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuFoodModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuFoodModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
