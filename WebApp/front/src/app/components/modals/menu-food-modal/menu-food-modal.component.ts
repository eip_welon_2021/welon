import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DelicacyType } from 'src/app/types/delicacy';
import { MenuType } from 'src/app/types/menu';

interface MenuFoodModalData {
    menu: MenuType;
    entries: DelicacyType[];
    dishes: DelicacyType[];
    desserts: DelicacyType[];
    drinks: DelicacyType[];
}

@Component({
  selector: 'app-menu-food-modal',
  templateUrl: './menu-food-modal.component.html',
  styleUrls: ['./menu-food-modal.component.scss']
})
export class MenuFoodModalComponent implements OnInit {

    public menu: MenuType;

    edit = {
        name: false,
        description: false
    };

    constructor(
            public dialogRef: MatDialogRef<MenuFoodModalComponent>,
            @Inject(MAT_DIALOG_DATA) public data: MenuFoodModalData) {
                this.menu = this.data.menu;
            }

    // Update data locally
    updateData(path: string, value: string) {
        this.menu[path] = value;
        this.edit[path] = false;
    }

    ngOnInit() {}

    // Check if delicacy is in list
    isDataInList(path: string, item: DelicacyType) {
        return this.menu[path].includes(item._id);
    }

    // Add delicacy to list
    addItem(path: string, item: DelicacyType) {
        this.menu[path].push(item._id);
    }

    // Remove delicacy from list
    removeItem(path: string, item: DelicacyType) {
        this.menu[path] = this.menu[path].filter((e: string) => e !== item._id) || [];
    }

    // Change input edit data
    changeEdit(name) {
        Object.keys(this.edit).forEach(key => {
            this.edit[key] = false;
        });
        if (name) {
            this.edit[name] = true;
        }
    }

    // On save btn
    closeDialog() {
        if (!this.menu.name || this.menu.name.trim() === ''
           || !this.menu.description || this.menu.description.trim() === '') {
            return;
        }
        this.dialogRef.close({ event: 'close', data: this.menu});
    }

    // On cancel btn
    escapeDialog() {
      this.dialogRef.close({ event: 'escape', data: {} });
    }

}
