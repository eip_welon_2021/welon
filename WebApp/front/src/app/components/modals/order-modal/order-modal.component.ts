import { Component, } from '@angular/core';
import { OrderService } from 'src/app/services/order/order.service';

@Component({
  selector: 'app-order-modal',
  templateUrl: './order-modal.component.html',
  styleUrls: ['./order-modal.component.scss']
})
export class OrderModalComponent {

  total = 0;

  constructor(public orderService: OrderService) {
    this.orderService.orderMap.forEach((value, key) => {
      this.total += this.orderService.table[value].get(key).price * this.orderService.table[value].get(key).nb;
    });
  }

  // Sent order
  validate() {
    this.orderService.sendOrder().subscribe(
      (res) => {
        this.createImageFromBlob(res);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  // create qrcode
  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load',
      () => {
          this.orderService.qrCode = reader.result;
          this.orderService.isQR = true;
      },
      false);

    if (image) {
      if (image.type !== 'application/pdf') {
        reader.readAsDataURL(image);
      }
    }
  }
}
