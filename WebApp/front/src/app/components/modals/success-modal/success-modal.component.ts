import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
    title: string;
    msg: string;
}

@Component({
  selector: 'app-success-modal',
  templateUrl: './success-modal.component.html',
  styleUrls: ['./success-modal.component.scss']
})
export class SuccessModalComponent {
    constructor(
        public dialogRef: MatDialogRef<SuccessModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

    onClick(): void {
      this.dialogRef.close();
    }
}
