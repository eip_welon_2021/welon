import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { ImageService } from 'src/app/services/upload/image.service';
import { SuccessModalComponent } from '../success-modal/success-modal.component';

export interface DialogData {
  msg: string;
  image: any;
}

@Component({
  selector: 'app-upload-modal',
  templateUrl: './upload-modal.component.html',
  styleUrls: ['./upload-modal.component.scss']
})
export class UploadModalComponent {

  public newImage?: string = null;
  public newFile?: File = null;
  public error?: string = null;

  constructor(
      private dialSuccess: MatDialog,
    public dialogRef: MatDialogRef<UploadModalComponent>,
    private imageService: ImageService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  showImage(event) {
    this.newFile = (event.target as HTMLInputElement).files[0];

    const reader = new FileReader();

    reader.addEventListener('load', () => {
        this.newImage = reader.result.toString();

        const filesize = ((this.newFile.size / 1024) / 1024);
        if (filesize > 2) {
            this.error = 'L\'image dépasse la taille de 2Mo.';
        }
    });

    if (typeof this.newFile !== 'undefined') {
        reader.readAsDataURL(this.newFile);
    } else {
        this.error = null;
        this.newImage = null;
        this.newFile = null;
    }
  }

  onReturn(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    if (this.error === null) {
        const formData: any = new FormData();

        formData.append('image', this.newFile);
        this.imageService.uploadImage(formData).subscribe(
            () => {
                this.dialogRef.close();
                this.dialSuccess.open(SuccessModalComponent, {
                    data: {
                        title: 'Votre demande a bien été prise en compte',
                        msg: 'Votre photo de restaurant sera modifiée dans un peu moins d\'une minute.'
                    }
                });
            },
            () => {
                this.error = 'Une erreur c\'est produite: L\'image peut être trop lourde ou au mauvais format.';
            }
        );
    }
  }

  resetImage(): void {
    this.error = null;
    this.newFile = null;
    this.newImage = null;
  }
}
