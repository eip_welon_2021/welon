import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-topbar-dashboard',
  templateUrl: './topbar-dashboard.component.html',
  styleUrls: ['./topbar-dashboard.component.scss']
})
export class TopBarDashboardComponent implements OnInit {

    menu_user: any[] = [
        {
            name: 'Tutoriel',
            icon: 'fas fa-question',
            route: '/tutorials/web',
            click: () => false
        },
        {
            name: 'Profil',
            icon: 'fas fa-user',
            route: '/profile',
            click: () => false
        },
        {
            name: 'Se Déconnecter',
            danger: true,
            click: this.logout.bind(this),
            icon: 'fas fa-sign-out-alt',
        },
    ];

    menu_route: any[] = [
        {
          icon: 'fas fa-chart-pie',
          name: 'tableau de bord',
          route: '/dashboard'
        },
        {
          icon: 'fas fa-book-open',
          name: 'carte',
          route: '/menus'
        },
        {
          icon: 'fas fa-receipt',
          name: 'commandes',
          route: '/order'
        }
      ];

    constructor(private authService: AuthService,
                private router: Router) {
        // Set active link
        this.router.events
        .pipe(filter(e => e instanceof NavigationStart || e instanceof NavigationEnd))
        .subscribe((navStart: NavigationStart) => {
            let path = navStart.url;
            const idx = navStart.url.indexOf('#');

            if (idx >= 0) {
                path = navStart.url.substring(0, idx);
            }

            this.menu_route.forEach(item => {
                item.selected = false;
                if (item.route && path === item.route) {
                    item.selected = true;
                }
            });

            this.menu_user.forEach(item => {
                item.selected = false;
                if (item.route && path === item.route) {
                    item.selected = true;
                }
            });
        });
    }

    ngOnInit() { }

    logout() {
        this.authService.logout();
        return true;
    }
}
