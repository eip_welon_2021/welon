import { Component, OnInit, Input } from '@angular/core';
import { OrderService } from 'src/app/services/order/order.service';
import { FoodType } from 'src/app/types/food';

@Component({
  selector: 'tr[app-orderrow]',
  templateUrl: './orderrow.component.html',
  styleUrls: ['./orderrow.component.scss']
})
export class OrderrowComponent implements OnInit {

  @Input()
  private id: string;

  @Input()
  food: FoodType;

  @Input()
  private kind: number;

  constructor(private orderService: OrderService) { }

  ngOnInit() {}

  // Add delicacy or menu to order
  add() {
    this.orderService.addOne(this.id, this.kind);
  }

  // Remove delicacy or menu to order
  remove() {
    this.orderService.removeOne(this.id, this.kind);
  }

}
