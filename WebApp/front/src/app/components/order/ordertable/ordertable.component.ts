import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { OrderService } from '../../../services/order/order.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-ordertable',
  templateUrl: './ordertable.component.html',
  styleUrls: ['./ordertable.component.scss']
})
export class OrdertableComponent implements OnInit, OnDestroy {

  orderSub: Subscription;
  order = new Map();
  title: string;

  @Input()
  private kind: number;

  constructor(private orderService: OrderService) { }

  ngOnInit() {
    this.title =
        this.kind === 0 ? 'Menus' :
            (this.kind === 1 ? 'Entrées' :
                (this.kind === 2 ? 'Plats' :
                    (this.kind === 3 ? 'Desserts' : 'Boissons')));

    this.orderSub = this.orderService.getOrder().subscribe(
      (order) => {
        this.order = order[this.kind];
      });
  }

  ngOnDestroy() {
    this.orderSub.unsubscribe();
  }

}
