import { Component, Input, ViewChild, ElementRef, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-password-input',
  templateUrl: './password-input.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PasswordInputComponent),
      multi: true
    }
  ],
  styleUrls: ['./password-input.component.scss']
})
export class PasswordInputComponent implements ControlValueAccessor  {

    @Input() label;
    @Input() autocomplete;
    @Input() labelClass;
    @Input() inputClass;
    @Input() id;

    @ViewChild('password') password: ElementRef;

    constructor() { }

    // Control accessor methods
    propagateChange = (_: any) => {};

    writeValue(value: any): void {
        if (typeof value !== 'undefined') {
            this.password.nativeElement.value = value;
        }
    }
    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(): void {}

    setDisabledState(): void {}

    onDataChange(value) {
        this.propagateChange(value);
    }

    // Change visibility
    passwordVisibility() {
      this.password.nativeElement.type =
          this.password.nativeElement.type === 'password' ? 'text' : 'password';
    }

}
