import { Component, Input } from '@angular/core';
import { ProfileService } from '../../services/profile/profile.service';
import { MatDialog } from '@angular/material';
import { UploadModalComponent } from '../modals/upload-modal/upload-modal.component';
import { ProfileType } from 'src/app/types/profile';

@Component({
  selector: 'app-profile-image',
  templateUrl: './profile-image.component.html',
  styleUrls: ['./profile-image.component.scss']
})
export class ProfileImageComponent {

  public image: string = null;
  public loading = true;
  public _profile: ProfileType;

  @Input() set profile(val: ProfileType) {
    this._profile = val;

    this.profileService.getPicture()
    .subscribe(
        (next: Blob) => {
            this.createImageFromBlob(next);
        },
        () => {
            this.image = null;
            this.loading = false;
        },
        () => this.loading = false
    );
  }

  constructor(private profileService: ProfileService, private dialog: MatDialog) { }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();

    reader.addEventListener('load', () => {
        this.image = reader.result.toString();
    });

    if (image) {
        reader.readAsDataURL(image);
    }
  }

  changePicture() {
    const dialogRef = this.dialog.open(UploadModalComponent, {
      disableClose: true,
      data: {image: this.image}
    });
  }
}
