import { EventEmitter, Output, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/auth/auth.service';
import { UserType } from 'src/app/types/user';
import { mustMatch } from 'src/app/helpers/CustomValidators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../../pages/login/login.component.scss', './register.component.scss']
})
export class RegisterComponent implements OnInit {

  @Output() clickBack = new EventEmitter();
  error: string;
  step: number ;
  totalStep: number;
  clickedRegister: boolean;
  registerForm: FormGroup;

  constructor(private authService: AuthService, private _formBuilder: FormBuilder, private _refs: ChangeDetectorRef) {
    this.step = 1;
    this.totalStep = 0;
    this.clickedRegister = false;
    this.registerForm = this._formBuilder.group({
      infos: this._formBuilder.group({
        name : this._formBuilder.control('',  Validators.required),
        phone : this._formBuilder.control('',  [Validators.required, Validators.pattern(/^0[1-9][0-9]{8}$/)])
      }),
      map: this._formBuilder.group({
        address: this._formBuilder.control('', [Validators.required]),
        latitude: this._formBuilder.control('', [Validators.required, Validators.pattern(/^\-?[0-9]+(\.[0-9]+)?$/)]),
        longitude: this._formBuilder.control('', [Validators.required, Validators.pattern(/^\-?[0-9]+(\.[0-9]+)?$/)])
      }),
      auth: this._formBuilder.group({
        email : this._formBuilder.control('', [Validators.required, Validators.email]),
        password : this._formBuilder.control('',  Validators.required),
        passwordConfirm : this._formBuilder.control('',  Validators.required)
      },  {
        validators: mustMatch('password', 'passwordConfirm')
      })
    });

    this.totalStep = Object.keys(this.registerForm.value).length;
  }

  ngOnInit() {
  }

  cleanErr() {
    this.error = '';
  }

  clickBackBtn() {
    this.clickBack.emit();
  }

  // change data of map
  populateMap(values: any) {
    this.registerForm.get('map').setValue(values);
    this.registerForm.updateValueAndValidity();

    this._refs.detectChanges();
  }

  // check if step is valid
  isValidStep() {
    return this.registerForm.get(Object.keys(this.registerForm.value)[this.step - 1]).valid;
  }

  // go to the next step
  nextStep(e?: any) {
    if (!this.isValidStep()) {
      return false;
    }

    if (this.step >= this.totalStep) {
      this.onRegister();
    } else if (e !== 'click' && Object.keys(this.registerForm.value)[this.step - 1] === 'map') {
      return false;
         } else {
      this.step = this.step + 1;
         }
    return false;
  }

  // go to the previous step
  prevStep() {
    this.step = this.step - 1;
  }

  // send data to api
  onRegister() {
    if (this.registerForm.valid) {
      this.error = '';
      this.clickedRegister = true;

      const data: UserType = {
        email: this.registerForm.get('auth').value.email,
        password: this.registerForm.get('auth').value.password,
        name: this.registerForm.get('infos').value.name,
        address: this.registerForm.get('map').value.address,
        longitude: this.registerForm.get('map').value.longitude,
        latitude: this.registerForm.get('map').value.latitude,
        phone: this.registerForm.get('infos').value.phone
      };

      this.authService.register(data)
      .subscribe(
        (res) => {
          this.authService.logme(this.registerForm.value.email, res['token']);
        },
        (err) => {
          this.error = err.error;
          this.clickedRegister = false;
          console.log(err);
        }
      );
    }
  }
}
