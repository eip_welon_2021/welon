import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WBtnIconComponent } from './w-btn-icon.component';

describe('WBtnIconComponent', () => {
  let component: WBtnIconComponent;
  let fixture: ComponentFixture<WBtnIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WBtnIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WBtnIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
