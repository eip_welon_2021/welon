import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'w-btn-icon',
  templateUrl: './w-btn-icon.component.html',
  styleUrls: ['./w-btn-icon.component.scss']
})
export class WBtnIconComponent {

    @Input() icon = undefined;
    @Input() disabled = false;
    @Input() inverted = false;
    @Input() color = 'primary';

    @Output() click: EventEmitter<MouseEvent> = new EventEmitter();

    constructor() { }

    onButtonClick(event: MouseEvent) {
        event.stopPropagation();

        this.click.emit(event);
   }
}
