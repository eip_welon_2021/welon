import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WBtnComponent } from './w-btn.component';

describe('WBtnComponent', () => {
  let component: WBtnComponent;
  let fixture: ComponentFixture<WBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
