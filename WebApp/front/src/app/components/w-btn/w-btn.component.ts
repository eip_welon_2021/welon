import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'w-btn',
  templateUrl: './w-btn.component.html',
  styleUrls: ['./w-btn.component.scss']
})
export class WBtnComponent {

    @Input() rounded = false;
    @Input() disabled = false;
    @Input() inverted = false;
    @Input() color = 'primary';
    @Input() preIcon = undefined;

    @Output() click: EventEmitter<MouseEvent> = new EventEmitter();

    constructor() { }

    onButtonClick(event: MouseEvent) {
        event.stopPropagation();

        this.click.emit(event);
    }
}
