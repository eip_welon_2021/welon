import { Component, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'w-card',
  templateUrl: './w-card.component.html',
  styleUrls: ['./w-card.component.scss']
})
export class WCardComponent {
    @Input() imgSrc = '/assets/default_image.jpg';
    @Input() size = 'card';
    @Input() type = undefined;
    @Input() readonly = false;

    @Input() price: number;
    @Output() priceChange = new EventEmitter<number>();

    @Output() clickAction = new EventEmitter();

    @ViewChild('inputPrice') inputPrice: ElementRef;

    public editPrice = false;

    constructor() { }

    openPrice() {
        if (!this.readonly) {
            this.editPrice = true;
            setTimeout(() => {
                this.inputPrice.nativeElement.focus();
            }, 0);
        }
    }

    closePrice() {
        if (!this.readonly) {
            this.editPrice = false;
            this.priceChange.emit(this.price);
        }
    }

    action(e: any) {
        e.stopPropagation();
        this.clickAction.emit();
    }
}
