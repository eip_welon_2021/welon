import { FormGroup } from '@angular/forms';

// Check if 2 from controls are similar
export function mustMatch(firstControl: string, secondControl: string): any {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[firstControl];
      const matchingControl = formGroup.controls[secondControl];

      if (matchingControl.errors && ! matchingControl.errors.mustMatch) {
        return;
      }

      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
}
