import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Cookie } from 'src/app/services/cookie/cookie.service';
import { NgcCookieConsentService } from 'ngx-cookieconsent';

@Component({
  selector: 'app-login-layout',
  templateUrl: './login-layout.component.html',
  styleUrls: ['./login-layout.component.scss']
})
export class LoginLayoutComponent implements OnInit {

  clickedLogin = false;
  clickedRegister = false;

  constructor(private authService: AuthService, private cookieService: Cookie, private ccService: NgcCookieConsentService) {}

  ngOnInit() { }

  goHome() {
    if (this.clickedLogin) {
      this.clickedLogin = false;
    }
    if (this.clickedRegister) {
      this.clickedRegister = false;
    }
  }

}
