import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifiedAccountPageComponent } from './account-verified.component';

describe('VerifyPageComponent', () => {
  let component: VerifiedAccountPageComponent;
  let fixture: ComponentFixture<VerifiedAccountPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifiedAccountPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifiedAccountPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
