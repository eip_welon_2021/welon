import { Component, OnInit } from '@angular/core';
import { ProfileType } from '../../types/profile';
import { Cookie } from 'src/app/services/cookie/cookie.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-account-verified',
  templateUrl: './account-verified.component.html',
  styleUrls: ['./account-verified.component.scss']
})
export class VerifiedAccountPageComponent implements OnInit {

    error = false;

    constructor(private _auth: AuthService,
        private _router: Router,
        private _route: ActivatedRoute,
        private _cookie: Cookie) {
        // check if URI has accessToken
        this._route.queryParams.subscribe(params => {
            if (typeof params.token !== 'undefined') {
                this._cookie.set('accessToken', params.token);
            } else {
                this._router.navigate(['/login']);
            }
        });
    }

    ngOnInit() {}

    // check if the account is verified on refresh
    connect() {
        this._auth.checkVerify()
        .subscribe((res: ProfileType) => {
            this.error = false;
            if (res.verified) {
                this._router.navigate(['/dashboard']);
            } else {
                this._router.navigate(['/verify']);
            }
        }, () => {
            this.error = true;
        });
    }
}
