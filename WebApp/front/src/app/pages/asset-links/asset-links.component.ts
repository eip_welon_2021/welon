import { Component, OnInit, Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-asset-links',
  templateUrl: './asset-links.component.html',
  styleUrls: ['./asset-links.component.scss']
})
export class AssetLinksComponent implements OnInit {

  constructor(
      private authService: AuthService
  ) { }

  ngOnInit() {
    this.authService.logoutAsset();
  }

}
