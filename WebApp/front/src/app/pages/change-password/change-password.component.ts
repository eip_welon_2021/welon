import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RequestService } from 'src/app/services/request/request.service';
import { Cookie } from 'src/app/services/cookie/cookie.service';
import { mustMatch } from 'src/app/helpers/CustomValidators';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  public passwords: FormGroup;

  public success = false;
  public error = null;

  constructor(private _router: Router, private _request: RequestService, private _cookie: Cookie) {
    this.passwords = new FormGroup({
      new: new FormControl('', [Validators.required]),
      confirm: new FormControl('', [Validators.required]),
      current: new FormControl('', [Validators.required])
    }, {
      validators: mustMatch('new', 'confirm')
    });
  }

  ngOnInit() { }

  backNav() {
    this._router.navigate(['profile']);
  }

  // Submit change password
  submit() {
    if (this.passwords.valid) {
        this.error = null;
        this._request.post('auth/password/reset/restaurant', {
            email: this._cookie.get('user'),
            old_password: this.passwords.get('current').value,
            new_password: this.passwords.get('new').value
        }, 'text/plain').subscribe(() => {
          this.success = true;
        }, (e) => {
            this.error = e.error;
        });
    } else {
        this.success = false;
        if (this.passwords.errors === null) {
            this.error = 'Un ou plusieurs champs ne sont pas renseignés.';
        } else {
            this.error = 'Les mots de passe sont différents.';
        }
    }
  }

}
