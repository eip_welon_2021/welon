import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/charts/dashboard/dashboard.service';

export interface Res {
  boisson_quantity: number;
  dessert_quantity: number;
  entree_quantity: number;
  plat_quantity: number;
  menu_quantity: number;
  general_average: number;
  price_average: number;
  quality_average: number;
  quantity_average: number;
  ratings_number: number;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  average = '0';
  nbrFeedbacks = '0';
  average_perc = '0%';
  hasData: Boolean = false;
  moneyOfYesterday = undefined;

  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.dashboardService.getData()
    .subscribe(
        (res: Res) => {
            if (res.boisson_quantity === 0 || res.dessert_quantity === 0 || res.entree_quantity === 0 || res.plat_quantity === 0) {
                return ;
            }
            this.hasData = true;
            this.average = res.general_average.toFixed(1);
            this.nbrFeedbacks = res.ratings_number.toString();
            this.average_perc = ((res.general_average / 5) * 100).toString() + '%';
            this.dashboardService.getYesterdayMoneyEarned().subscribe(data => {
                this.moneyOfYesterday = data.total;
            });
        },
        (err) => {
            console.log(err);
        }
    );
  }

}
