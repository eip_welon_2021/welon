import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { mustMatch } from 'src/app/helpers/CustomValidators';
import { ProfileService } from 'src/app/services/profile/profile.service';
import { ProfileType } from 'src/app/types/profile';

@Component({
  selector: 'app-delete-account',
  templateUrl: './delete-account.component.html',
  styleUrls: ['./delete-account.component.scss']
})
export class DeleteAccountComponent implements OnInit {

  public deleteAccount: FormGroup;
  public profile: ProfileType;

  public success = false;
  public error = null;

  constructor(private _router: Router, private _profileService: ProfileService) {
    this.deleteAccount = new FormGroup({
      current: new FormControl('', [Validators.required]),
      confirm: new FormControl('', [Validators.required])
    }, {
      validators: mustMatch('current', 'confirm')
    });

    this._profileService.getData().subscribe((res: ProfileType) => {
        this.profile = res;
    });
  }

  ngOnInit() { }

  backNav() {
    this._router.navigate(['profile']);
  }

  submitForm() {
    this.success = false;
    this.error = null;
    if (this.deleteAccount.valid) {
        this.error = null;
        this._profileService.deleteAccount(this.profile.email)
        .subscribe(
            () => {
                this.success = true;
                this._router.navigate(['/login']);
            },
            (err) => this.error = err.error || 'Une erreur est survenue.'
        );
    } else {
      if (this.deleteAccount.errors === null) {
        this.error = 'Un ou plusieurs champs ne sont pas correctement renseignés.';
      } else {
        this.error = 'Les mots de passe sont différents.';
      }
    }
  }
}
