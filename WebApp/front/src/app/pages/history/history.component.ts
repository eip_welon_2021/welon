import { Component, OnInit } from '@angular/core';
import { Cookie } from 'src/app/services/cookie/cookie.service';
import { DessertsService } from 'src/app/services/menus/desserts/desserts.service';
import { DishesService } from 'src/app/services/menus/dishes/dishes.service';
import { DrinksService } from 'src/app/services/menus/drinks/drinks.service';
import { EntriesService } from 'src/app/services/menus/entries/entries.service';
import { Food } from 'src/app/services/menus/food';
import { MenusService } from 'src/app/services/menus/menus/menus.service';
import { OrderService } from 'src/app/services/order/order.service';
import { DelicacyType } from 'src/app/types/delicacy';
import { MenuType } from 'src/app/types/menu';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

    public history: any[];
    public menuList: MenuType[];
    public entryList: DelicacyType[];
    public dishList: DelicacyType[];
    public dessertList: DelicacyType[];
    public drinkList: DelicacyType[];

    constructor(private _orderService: OrderService,
        private _menusService: MenusService,
        private _entriesService: EntriesService,
        private _dishesService: DishesService,
        private _dessertsService: DessertsService,
        private _drinksService: DrinksService,
        private _cookieService: Cookie) {

        // get all data
        this._orderService.getAll().subscribe((_h) => {
            this.history = _h;
        });

        this._menusService.getAll().subscribe(data => {
            this.menuList = data.filter(e => e.id_restaurant === this._cookieService.get('id'));
        });

        this._entriesService.getAll().subscribe(data => {
            this.entryList = data;
        });

        this._dishesService.getAll().subscribe(data => {
            this.dishList = data;
        });

        this._dessertsService.getAll().subscribe(data => {
            this.dessertList = data;
        });

        this._drinksService.getAll().subscribe(data => {
            this.drinkList = data;
        });
    }

    ngOnInit() {}

    // get name list of delicacies
    getData(listElements: DelicacyType[] = [], list) {
        const data = [];

        list.forEach(j => {
            data.push(j[0]);
        });

        const l = listElements.filter(e => data.includes(e._id)).map(e => e.name).join(', ');
        return l === '' ? 'Aucune donnée renseignée.' : l;
    }
}

