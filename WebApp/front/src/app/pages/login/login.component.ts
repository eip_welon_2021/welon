import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Cookie } from 'src/app/services/cookie/cookie.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginPageComponent implements OnInit {

  clickedLogin = false;
  clickedRegister = false;

  constructor(private _router: Router, private cookieService: Cookie, private _auth: AuthService) { }

  ngOnInit() {
    if (this.cookieService.check('loggedIn') === true) {
      this._auth.logout();
    }
  }

  goHome() {
    if (this.clickedLogin) {
      this.clickedLogin = false;
    }
    if (this.clickedRegister) {
      this.clickedRegister = false;
    }
  }

}
