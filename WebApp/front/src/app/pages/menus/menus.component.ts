import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DelicacyModalComponent } from 'src/app/components/modals/delicacy-modal/delicacy-modal.component';
import { MenuFoodModalComponent } from 'src/app/components/modals/menu-food-modal/menu-food-modal.component';
import { Cookie } from 'src/app/services/cookie/cookie.service';
import { DessertsService } from 'src/app/services/menus/desserts/desserts.service';
import { DishesService } from 'src/app/services/menus/dishes/dishes.service';
import { DrinksService } from 'src/app/services/menus/drinks/drinks.service';
import { EntriesService } from 'src/app/services/menus/entries/entries.service';
import { Food } from 'src/app/services/menus/food';
import { MenusService } from 'src/app/services/menus/menus/menus.service';
import { RequestService } from 'src/app/services/request/request.service';
import { DelicacyType } from 'src/app/types/delicacy';
import { MenuType } from 'src/app/types/menu';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.scss']
})
export class MenusComponent {

    public state = 0;
    public menu: Food<MenuType>;
    public entry: Food<DelicacyType>;
    public dish: Food<DelicacyType>;
    public dessert: Food<DelicacyType>;
    public drink: Food<DelicacyType>;

    menuList: MenuType[];
    entryList: DelicacyType[];
    dishList: DelicacyType[];
    dessertList: DelicacyType[];
    drinkList: DelicacyType[];

    constructor(private route: ActivatedRoute,
        private router: Router,
        private _requestService: RequestService,
        private _cookieService: Cookie,
        private _matDialog: MatDialog) {
        this.menu = new MenusService(this._requestService);
        this.entry = new EntriesService(this._requestService);
        this.dish = new DishesService(this._requestService);
        this.dessert = new DessertsService(this._requestService);
        this.drink = new DrinksService(this._requestService);

        // get data
        this.menu.getAll().subscribe(items => {
            this.menuList = items.filter(e => e.id_restaurant === this._cookieService.get('id'));
        });

        this.entry.getAll().subscribe(items => {
            this.entryList = items;
        });

        this.dish.getAll().subscribe(items => {
            this.dishList = items;
        });

        this.dessert.getAll().subscribe(items => {
            this.dessertList = items;
        });

        this.drink.getAll().subscribe(items => {
            this.drinkList = items;
        });

        // check the state in the fragment
        this.route.fragment.subscribe((fragment: string) => {
            if (fragment !== undefined && fragment !== null && fragment !== '') {
                const state = parseInt(fragment, 10);
                if (!isNaN(state)) {
                    this.changeState(state);
                    return ;
                }
            }
            this.router.navigate(['/menus'], {
                fragment: '0',
                skipLocationChange: true
            });
        });
    }

    changeState(nbr: number): void {
        this.state = nbr;
        this.router.navigate(['/menus'], {
            fragment: nbr.toString()
        });
    }

    deleteData(path: string, handler: Food<any>, item: DelicacyType) {
        handler.delete(item._id).subscribe(() => {
            handler.getAll().subscribe(values => {
                if (path === 'menu') {
                    this.menuList = values.filter(e => e.id_restaurant === this._cookieService.get('id'));
                } else if (path === 'entry') {
                    this.entryList = values;
                } else if (path === 'dish') {
                    this.dishList = values;
                } else if (path === 'dessert') {
                    this.dessertList = values;
                } else if (path === 'drink') {
                    this.drinkList = values;
                }
            }, err => console.log(err));
        }, err => console.log(err));
    }

    addItem(handler: Food<any>) {
        let dialogRef;
        if (this.state === 0) {
            dialogRef = this._matDialog.open(MenuFoodModalComponent, {
                data: {
                    menu: {
                        name: undefined,
                        description: undefined,
                        price: 0,
                        id_entree: [],
                        id_plat: [],
                        id_dessert: [],
                        id_boisson: []
                    },
                    entries: this.entryList,
                    dishes: this.dishList,
                    desserts: this.dessertList,
                    drinks: this.drinkList
                },
                disableClose: true
            });
        } else {
            dialogRef = this._matDialog.open(DelicacyModalComponent, {
                disableClose: true
            });
        }

        // add delicacy or menu
        dialogRef.afterClosed().subscribe(result => {
            if (result.event === 'close') {
                handler.add(result.data).subscribe(() => {
                    handler.getAll().subscribe(data => {
                        if (this.state === 0) {
                            this.menuList = data.filter(e => e.id_restaurant === this._cookieService.get('id'));
                        } else if (this.state === 1) {
                            this.entryList = data;
                        } else if (this.state === 2) {
                            this.dishList = data;
                        } else if (this.state === 3) {
                            this.dessertList = data;
                        } else if (this.state === 4) {
                            this.drinkList = data;
                        }
                    }, err => console.log(err));
                }, err => console.log(err));
            }
        });
    }

    menus() {
        this.changeState(0);
    }

    entries() {
        this.changeState(1);
    }

    dishes() {
        this.changeState(2);
    }

    desserts() {
        this.changeState(3);
    }

    drinks() {
        this.changeState(4);
    }

}
