import { Component } from '@angular/core';
import { OrderService } from '../../services/order/order.service';
import { MatDialog } from '@angular/material/dialog';
import { OrderModalComponent } from 'src/app/components/modals/order-modal/order-modal.component';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent {

  constructor(public orderService: OrderService, private dialog: MatDialog) {
    this.orderService.resetFood();
    this.orderService.resetOrder();
  }

  // display the modal
  validate() {
    this.openDialog();
  }

  // reset the numbber of each Food to 0
  reset() {
    this.orderService.resetFood();
  }

  openDialog() {
    const dialogRef = this.dialog.open(OrderModalComponent);
  }

  continue() {
    this.orderService.resetOrder();
    this.orderService.orderMap.clear();
    this.orderService.isQR = false;
  }

}
