import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { ProfileService } from '../../services/profile/profile.service';
import { ProfileType } from '../../types/profile';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    profileForm: FormGroup;
    profile: ProfileType;
    success = false;
    error?: string = null;

    constructor(
        private _refs: ChangeDetectorRef,
        private _profileService: ProfileService) {
        this.profileForm = new FormGroup({
            name: new FormControl('',  Validators.required),
            phone: new FormControl('', [Validators.required, Validators.pattern(/^0[1-9][0-9]{8}$/)]),
            map: new FormGroup({
                address: new FormControl('', [Validators.required]),
                latitude: new FormControl('', [Validators.pattern(/^(\-?[0-9]+(\.[0-9]+)?)?$/)]),
                longitude: new FormControl('', [Validators.pattern(/^(\-?[0-9]+(\.[0-9]+)?)?$/)])
            }),
            email : new FormControl({ value: '', disabled: true }, [Validators.required, Validators.email]),
        });
    }

    // get data
    ngOnInit() {
        this._profileService.getData().subscribe(
            (res: ProfileType) => {
                this.profileForm.patchValue({
                    name: res.name,
                    phone: res.phone,
                    map: {
                        address: res.address,
                        latitude: res.latitude,
                        longitude: res.longitude
                    },
                    email: res.email
                });
                this.profile = res;
                this.profileForm.updateValueAndValidity();
                this._refs.detectChanges();
            });
    }

    onSubmit () {
        this.error = null;
        this.success = false;
        this._profileService.update({
            name: this.profileForm.get('name').value,
            phone: this.profileForm.get('phone').value,
            address: this.profileForm.get('map').get('address').value,
            longitude: this.profileForm.get('map').get('longitude').value,
            latitude: this.profileForm.get('map').get('latitude').value
        }).subscribe(
            (res: ProfileType) => {
                this.profile = res;
                this.success = true;
            },
            (err) => this.error = err.error || 'Une erreur est survenue.'
        );
    }

    populateMap(values: any) {
        this.profileForm.get('map').setValue(values);
        this.profileForm.updateValueAndValidity();

        this._refs.detectChanges();
    }
}
