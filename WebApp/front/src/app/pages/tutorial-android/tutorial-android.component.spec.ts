import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorialAndroidComponent } from './tutorial-android.component';

describe('TutorialAndroidComponent', () => {
  let component: TutorialAndroidComponent;
  let fixture: ComponentFixture<TutorialAndroidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorialAndroidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorialAndroidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
