import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorialIosComponent } from './tutorial-ios.component';

describe('TutorialIosComponent', () => {
  let component: TutorialIosComponent;
  let fixture: ComponentFixture<TutorialIosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorialIosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorialIosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
