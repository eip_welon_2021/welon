import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorialWebComponent } from './tutorial-web.component';

describe('TutorialWebComponent', () => {
  let component: TutorialWebComponent;
  let fixture: ComponentFixture<TutorialWebComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorialWebComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorialWebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
