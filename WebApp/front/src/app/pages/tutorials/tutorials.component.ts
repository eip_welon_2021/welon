import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tutorials',
  templateUrl: './tutorials.component.html',
  styleUrls: ['./tutorials.component.scss']
})
export class TutorialsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  clickAndroid() {
    window.location.pathname = '/tutorials/android';
  }

  clickIos() {
    window.location.pathname = '/tutorials/ios';
  }

}
