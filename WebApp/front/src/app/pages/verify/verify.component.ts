import { Component, OnInit } from '@angular/core';
import { ProfileType } from '../../types/profile';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyPageComponent implements OnInit {

    constructor(private _auth: AuthService,
        private _router: Router) { }

    ngOnInit() {
    }

    disconnect() {
        this._auth.logout();
    }

    // check if the account is verfied
    update() {
        this._auth.checkVerify()
        .subscribe(
            (res: ProfileType) => {
                if (typeof res.verified !== 'undefined' && res.verified === false) {
                    this._router.navigate(['/verify']);
                } else {
                    this._router.navigate(['/dashboard']);
                }
            }
        );
    }
}
