import { Injectable } from '@angular/core';
import { RequestService } from 'src/app/services/request/request.service';

@Injectable({
  providedIn: 'root'
})
export class BarChartService {

  constructor(private requestService: RequestService) { }

  getData() {
      return this.requestService.get('dashboard/rating/best');
  }
}
