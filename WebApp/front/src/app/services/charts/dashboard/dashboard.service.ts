import { Injectable } from '@angular/core';
import { RequestService } from 'src/app/services/request/request.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {


  constructor(public requestService: RequestService) {
  }

  getData() {
    return this.requestService.get('dashboard/rating/general');
  }

  getYesterdayMoneyEarned() {
    return this.requestService.get('dashboard/turnover?time=1');
  }

}
