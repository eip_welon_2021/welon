import { Injectable } from '@angular/core';
import { RequestService } from 'src/app/services/request/request.service';

@Injectable({
  providedIn: 'root'
})
export class DoughnutChartService {

  constructor(private requestService: RequestService) { }

  getData() {
    return (this.requestService.get('dashboard/rating/most/rated'));
  }
}
