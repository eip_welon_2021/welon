import { Injectable } from '@angular/core';
import { RequestService } from '../../request/request.service';

@Injectable({
  providedIn: 'root'
})
export class LineChartService {

  constructor(private requestService: RequestService) { }

  getData(time: number = 7) {
    return (this.requestService.get('dashboard/turnover?time=' + time));
  }
}
