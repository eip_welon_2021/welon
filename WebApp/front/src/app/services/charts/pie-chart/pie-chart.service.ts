import { Injectable } from '@angular/core';
import { RequestService } from '../../request/request.service';

@Injectable({
  providedIn: 'root'
})
export class PieChartService {

  constructor(private requestService: RequestService) { }

  getData() {
    return (this.requestService.get('dashboard/rating/less/rated'));
  }
}
