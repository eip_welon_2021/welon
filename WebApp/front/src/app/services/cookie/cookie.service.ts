import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

// overlay of cookie service

@Injectable({
  providedIn: 'root'
})
export class Cookie {

  constructor(private cookieService: CookieService) { }

  set(name: string, value: string) {
    return this.cookieService.set(name, value, 1, '/', null, false, 'Strict');
  }

  get(name: string) {
    return this.cookieService.get(name);
  }

  getAll() {
    return this.cookieService.getAll();
  }

  check(name: string) {
    return this.cookieService.check(name);
  }

  deleteAll() {
    return this.cookieService.deleteAll();
  }

  delete(name: string) {
    return this.cookieService.delete(name);
  }
}
