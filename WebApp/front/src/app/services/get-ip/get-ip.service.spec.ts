import { TestBed } from '@angular/core/testing';

import { GetIpService } from './get-ip.service';

describe('GetIpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetIpService = TestBed.get(GetIpService);
    expect(service).toBeTruthy();
  });
});
