import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import conf from '../../conf/conf.json';

@Injectable({
  providedIn: 'root'
})
export class GetIpService {

  constructor() { }

  getIp() {
    return (environment.production || environment.onlineAPI) ? conf.ip : conf.localhost;
  }
}
