import { Injectable } from '@angular/core';
import {} from 'googlemaps';

const API_KEY = 'AIzaSyATVtdfSDKZQo5LX-drzJdX6i_RLl7Uay4';

@Injectable({
  providedIn: 'root'
})
export class MapsService {

    private initialized = false;
    private startedInit = false;

    constructor() {
        let script: HTMLScriptElement = <HTMLScriptElement>document.getElementById('mapsScript');

        // create and check if the script is already loaded
        if (!window.initMap) {
            window.initMap = () => {
                this.initialized = true;
            };
        }
        if (script === null && !this.initialized) {
            script = document.createElement('script');
            script.id = 'mapsScript';
            script.src = `https://maps.googleapis.com/maps/api/js?key=${API_KEY}&libraries=places&callback=initMap`;
            script.defer = true;
            script.async = true;

            // Append the 'script' element to 'head'
            document.head.appendChild(script);
            this.startedInit = true;
        } else {
            window.initMap();
        }
    }

    private initMap(element: HTMLElement, opt: google.maps.MapOptions, init: (map) => void) {
        if (this.startedInit && this.initialized) {
            const map = new google.maps.Map(element, opt);

            return init(map);
        }
        init(null);
    }

    loadMap(element: HTMLElement, opt: google.maps.MapOptions, init: (map) => void) {
        if (this.initialized) {
            this.initMap(element, opt, init);
        } else {
            setTimeout(this.loadMap.bind(this), 100, element, opt, init);
        }
    }

    createAutocomplete(
        element: HTMLInputElement,
        onPlaceChange?: google.maps.MVCEventHandler<google.maps.places.Autocomplete, any[]>
    ): google.maps.places.Autocomplete {

        // new autocomplete on locations in france
        const autocomplete = new google.maps.places.Autocomplete(element,  {
            types: ['geocode'],
            componentRestrictions: {country: 'fr'},
            strictBounds: true
        });

        autocomplete.setFields(['formatted_address', 'place_id']);

        if (onPlaceChange) {
            autocomplete.addListener('place_changed', onPlaceChange);
        }

        return autocomplete;
    }

    getLocationFromPlaceId(geocoder: google.maps.Geocoder, placeId: string): Promise<google.maps.GeocoderResult> {
        return new Promise((resolve, reject) => {
            geocoder.geocode({
                placeId
            }, (results, status) => {
                if (status === 'OK') {
                    return resolve(results[0]);
                }
                reject({
                    status
                });
            });
        });
    }

    getAddressFromLocation(geocoder: google.maps.Geocoder, location: google.maps.LatLng): Promise<google.maps.GeocoderResult> {
        return new Promise((resolve, reject) => {
            geocoder.geocode({
                location
            }, (results, status) => {
                if (status === 'OK') {
                    return resolve(results[0]);
                }
                reject({
                    status
                });
            });
        });
    }

    createRestaurantMarker(location: google.maps.LatLng, onClick?: (e: any) => void) {
        const icon: google.maps.ReadonlyIcon = {
            url: 'assets/pin.svg',
            scaledSize: new google.maps.Size(60, 40)
        };

        const marker = new google.maps.Marker({
            position: location,
            icon: icon
        });

        if (onClick) {
            marker.addListener('click', onClick);
        }

        return marker;
    }
}
