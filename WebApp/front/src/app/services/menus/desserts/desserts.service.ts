import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Food } from '../food';
import { RequestService } from '../../request/request.service';
import { DelicacyType } from 'src/app/types/delicacy';
import { RateType } from 'src/app/types/rate';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DessertsService implements Food<DelicacyType> {

    constructor(private requestService: RequestService) { }

    getOne(id: string): Observable<DelicacyType> {
        return this.requestService.get('desserts/infos/' + id);
    }

    getAll(): Observable<DelicacyType[]> {
        return this.requestService.get('desserts');
    }

    getRating(id: string): Observable<RateType> {
        return this.requestService.get('rating/desserts/average/' + id);
    }

    add(data: DelicacyType): Observable<any> {
        return this.requestService.post(
            'desserts',
            JSON.stringify({
                name: data.name,
                desc: data.description,
                price: data.price.toString(10)
            })
        );
    }

    delete(id: string): Observable<any> {
        return this.requestService.delete('desserts/' + id);
    }

    edit(data: DelicacyType): Observable<any> {
        return this.requestService.put(
            'desserts/' + data._id,
            JSON.stringify({
                name: data.name,
                desc: data.description,
                price: data.price.toString(10)
            })
        );
    }
}
