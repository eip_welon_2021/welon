import { Observable } from 'rxjs';
import { RateType } from '../../types/rate';

export interface Food<T> {
    getOne(id: string): Observable<T>;
    getAll(): Observable<T[]>;
    getRating(id: string): Observable<RateType>;
    add(data: T): Observable<any>;
    delete(id: string): Observable<any>;
    edit(data: T): Observable<any>;
}
