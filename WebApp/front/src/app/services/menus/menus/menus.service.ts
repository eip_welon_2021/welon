import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestService } from '../../request/request.service';
import { MenuType } from '../../../types/menu';
import { Food } from '../food';
import { RateType } from 'src/app/types/rate';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MenusService implements Food<MenuType> {

    constructor(private requestService: RequestService) { }

    getOne(id: string): Observable<MenuType> {
        return this.requestService.get('menu/' + id);
    }

    getAll(): Observable<MenuType[]> {
        return this.requestService.get('menus');
    }

    getRating(id: string): Observable<RateType> {
        return this.requestService.get('rating/menus/average/' + id);
    }

    add(data: MenuType): Observable<any> {
        return this.requestService.post(
            'menu/restaurant',
            JSON.stringify({
                name: data.name,
                desc: data.description,
                price: data.price.toString(10),
                entrees: data.id_entree,
                plats: data.id_plat,
                desserts: data.id_dessert,
                boissons: data.id_boisson
            })
        );
    }

    delete(id: string): Observable<any> {
        return this.requestService.delete('menu/' + id);
    }

    edit(data: MenuType): Observable<any> {
        return this.requestService.put(
            'menu/' + data._id,
            JSON.stringify({
                _id: data._id,
                name: data.name,
                desc: data.description,
                price: data.price.toString(10),
                entrees: data.id_entree,
                plats: data.id_plat,
                desserts: data.id_dessert,
                boissons: data.id_boisson
            })
        );
    }
}
