import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { RequestService } from 'src/app/services/request/request.service';
import { MenusService } from '../menus/menus/menus.service';
import { DishesService } from '../menus/dishes/dishes.service';
import { DessertsService } from '../menus/desserts/desserts.service';
import { DrinksService } from '../menus/drinks/drinks.service';
import { EntriesService } from '../menus/entries/entries.service';
import { DelicacyType } from 'src/app/types/delicacy';
import { MenuType } from 'src/app/types/menu';
import { FoodType } from 'src/app/types/food';
import { Cookie } from '../cookie/cookie.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

    isQR = false;
    qrCode: any;
    nbEnd = 0;
    table = [];

    nbEndSub = new Subject<number> ();
    order = new Subject<any> ();
    orderMap = new Map<string, string> ();

    private menusMap = new Map<string, FoodType> ();
    private entriesMap = new Map<string, FoodType> ();
    private dishesMap = new Map<string, FoodType> ();
    private dessertsMap = new Map<string, FoodType> ();
    private drinksMap = new Map<string, FoodType> ();

    constructor(private entriesService: EntriesService,
        private menusService: MenusService,
        private dishesService: DishesService,
        private dessertsService: DessertsService,
        private drinksService: DrinksService,
        private requestService: RequestService,
        private _cookieService: Cookie) {
        this.table = [this.menusMap, this.entriesMap, this.dishesMap, this.dessertsMap, this.drinksMap];
        this.nbEndSub.next(0);
        this.nbEndSub.subscribe(a => {
            if (a >= 5) {
                this.setOrder();
            }
        });
        this.resetOrder();
    }

    endedSub() {
        this.nbEnd = this.nbEnd + 1;
        this.nbEndSub.next(this.nbEnd);
    }

    resetOrder() {
        this.nbEnd = 0;
        this.nbEndSub.next(0);

        // get all data
        this.menusService.getAll().subscribe((menus: MenuType[]): void => {
            menus = menus.filter(e => e.id_restaurant === this._cookieService.get('id'));
            for (const ent of menus) {
                this.menusMap.set(ent._id, {name: ent.name, price: ent.price, nb: 0});
            }
            this.endedSub();
        });

        this.entriesService.getAll().subscribe((entries: DelicacyType[]) => {
            for (const ent of entries) {
                this.entriesMap.set(ent._id, {name: ent.name, price: ent.price, nb: 0});
            }
            this.endedSub();
        });

        this.dishesService.getAll().subscribe((dishes: DelicacyType[]) => {
            for (const d of dishes) {
                this.dishesMap.set(d._id, {name: d.name, price: d.price, nb: 0});
            }
            this.endedSub();
        });

        this.drinksService.getAll().subscribe((dishes: DelicacyType[]) => {
            for (const d of dishes) {
                this.drinksMap.set(d._id, {name: d.name, price: d.price, nb: 0});
            }
            this.endedSub();
        });

        this.dessertsService.getAll().subscribe((dishes: DelicacyType[]) => {
            for (const d of dishes) {
                this.dessertsMap.set(d._id, {name: d.name, price: d.price, nb: 0});
            }
            this.endedSub();
        });
    }

    setOrder() {
        this.order.next(this.table);
    }

    getOrder() {
        return this.order.asObservable();
    }

    // reset all the dalicacy & menus
    resetFood() {
        this.entriesMap.forEach((value) => {
            value.nb = 0;
        });
        this.menusMap.forEach((value) => {
            value.nb = 0;
        });
        this.drinksMap.forEach((value) => {
            value.nb = 0;
        });
        this.dessertsMap.forEach((value) => {
            value.nb = 0;
        });
        this.dishesMap.forEach((value) => {
            value.nb = 0;
        });
        this.setOrder();
        this.orderMap.clear();
    }

    // add one delicacy or menu from table
    addOne(id, kind) {
        this.table[kind].get(id).nb += 1;
        if (this.orderMap.has(id) === false) {
            this.orderMap.set(id, kind);
        }
    }

    // remove one delicacy or menu from table
    removeOne(id, kind) {
        if (this.table[kind].get(id).nb === 1) {
            this.table[kind].get(id).nb = 0;
            this.orderMap.delete(id);
        } else if (this.table[kind].get(id).nb !== 0) {
            this.table[kind].get(id).nb -= 1;
        }
    }

    getAll(): Observable<OrderType[]> {
        return this.requestService.get('command/restaurant');
    }

    getQRCode(id: string): Observable<Blob> {
        return this.requestService.get('/command/qrcode/' + id);
    }

    sendOrder() {
        const _menus = [];
        const _entrees = [];
        const _plats = [];
        const _desserts = [];
        const _boissons = [];
        this.orderMap.forEach((kind, id) => {
            if (kind.toString() === '0') {
                _menus.push([id, this.table[kind].get(id).nb.toString()]);
            } else if (kind.toString() === '1') {
                _entrees.push([id, this.table[kind].get(id).nb.toString()]);
            } else if (kind.toString() === '2') {
                _plats.push([id, this.table[kind].get(id).nb.toString()]);
            } else if (kind.toString() === '3') {
                _desserts.push([id, this.table[kind].get(id).nb.toString()]);
            } else if (kind.toString() === '4') {
                _boissons.push([id, this.table[kind].get(id).nb.toString()]);
            }
        });
        return (this.requestService.post(
            'commands',
            {
                menus: _menus,
                entrees: _entrees,
                plats: _plats,
                desserts: _desserts,
                boissons: _boissons
            }, 'blob'));
    }

}
