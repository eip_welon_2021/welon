import { Injectable } from '@angular/core';
import { RequestService } from '../request/request.service';
import { Cookie } from 'src/app/services/cookie/cookie.service';
import { ProfileType } from 'src/app/types/profile';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

    constructor(private requestService: RequestService, private http: HttpClient) { }

    getPicture() {
        return new Observable((observer) => {
            this.requestService.get('auth/me')
            .subscribe(
                (user) => {
                    // get image with get request
                    if (!user.img || user.img === undefined || user.img === null || user.img === '') {
                        return observer.error();
                    }
                    this.http.get(user.img, { responseType: 'blob' })
                    .subscribe(
                        next => {
                            observer.next(next);
                            observer.complete();
                        },
                        err => observer.error(err)
                    );
                },
                (err) => observer.error(err)
            );
        });
    }

    getData() {
        return (this.requestService.get('auth/me'));
    }

    update(new_profile: ProfileType) {
        return (this.requestService.put('auth/restaurant', new_profile as any));
    }

    deleteAccount(email: string) {
        return (this.requestService.delete('auth/restaurant', { email }));
    }
}


