import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { GetIpService } from '../../services/get-ip/get-ip.service';
import { Cookie } from 'src/app/services/cookie/cookie.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  private ip: string;

  constructor(private http: HttpClient,
    private cookieService: Cookie,
    private getIpService: GetIpService) {
    this.ip = this.getIpService.getIp();
  }

  // post method request used without access token
  postNoControl(route: string, params: HttpParams | {[param: string]: any | any[]}) {
    return (this.http.post(this.ip + route, params));
  }

  // get method request with access token
  get(route: String, requestType?: any): Observable<any> {
    const headers = new HttpHeaders().set('x-access-token', this.cookieService.get('accessToken'));
    if (requestType) {
        return (this.http.get(this.ip + route, { headers, 'responseType': requestType }));
    }
    return (this.http.get(this.ip + route, { headers }));
  }

  // post method request with access token
  post(route: String, params: any, responseType?: any): Observable<any> {
    const headers = new HttpHeaders({'Content-Type': 'application/json'}).set('x-access-token', this.cookieService.get('accessToken'));
    if (responseType) {
      return (this.http.post(this.ip + route, params, {headers, 'responseType': responseType}));
    }
    return (this.http.post(this.ip + route, params, {headers}));
  }

  // post method request with form data parameters and access token
  postFormData(route: String, params: FormData, responseType?: any): Observable<any> {
    const headers = new HttpHeaders().set('x-access-token', this.cookieService.get('accessToken'));
    if (responseType) {
      return(this.http.post(this.ip + route, params, {headers, 'responseType': responseType}));
    }
    return (this.http.post(this.ip + route, params, {headers}));
  }

  // delete method request with access token and possible body
  delete(route: String, data?: any): Observable<any> {
    const headers = new HttpHeaders().set('x-access-token', this.cookieService.get('accessToken'));
    return (this.http.request('delete', this.ip + route, {
        headers,
        body: data
      }));
  }

  // put method request with access token and body
  put(route: String, params: any): Observable<any> {
    const headers = new HttpHeaders({'Content-Type': 'application/json'}).set('x-access-token', this.cookieService.get('accessToken'));
    return (this.http.put(this.ip + route, params, {headers}));
  }

}
