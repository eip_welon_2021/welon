import { Injectable } from '@angular/core';
import { RequestService } from 'src/app/services/request/request.service';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private requestService: RequestService) { }

  public uploadImage(formData: FormData) {
    return this.requestService.postFormData('upload/images', formData);
  }
}
