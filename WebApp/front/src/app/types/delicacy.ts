// entries, dishes, desserts, drinks and menus base interface
export interface DelicacyType {
    readonly _id?: string;
    readonly id_restaurant?: string;
    name: string;
    description: string;
    price: number;
    readonly general_average?: number;
    readonly quality_average?: number;
    readonly quantity_average?: number;
    readonly price_average?: number;
    readonly rating_numbers?: number;
}
