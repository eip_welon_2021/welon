export interface FoodType {
    readonly _id?: string;
    name: string;
    price: number;
    nb: number;
}
