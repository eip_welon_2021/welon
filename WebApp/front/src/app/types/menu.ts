import { DelicacyType } from './delicacy';

export interface MenuType extends DelicacyType {
    id_entree?: Array<string>;
    id_plat?: Array<string>;
    id_dessert?: Array<string>;
    id_boisson?: Array<string>;
}
