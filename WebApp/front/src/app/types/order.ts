interface OrderType {
    readonly _id?: string;
    readonly id_restaurant?: string;
    readonly id_user?: string;
    menus: string[][];
    entrees: string[][];
    plats: string[][];
    desserts: string[][];
    boissons: string[][];
    readonly price?: number;
    readonly created_at?: Date;
}
