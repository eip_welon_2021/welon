export interface ProfileType {
    readonly _id?: string;
    readonly verified?: boolean;
    name: string;
    email?: string;
    address: string;
    latitude: number;
    longitude: number;
    phone: string;
    password?: string;
    current_password?: string;
    img?: string;
}
