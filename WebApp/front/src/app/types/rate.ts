export interface RateType {
    readonly _id: string;
    number: number;
    quantity: number;
    quality: number;
    price: number;
}
