export interface UserType {
    email: string;
    password: string;
    name?: string;
    address?: string;
    latitude?: number;
    longitude?: number;
    phone?: string;
}
