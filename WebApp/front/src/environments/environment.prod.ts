export const environment = {
  production: true,
  onlineAPI: true,
  https: true,
  cookieDomain: 'welon.fr'
};
