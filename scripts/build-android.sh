#!/bin/bash

cd ../Mobile
./gradlew :resolveDependencies
./gradlew assembleRelease
mkdir -p ../WebApp/front/src/assets/android
cp ./app/build/outputs/apk/release/app-release.apk ../WebApp/front/src/assets/android/welon-app.apk
cd -
./mise-en-prod.sh