#!/bin/bash

clear

echo "Are you on a new server ? ( y / n )"

read answer

if [ $answer != "y" ]
then
    echo "Canceling"
    exit 0
fi

echo "ARE YOU REALLY SURE YOU ARE ON A NEW SERVER ? ( YES / no )"

read zozo

if [ $zozo != "YES" ]
then
    echo "Script cancelled."
    exit 0
fi
   
echo "installation script launched"

echo
echo "First Step: Initiate GitLab Repo"

echo " 1. Connect to GitLab on your web Browser"
echo "GitLab.com"
echo

# ssh-keygen -o -t rsa -b 4096 -C "vm-Welon"

echo " 2. Copy The following Key (everything beetwen the 2 blank line):"
echo
cat ~/.ssh/id_rsa.pub
echo
echo " 3. Go to:"
echo "https://gitlab.com/profile/keys"
echo
echo " 4. Paste the key on the text box"
echo
echo " 5. Click on Add Key"
echo
echo " 6. Check if you see your key in Your key list when you scroll down the page"

echo
echo "Do You see it ? (y / n)"

read zozo

if [ $zozo != "y" ]
then
    echo "Restart plz"
    echo
    exit 0
fi

mkdir ~/test
cd ~/test

clear
sudo apt-get update
clear
sudo apt-get install git unzip
clear
sudo apt install -y --no-install-recommends ca-certificates p11-kit
sudo apt install openjdk-8-jre openjdk-8-jdk
echo "export ANDROID_HOME=/opt/android-sdk-linux" >> ~/.bashrc
echo "export PATH=${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools" >> ~/.bashrc
sudo mkdir -p ${ANDROID_HOME}
cd ${ANDROID_HOME}
sudo wget -q https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip -O android_tools.zip
sudo unzip android_tools.zip
sudo rm android_tools.zip
sudo chmod +w .
sudo chown -R adminwelon .
yes | sdkmanager --licenses

git clone git@gitlab.com:eip_welon_2021/welon.git
