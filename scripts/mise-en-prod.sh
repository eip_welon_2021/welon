#!/bin/bash

echo "The script must be executed in the script folder"
cd ..
git pull
cd WebApp/front
npm run build
cd ../../
sudo service nginx stop
pm2 stop 'sudo docker-compose up'
sudo docker-compose down
sudo docker-compose build
pm2 start 'sudo docker-compose up'
sudo service nginx start
pm2 list
